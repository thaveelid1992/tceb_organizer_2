import React, {Component} from 'react'
import {
    Text,
    TextInput,
    Platform,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    Alert,
    AsyncStorage,
    Dimensions,
    ActivityIndicator,
    SafeAreaView,
    RefreshControl
} from 'react-native';
import {Container, Header, Left, Body, Right, Button, Icon, Title, Input, Item} from 'native-base';
import {NavigationActions} from "react-navigation";
import {color, fontSize} from '../utills/color';
import * as styleApp from '../utills/StyleApp';
import {Col, Row, Grid} from "react-native-easy-grid";
import * as actions from './CheckinListv2Action'
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Strings} from '../utills/StringConfig';
import {Medium, Light} from '../utills/Fonts';
import ItemList from '../components/ItemCheckinByList';
import ItemListV2 from '../components/ItemCheckinByListv2';
import FastImage from 'react-native-fast-image';
import Loading from '../components/Loading';
import AtoZList from 'react-native-atoz-list';

const {width, height} = Dimensions.get('window');

let api_tokenMain

const ic_notfound = require('../assets/images/ic_notfound.png');
const qr_white = require('../assets/images/qr_white.png');

class CheckInListv2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFilter: false,
            start_position: 1,
            refreshing: false,
            search: ''
        }
    }

    static navigationOptions = {
        header: null,
    };

    backPress() {
        this.setState({isFilter: false})
        this.props.actions.setBack()
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    componentDidMount() {
        this.getEventAttendee(this.state.start_position)

    }

    componentWillReceiveProps(nextProps){
        if(nextProps.checkinlistv2.status){
            this.props.actions.setDefaultStatus();
        }
    }

    _onRefresh = () => {
        this.setState({refreshing: false});
        this.getEventAttendee(1)
    }

    getEventAttendee(position) {
        const {params} = this.props.navigation.state;
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                api_tokenMain = api_token;
                this.props.actions.setDefault()
                this.props.actions.getEventAttendeeV2(api_tokenMain, params.data.event.id, position, params.data.zone.key);
            }
        });
    }

    filterPress() {
        if (this.state.isFilter) {
            this.setState({isFilter: false})
        } else {
            const {params} = this.props.navigation.state;
            this.props.actions.getEventTicket(api_tokenMain, params.data.event.id)
            this.setState({isFilter: true})
        }
    }

    qrPress() {
        const {params} = this.props.navigation.state;
        this.props.navigation.navigate('Scanner', {
            data: {
                zone: params.data.zone,
                eventName: params.data.eventName,
                event: params.data.event
            }
        })
    }

    dashboardPress() {
        const {params} = this.props.navigation.state;
        this.props.navigation.navigate('dashboard', {data: params})
    }

    handleLoadMore = () => {
        const {attendee_count, data} = this.props.checkinlistv2;
        const {params} = this.props.navigation.state;
        if (attendee_count > data.length) {
            this.props.actions.getEventAttendeeLoadMore(api_tokenMain, params.data.event.id, this.state.start_position + 1, params.data.zone.key);
            this.setState({start_position: this.state.start_position + 1});
        }
    };

    renderListItem(item) {
        const {id, full_name, email, tel, ticket_type, ticket_id, has_arrived} = item;
        const {params} = this.props.navigation.state;
        return (
            <ItemListV2 data={item}
                        event={params.data.event}
                        zone={params.data.zone}
                        token={api_tokenMain}/>
            // <View>
            //     <Text numberOfLines={1} style={styles.rowText}>{full_name}</Text>
            // </View>
        );
    }

    renderListTicketType({item}) {
        const {title} = item;
        return (
            <TouchableOpacity onPress={() => this.selectItemTicketType(item)}>
                <Grid style={styles.grid}>
                    <Row style={styles.rowImageticket}>
                        <Col style={styles.colImageticket}>
                            <Image
                                source={require('../assets/images/ic_ticket.png')}
                                style={styles.imageSizeTicket}/>
                        </Col>
                        <Col style={styles.rowTickettype}>
                            <Text numberOfLines={1} style={styles.rowText}>{title}</Text>
                        </Col>
                    </Row>
                    <Row>
                        <View style={styles.line}></View>
                    </Row>
                </Grid>
            </TouchableOpacity>
        );
    }

    renderHeader() {
        return (
            <View style={styles.viewFilter}>
                <Text style={styles.textFilter}>{Strings.Filter_Header}</Text>
            </View>
        );
    }

    selectItem(event) {
        Alert.alert(
            Strings.Confirm_Check_in,
            event.full_name,
            [
                {text: Strings.OK, onPress: () => this.userCheckinByID(event.id)},
                {text: Strings.Cancel, onPress: () => ''},
            ],
            {cancelable: false}
        )
    }

    selectItemTicketType(tickettype) {
        setTimeout(() => {
            const {params} = this.props.navigation.state;
            this.props.navigation.navigate('checkintickettype', {datatickettype: tickettype, dataevent: params.data})
        }, 220)
    }

    userCheckinByID(id) {
        this.props.actions.setCheckInByID(api_tokenMain, id);
    }

    render() {
        return (
            <SafeAreaView style={{backgroundColor: color.primary, flex: 1}}>
                <View style={styles.container}>
                    <View style={styles.actionBar}>
                        <Grid>
                            <Col size={33} style={{ marginTop: '2%' , marginLeft: 10}}>
                                <TouchableOpacity onPress={this.backPress.bind(this)}>
                                    <Image source={require('../assets/images/ic_back.png')}
                                           style={styles.btnImage} resizeMode='contain'/>
                                </TouchableOpacity>
                            </Col>
                            <Col size={34} style={{marginTop: '1%',alignSelf: 'center', alignItems: 'center'}}>
                               <Text style={{fontSize: 24,fontFamily: Medium.font,color: 'white'}}>{Strings.checktit_in}</Text>
                            </Col >
                            <Col size={33}>
                                 {/* style={{
                                     marginTop: '1%',
                                     flexDirection: 'row',
                                     marginLeft: 8,
                                     marginRight: 8
                                 }}>
                                <TouchableOpacity
                                    style={styles.touchBtnImage} onPress={this.filterPress.bind(this)}>
                                    <Image
                                        source={this.state.isFilter ? require('../assets/images/ic_filter_active.png') : require('../assets/images/ic_filter.png')}
                                        style={styles.sizeimagefilter}/>
                                </TouchableOpacity> */}
                            </Col>
                        </Grid>
                    </View>
                    <View style={{
                        flexDirection: "row",
                        alignItems: 'center',
                        alignSelf: 'center',
                        justifyContent: 'center',
                        marginTop: 4,
                        marginBottom: 4
                    }}>
                        <TouchableOpacity
                            onPress={() => this.qrPress()}
                            style={styles.ApproveButton}
                            underlayColor='#fff'>
                                <Image source={qr_white}
                                           style={styles.btnQr} resizeMode='contain'/>
                            <Text style={styles.textchecked}>{Strings.qr_scan_title}</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{
                        flexDirection: 'row',
                        backgroundColor: color.white,
                        borderRadius: 10,
                        borderWidth: 4,
                        borderColor: color.white,
                        marginTop: 6,
                        marginBottom: 20,
                        marginHorizontal: 10
                    }}>
                        <Image source={require('../assets/images/ic_search_gary.png')}
                               style={{
                                   width: 18,
                                   height: 18,
                                   alignItems: 'center',
                                   alignSelf: 'center',
                                   justifyContent: 'center',
                                   marginHorizontal: 4
                               }}/>
                        <Input style={styles.textBox} placeholder={Strings.search}
                               onChangeText={(text) => this.onSearchChange(text)}
                               placeholderTextColor={color.textTitle}
                               value={this.state.search}/>
                    </View>
                    {this.renderContainer()}
                </View>
            </SafeAreaView>
        );
    }

    onSearchChange(text) {
        const {params} = this.props.navigation.state;
        this.setState({search: text})
        if (text.length > 0) {
            this.props.actions.getEventAttendeeSearch(api_tokenMain, params.data.event.id, 1, params.data.zone.key, text);
        } else {
            this.props.actions.setDefault();
            this.getEventAttendee(1);
        }
    }

    renderContainer() {
        // return(
        //     <View style={{flex:1,justifyContent:'center'}}>
        //         <ActivityIndicator
        //             size="large" color="#FC9575"
        //             animating={true}/>
        //     </View>
        // );
        if (this.state.isFilter) {
            if (this.props.checkinlistv2.isTypeLoading) {
                return (
                    <View style={{flex: 1, alignItems: 'center', alignSelf: 'center', justifyContent: 'center'}}>
                        <ActivityIndicator
                            size="large" color="#FC9575"
                            animating={this.props.checkinlistv2.isTypeLoading}/>
                    </View>
                );
            } else {
                return (
                    <AtoZList
                        data={this.props.checkinlistv2.datatickettype}
                        renderCell={this.renderListTicketType.bind(this)}
                        initialScrollIndex={1}
                        renderSection={this._renderHeader}/>

                );
            }
            // return (
            //     <View style={{flex:1,alignItems:'center',alignSelf:'center',justifyContent:'center'}}>
            //         <ActivityIndicator
            //             size="large" color="#FC9575"
            //             animating={true}/>
            //     </View>
            // );
        } else {
            if (this.props.checkinlistv2.isLoading) {
                return (
                    <View style={{flex: 1, alignItems: 'center', alignSelf: 'center', justifyContent: 'center'}}>
                        <ActivityIndicator
                            size="large" color="#FC9575"
                            animating={this.props.checkinlistv2.isLoading}/>
                    </View>
                );
            } else {
                if (this.props.checkinlistv2.isSearch) {
                    if (this.props.checkinlistv2.data_search.length === 0) {
                        return (
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <FastImage source={ic_notfound} style={{width: width * 0.3, height: height * 0.3}}
                                           resizeMode='stretch'/>
                                <Text style={{marginTop: height * 0.03, fontSize: 24, color: '#8CA0B3'}}>
                                    {Strings.data_attendee_empty}
                                </Text>
                            </View>
                        );
                    } else {
                        return (
                            <AtoZList
                                data={this.props.checkinlistv2.data_search}
                                renderCell={this.renderListItem.bind(this)}
                                renderSection={this._renderHeader}
                                initialScrollIndex={1}
                                cellHeight={90}
                            />
                        );
                    }
                } else {
                    if (this.props.checkinlistv2.data.length === 0) {
                        return (
                            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                                <FastImage source={ic_notfound} style={{width: width * 0.3, height: height * 0.3}}
                                           resizeMode='stretch'/>
                                <Text style={{marginTop: height * 0.03, fontSize: 24, color: '#8CA0B3'}}>
                                    {Strings.data_attendee_empty}
                                </Text>
                            </View>
                        );
                    }
                    return (
                        <AtoZList
                            data={this.props.checkinlistv2.data}
                            renderCell={this.renderListItem.bind(this)}
                            renderSection={this._renderHeader}
                            ref={(ref) => { this.flatListRef = ref; }}
                            cellHeight={90}/>
                    );
                }
            }
        }
    }

    _renderHeader(data) {
        return (
            <View style={{height: 35, justifyContent: 'center', backgroundColor: '#eee', paddingLeft: 10}}>
                <Text>{data.sectionId}</Text>
            </View>
        )
    }

    renderFooter = () => {
        const {attendee_count, data} = this.props.checkinlistv2;
        const {params} = this.props.navigation.state;
        if (attendee_count > data.length) {
            return (
                <View style={{flex: 1, alignItems: 'center', alignSelf: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator
                        size="large" color="#FC9575"
                        style={{marginTop: 10, marginBottom: 10}}
                        animating={true}/>
                </View>
            );
        } else {
            return null;
        }

    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.bg_v2
    },
    actionBar: {
        height: Platform.OS === 'ios' ? 64 : 54,
        backgroundColor: color.primary,
    },
    actionBarLeft: {
        flex: 0.5,
    },
    actionBarBody: {
        flex: 3,
    },
    actionBarRight: {
        flex: 1.5,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        marginLeft: 8
    },
    iconImage: {
        width: 10,
        height: 10,
    },
    textBox: {
        fontSize: fontSize.medium,
        alignSelf: 'stretch',
        color: color.textTitle,
    },
    btnImage: {
        marginTop: Platform.OS === 'ios' ? 10 : 0,
        height: 28,
        width: 28
    },
    btnQr: {
        height: 28,
        width: 28,
        marginRight: 8
    },
    btnImagehead: {
        height: 28,
        width: 28
    },
    line: {
        borderBottomColor: color.textBlue,
        borderBottomWidth: 0.5,
        marginTop: 10,
        marginBottom: 10,
        width: '100%',
    },
    rowTextTitle: {
        flex: 1,
        fontSize: fontSize.small,
        alignSelf: 'center',
        fontFamily: Light.font,
        color: color.textFilterBlue
    },
    rowText: {
        flex: 3,
        paddingLeft: 8,
        fontSize: fontSize.medium,
        color: color.primary,
        fontFamily: Light.font,
    },
    rowData: {
        flex: 4
    },
    rowTickettype: {
        flex: 4,
    },
    colImageticket: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    },
    imageSize: {
        width: '75%',
        height: '75%',
        resizeMode: 'contain'
    },
    imageSizeTicket: {
        width: '100%',
        height: '100%',
        resizeMode: 'contain'
    },
    colImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8,
    },
    rowImage: {
        paddingHorizontal: 20
    },
    rowImageticket: {
        paddingHorizontal: 20,
        paddingVertical: 10
    },
    grid: {
        marginTop: 10,
    },
    sizeimagefilter: {
        width: '70%',
        height: '70%',
        resizeMode: 'contain'
    },
    touchBtnImage: {
        flex: 1,
        alignItems: 'flex-end',
        marginTop: Platform.OS === 'ios' ? 10 : 0
    },
    viewFilter: {
        backgroundColor: color.colorFilter,
        paddingHorizontal: 20,
        paddingVertical: 5
    },
    textFilter: {
        color: color.white,
        fontSize: fontSize.medium,
        fontFamily: Light.font,
    },
    textchecked: {
        alignSelf: 'center',
        color: color.white,
        fontSize: fontSize.medium,
        fontFamily: Light.font,
    },
    ApproveButton: {
        flex : 0.5,
        marginTop: 8,
        marginBottom: 8,
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 8,
        paddingRight: 8,
        backgroundColor: '#50E3C2',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#50E3C2',
        flexDirection: "row",
        alignSelf: 'center',
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const mapStateToProps = ({checkinlistv2}) => {
    return {
        checkinlistv2
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(CheckInListv2)
