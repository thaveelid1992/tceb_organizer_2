const INITIAL_STATE = {
    data: [],
    datatickettype: [],
    message: '',
    status: false,
    checkin: false,
    attendee_checkin: {},
    isTypeLoading:false,
    attendee_count: 0,
    isLoading: false,
    isLoading_more:false,
    isSearch:false,
    data_search: [],
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'CHECKINBYLISTV2_ACTION_LOADING':
            return {
                ...state,
                isLoading: true,
            };
        case 'AttendeeV2_SUCCESS':
            return {
                ...state,
                status: true,
                isSearch:false,
                checkin: false,
                isLoading: false,
                attendee_count: action.payload.attendees_count,
                data: action.payload.attendees
            };
            case 'CHECKINBYLISTV2_ACTION_LOADING_MORE':
            return {
                ...state,
                isLoading_more: true,
            };
        case 'AttendeeV2_LoadMore_SUCCESS':
            return {
                ...state,
                status: true,
                isSearch:false,
                checkin: false,
                isLoading_more: false,
                attendee_count: action.payload.attendees_count,
                data: state.data.concat(action.payload.attendees)
            };
        case 'CHECKINBYLISTV2_SUCCESS':
            return {
                ...state,
                status: true,
                checkin: true,
                isLoading: false,
                isSearch:false,
                attendee_checkin: action.payload
            };
        case 'AttendeeV2_SEARCH_RESET':
            return {
                ...state,
                isLoading: true,
                isSearch:false,
                data_search: [],
            };
        case 'AttendeeV2_SEARCH_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isSearch:true,
                data_search: action.payload.attendees,
            };
        case 'EVENTTICKETV2_SUCCESS':
            return {
                ...state,
                status: false,
                checkin: false,
                isTypeLoading:false,
                isLoading: false,
                datatickettype: action.payload.tickets,
            };
        case 'EVENTTICKETV2_LOADING':
            return {
                ...state,
                isLoading: false,
                isTypeLoading:true
            };
        case 'DEFAULTV2':
            return {
                ...state,
                data: [],
                datatickettype: [],
                message: '',
                status: false,
                checkin: false,
                attendee_checkin: {},
                isTypeLoading:false,
                attendee_count: 0,
                isLoading: false,
                isSearch:false,
                data_search: [],
            };
        case 'BACKV2':
            return {
                ...state,
                data: [],
                datatickettype: [],
                message: '',
                isLoading: false,
                status: false,
                checkin: false,
                attendee_checkin: {},
                attendee_count: 0,
            };
        case 'DEFAULTSTATUS':
            return {
                ...state,
                status: false,
            };
        default:
            return state;
    }
};