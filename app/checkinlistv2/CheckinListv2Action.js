import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';
import { Strings } from '../utills/StringConfig';

export const getEventAttendee = (token, event_id, start_position, zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKINBYLISTV2_ACTION_LOADING',
        })
        axios.get(`${baseUrl}/getEventAttendeeV2?event_id=${event_id}&start=${start_position}&zone_id=${zone_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'AttendeeV2_SUCCESS',
                        payload: response.data
                    })
                } else {
                    dispatch({
                        type: 'DEFAULTV2',
                    })
                }

            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};
export const getEventAttendeeLoadMore = (token, event_id, start_position, zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKINBYLISTV2_ACTION_LOADING_MORE',
        })
        axios.get(`${baseUrl}/getEventAttendeeV2?event_id=${event_id}&start=${start_position}&zone_id=${zone_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'AttendeeV2_LoadMore_SUCCESS',
                        payload: response.data
                    })
                } else {
                    dispatch({
                        type: 'DEFAULTV2',
                    })
                }

            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const getEventAttendeeSearch = (token, event_id, start_position, zone_id, que) => {
    return (dispatch) => {
        dispatch({
            type: 'AttendeeV2_SEARCH_RESET',
        })
        axios.get(`${baseUrl}/getEventAttendeeV2?event_id=${event_id}&start=${start_position}&zone_id=${zone_id}&q=${que}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                dispatch({
                    type: 'AttendeeV2_SEARCH_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const setCheckInByID = (token, id, zone_id) => {
    return (dispatch) => {
        axios.post(`${baseUrl}/CheckInByID`, {
            attendee_id: id,
            zone_id: zone_id,
        }, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            })
            .then((response) => {
                dispatch({
                    type: 'CHECKINBYLISTV2_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const getEventTicket = (token, event_id) => {
    return (dispatch) => {
        dispatch({
            type: 'EVENTTICKETV2_LOADING',
        })
        axios.get(`${baseUrl}/getEventTicketType?event_id=${event_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                dispatch({
                    type: 'EVENTTICKETV2_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
}

export const setDefault = () => {
    return (dispatch) => {
        dispatch({
            type: 'DEFAULTV2',
        })
    }
};

export const setDefaultStatus = () => {
    return (dispatch) => {
        dispatch({
            type: 'DEFAULTSTATUS',
        })
    }
};

export const setBack = () => {
    return (dispatch) => {
        dispatch({
            type: 'BACKV2',
        })
    }
};

export const getEventAttendeeV2 = (token, event_id, start_position, zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKINBYLISTV2_ACTION_LOADING',
        })
        axios.get(`${baseUrl}/getEventAttendeeV2?event_id=${event_id}&start=${start_position}&zone_id=${zone_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'AttendeeV2_SUCCESS',
                        payload: response.data
                    })
                } else {
                    dispatch({
                        type: 'DEFAULTV2',
                    })
                }

            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};