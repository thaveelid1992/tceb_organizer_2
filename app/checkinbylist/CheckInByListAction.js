import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';
import { Strings } from '../utills/StringConfig';

export const getEventAttendee = (token, event_id, start_position, zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKINBYLIST_ACTION_LOADING',
        })
        axios.get(`${baseUrl}/getEventAttendee?event_id=${event_id}&start=${start_position}&zone_id=${zone_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'Attendee_SUCCESS',
                        payload: response.data
                    })
                } else {
                    dispatch({
                        type: 'DEFAULT',
                    })
                }

            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};
export const getEventAttendeeLoadMore = (token, event_id, start_position, zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKINBYLIST_ACTION_LOADING_MORE',
        })
        axios.get(`${baseUrl}/getEventAttendee?event_id=${event_id}&start=${start_position}&zone_id=${zone_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'Attendee_LoadMore_SUCCESS',
                        payload: response.data
                    })
                } else {
                    dispatch({
                        type: 'DEFAULT',
                    })
                }

            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const getEventAttendeeSearch = (token, event_id, start_position, zone_id, que) => {
    return (dispatch) => {
        dispatch({
            type: 'Attendee_SEARCH_RESET',
        })
        axios.get(`${baseUrl}/getEventAttendee?event_id=${event_id}&start=${start_position}&zone_id=${zone_id}&q=${que}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                dispatch({
                    type: 'Attendee_SEARCH_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const setCheckInByID = (token, id, zone_id) => {
    return (dispatch) => {
        axios.post(`${baseUrl}/CheckInByID`, {
            attendee_id: id,
            zone_id: zone_id,
        }, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            })
            .then((response) => {
                dispatch({
                    type: 'CHECKINBYLIST_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const getEventTicket = (token, event_id) => {
    return (dispatch) => {
        dispatch({
            type: 'EVENTTICKET_LOADING',
        })
        axios.get(`${baseUrl}/getEventTicketType?event_id=${event_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                dispatch({
                    type: 'EVENTTICKET_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
}

export const setDefault = () => {
    return (dispatch) => {
        dispatch({
            type: 'DEFAULT',
        })
    }
};

export const setBack = () => {
    return (dispatch) => {
        dispatch({
            type: 'BACK',
        })
    }
};