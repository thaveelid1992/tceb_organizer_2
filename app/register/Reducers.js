const INITIAL_STATE = {
    data: {},
    data_ticket:[],
    data_zone:[],
    message: '',
    status: false,
    status_ticket:false,
    status_zone:false,
    callback:false,
    isLoading: false,
    isLoading_ticket:false,
    isLoading_zone : false,
    isGetZone: false,
    isGetTicket: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'REGIS_LOADING':
            return {
                ...state,
                isLoading: true
            };
        case 'REGIS_SUCCESS':
            return {
                ...state,
                status: true,
                callback:true,
                data: action.payload,
                isLoading: false
            };
        case 'REGIS_FAILED':
            return {
                ...state,
                status: false,
                callback:true,
                isLoading: false,
                message:action.payload,
            };
        case 'REGIS_RESET':
            return {
                ...state,
                data: {},
                data_zone: [],
                message: '',
                status: false,
                callback:false,
                isLoading: false,
            };
        case 'TICKET_LOAD_DATA':
                return {
                    ...state,
                    isLoading_ticket: true
                };
        case 'TICKET_SUCCESS':
                return {
                    ...state,
                    status_ticket: true,
                    data_ticket: action.payload.tickets,
                    isLoading_ticket: false,
                    isGetTicket:true
                };
        case 'TICKET_FAILED':
                return {
                    ...state,
                    isLoading_ticket: false,
                    isGetTicket:false
                };
        case 'ZONE_LOADING':
            return {
                ...state,
                isLoading_zone: true,
            };
        case 'ZONE_SUCCESS':
            return {
                ...state,
                isLoading_zone: false,
                isGetZone: true,
                data_zone: action.payload.zones
            };
        case 'ZONE_FAILED':
            return {
                ...state,
                isLoading_zone: false,
                isGetZone: false,
            };
        default:
            return state;
    }
};