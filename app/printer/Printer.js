import React, {Component} from 'react'
import {
    Text,
    StyleSheet,
    Dimensions,
    FlatList,
    View,
    TouchableOpacity,
    AsyncStorage,
    Platform,
    RefreshControl,
    Alert
} from 'react-native';
import {Container, Header, Left, Body, Right, Button, Icon, Title, Subtitle, Content} from 'native-base';
import {color, fontSize} from '../utills/color';
import {Medium, Light} from '../utills/Fonts';


const {width, height} = Dimensions.get('window');

class Printer extends Component {

    constructor(props) {
        super(props)
        this.state = {
            isEnabled: false,
            discovering: false,
            devices: [],
            unpairedDevices: [],
            connected: false,
            section: 0,
            message: ''
        }
    }

    componentWillMount() {
        // Promise.all([
        //     BluetoothSerial.isEnabled(),
        //     BluetoothSerial.list()
        // ])
        //     .then((values) => {
        //         const [isEnabled, devices] = values
        //         Alert.alert('found device');
        //         this.setState({isEnabled, devices})
        //     })
        //
        // BluetoothSerial.on('bluetoothEnabled', () => Toast.showShortBottom('Bluetooth enabled'))
        // BluetoothSerial.on('bluetoothDisabled', () => Toast.showShortBottom('Bluetooth disabled'))
        // BluetoothSerial.on('error', (err) => console.log(`Error: ${err.message}`))
        // BluetoothSerial.on('connectionLost', () => {
        //     if (this.state.device) {
        //         Alert.alert('connectionLost');
        //         this.setState({message: 'connectionLost'});
        //     }
        //     this.setState({connected: false})
        // })
    }

    render() {
        return (
            <Container>
                <Header>
                    <Body>
                    <TouchableOpacity>
                        <Text style={{
                            color: 'black',
                            fontSize: fontSize.large,
                            fontFamily: Medium.font
                        }}>printer Screen</Text>
                    </TouchableOpacity>
                    </Body>
                </Header>
                <Content style={{backgroundColor: 'white'}}>
                    <View style={{flex: 1}}>
                        <Text style={{textAlign: 'center'}}>{this.state.message}</Text>
                    </View>
                </Content>
            </Container>
        );
    }

}

const styles = StyleSheet.create({});

export default Printer;