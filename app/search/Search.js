import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    AsyncStorage,
    Image,
    TouchableOpacity,
    Dimensions, NativeModules, ActivityIndicator
} from 'react-native';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {color} from '../utills/color';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Container, Header, Left, Body, Right, Title, Content, Button, Input} from 'native-base';
import {fontSize} from "../utills/color";
import {Light, Medium} from "../utills/Fonts";

const ic_search = require('../assets/images/ic_search.png');
const back = require('../assets/images/arrow_back.png');
const {width, height} = Dimensions.get('window');
import {NavigationActions} from "react-navigation";
import * as actions from "./Actions";
import {Strings} from '../utills/StringConfig';
// import console = require('console');

type Props = {};
import Approve from '../components/ipadAlert'

class Search extends Component<Props> {
    constructor(props) {
        super(props)
        this.state = {
            search: '',
            done: false,
            isShow: false,
            profile: {}
        };
        // this.props.navigation.state.params.event
        // this.props.navigation.state.params.ticket
    }

    componentDidMount() {
        if (this.props.navigation.state.params.search != undefined && this.props.navigation.state.params.search != '') {
            AsyncStorage.getItem('User', (err, result) => {
                if (result) {
                    const {api_token} = JSON.parse(result);
                    this.setState({search: this.props.navigation.state.params.search});
                    this.props.actions.searchInput(api_token, this.props.navigation.state.params.event.id,this.props.navigation.state.params.zoneid, this.props.navigation.state.params.search);
                }
            });
        }
    }

    onBackPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    handleSearchInput(e) {
        let text = e.toLowerCase()
        this.setState({search: text});
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                this.props.actions.searchInput(api_token, this.props.navigation.state.params.event.id,this.props.navigation.state.params.zoneid, text);

            }
        });
    }

    onComfirm() {
        this.selectItem(this.state.profile);
        this.setState({isShow: false, profile: {}})
    }

    onCancel() {
        this.setState({isShow: false, profile: {}})
    }

    render() {
        if (!this.done) {
            return (
                <Container>
                    <Approve
                        loading={this.state.isShow}
                        close={this.onCancel.bind(this)}
                        eventName={this.props.navigation.state.params.event.name}
                        name={`${this.state.profile.first_name} ${this.state.profile.last_name}`}
                        phone={this.state.profile.tel}
                        id={this.state.profile.private_reference_number}
                        email={this.state.profile.email}
                        approve={this.onComfirm.bind(this)}/>
                    <Header style={{backgroundColor: color.primary}}>
                        <Left style={{flex: 1}}>
                            <Button transparent onPress={this.onBackPress.bind(this)}>
                                <Image source={back} resizeMode='contain' style={styles.titleIcon}/>
                            </Button>
                        </Left>
                        <Body>
                        <Title style={{
                            fontSize: 30,
                            fontFamily: Light.font,
                            color: 'white'
                        }}>{Strings.search}</Title>
                        </Body>
                        <Right/>
                    </Header>
                    <Content>
                        <View style={[{marginTop: 15, marginBottom: 10}]}>
                            <View style={{paddingHorizontal: 20}}>
                                <Grid>
                                    <Col size={10} style={{justifyContent: 'center'}}>
                                        <View transparent>
                                            <Image source={ic_search} style={styles.searchIcon}/>
                                        </View>
                                    </Col>
                                    <Col size={70} style={{justifyContent: 'center'}}>
                                        <View>
                                            <Input
                                                placeholder='Search Full Name or Email'
                                                onChangeText={this.handleSearchInput.bind(this)}
                                                style={{fontSize: 30, color: '#273D52', fontFamily: Medium.font}}
                                                value={this.state.search}
                                            />
                                        </View>
                                    </Col>
                                </Grid>
                            </View>
                            <View style={styles.line}/>
                        </View>
                        {this.renderLoading()}


                    </Content>
                </Container>
            );
        } else {
            return (<View></View>);
        }


    }

    renderLoading() {
        const {data, isLoading, message} = this.props.search;
        if (isLoading) {
            return (
                <View style={{marginTop: height * 0.2}}>
                    <ActivityIndicator
                        size="large" color="#FC9575"
                        animating={true}/>
                </View>
            );
        } else {
            if (this.state.search.length === 0) {
                return (
                    <View style={{marginTop: height * 0.2}}>
                        <Text style={{textAlign: 'center', fontSize: 30, fontFamily: Medium.font}}>
                            {message}
                        </Text>
                    </View>
                );
            } else {
                if (data.length > 0) {
                    return (
                        <FlatList
                            data={data}
                            keyExtractor={(item, index) => item.id}
                            renderItem={this.renderListItem.bind(this)}
                        />
                    );
                } else {
                    return (
                        <View style={{marginTop: height * 0.2}}>
                            <Text style={{textAlign: 'center', fontSize: 30, fontFamily: Medium.font}}>
                                {Strings.no_member}
                            </Text>
                        </View>
                    );
                }
            }
        }

    }

    renderListItem({item}) {
        const {company, email, event_id, first_name, last_name, position, private_reference_number, tel, ticket_id} = item
        return (
            <TouchableOpacity style={styles.cardItem} onPress={() => this.selectItemConfirm(item)}>
                <View>
                    <Text style={[styles.textItemStyle,]}>
                        {`${Strings.frst_name}  :  ${first_name}`}
                    </Text>
                    <Text style={[styles.textItemStyle,]}>
                        {`${Strings.last_name}  :  ${last_name}`}
                    </Text>
                    <Text style={[styles.textItemStyle,]}>
                        {`${Strings.email}  :  ${email}`}
                    </Text>
                    <Text style={[styles.textItemStyle,]}>
                        {`${Strings.phone_number}  :  ${tel}`}
                    </Text>

                </View>
            </TouchableOpacity>

        );
    }

    _changeView(html, token, ref_number) {
        this.setState({done: true});
        this.props.actions.resetData(token, ref_number,this.props.navigation.state.params.zoneid);
        NativeModules.ChangeViewBridge.changeToNativeView(html);
    }

    selectItemConfirm(item) {
        this.setState({isShow: true, profile: item})
    }

    selectItem(item) {
        let html = ''
        if (item.is_vip == 1) {
            html = `<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
span{
    display:inline-block;
    border-bottom:3px solid black;
    padding-bottom:2px;
},
p {
   line-height : 1em; //relative to this element, 14px
   margin-bottom : 1em; //relative to this element, 14px
  },
  font.b {
  font-family: Helvetica, sans-serif;
}
</style>
<body>
<p><font size="10" class="b" face="Helvetica"><center><b>${item.first_name}  ${item.last_name.substring(0, 1).toUpperCase()}. (Speaker)</b></font></p>
<p><font size="8" class="b" face="Helvetica"><center><b>${item.company}</b></font></p>
<center>
<img id='barcode' 
            src="https://api.qrserver.com/v1/create-qr-code/?data=${item.private_reference_number}&amp;size=100x100" 
            alt="" 
            title="HELLO" 
            width="300" 
            height="300" /></center>

</body>
</html>`;
        } else {
            html = `<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
span{
    display:inline-block;
    border-bottom:3px solid black;
    padding-bottom:2px;
},
p {
   line-height : 1em; //relative to this element, 14px
   margin-bottom : 1em; //relative to this element, 14px
  },
  font.b {
  font-family: Helvetica, sans-serif;
}
</style>
<body>
<p><font size="10" class="b" face="Helvetica"><center><b>${item.first_name}  ${item.last_name.substring(0, 1).toUpperCase()}.</b></font></p>
<p><font size="8" class="b" face="Helvetica"><center><b>${item.company}</b></font></p>
<center>
<img id='barcode' 
            src="https://api.qrserver.com/v1/create-qr-code/?data=${item.private_reference_number}&amp;size=100x100" 
            alt="" 
            title="HELLO" 
            width="300" 
            height=300" /></center>

</body>
</html>`;
        }
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                this._changeView(html, api_token, item.private_reference_number);
            }
        });
    }

    register() {
        this.props.navigation.navigate('register', {
            event: this.props.navigation.state.params.event,
            ticket: this.props.navigation.state.params.ticket
        })
    }


}

const styles = StyleSheet.create({
    image: {
        width: width,
        height: height * 0.25,
        backgroundColor: 'grey'
    },
    textTitle: {
        fontSize: fontSize.large,
        color: color.textTitle
    },
    textSubtitle: {
        fontSize: fontSize.small,
        color: 'gray',
        fontFamily: Light.font
    },
    textDetail: {
        fontSize: fontSize.medium,
        color: color.textBlue
    },
    line: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        marginTop: 10,
        marginBottom: 10,
    },
    space: {
        marginTop: 1.5,
        marginBottom: 1.5
    }
    , titleIcon: {
        width: width * 0.035,
        height: height * 0.028,
        tintColor: '#FFFFFF'
    },
    cardItem: {
        padding: 10,
        margin: 30,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#d6d7da',
    },
    textItemStyle: {
        fontSize: 25,
        color: 'black',
        fontFamily: Medium.font,
        marginLeft: 20
    },
    ButtonStyle: {
        borderRadius: 12,
        borderWidth: 1,
        borderColor: '#d6d7da',
        backgroundColor: color.primary,
        width: width * 0.4,
        height: height * 0.08
    },
    linemenu: {
        borderBottomColor: '#D8D8D8',
        borderBottomWidth: 0.5,
        marginTop: 5,
        marginBottom: 5,
    },
    searchIcon: {
        width: width * 0.05,
        height: width * 0.05,
        alignItems: 'center',
        alignSelf: 'center',
        tintColor: 'grey'
    },

});
const mapStateToProps = ({search}) => {
    return {
        search
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Search);
