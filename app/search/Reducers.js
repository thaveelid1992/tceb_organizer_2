const INITIAL_STATE = {
    data: [],
    message: 'please input Full Name or email',
    status: false,
    callback:false,
    isLoading: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SEARCH_LOADING':
            return {
                ...state,
                isLoading: true
            };
        case 'SEARCH_SUCCESS':
            return {
                ...state,
                status: true,
                callback:true,
                data: action.payload,
                isLoading: false
            };
        case 'SEARCH_FAILED':
            return {
                ...state,
                status: false,
                callback:true,
                isLoading: false,
                message:action.payload,
            };
        case 'SEARCH_RESET':
            return {
                ...state,
                data: {},
                message: 'please input Full Name or email',
                status: false,
                callback:false,
                isLoading: false,
            };
        default:
            return state;
    }
};