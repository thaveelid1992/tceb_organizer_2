import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {printerUrl} from '../navigation/AppNavigation';

export const searchInput = (token, event_id,zone_id, text) => {
    return (dispatch) => {
        dispatch({
            type: 'SEARCH_LOADING',
        })
        axios.get(`${printerUrl}/getAttendee?event_id=${event_id}&zone_id=${zone_id}&search=${text}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                if (response.data.status == 200) {
                    dispatch({
                        type: 'SEARCH_SUCCESS',
                        payload: response.data.data
                    })
                } else {
                    dispatch({
                        type: 'SEARCH_FAILED',
                        payload: 'No members found by that name or email'
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: 'SEARCH_FAILED',
                    payload: 'No members found by that name or email'
                })
            });
    }


};
export const resetData = (token, private_reference_number,zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'SEARCH_RESET',
        })
        axios.post(`${printerUrl}/ScanQrCode`, {
            private_reference_number: private_reference_number,
            zone_id: zone_id,
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {


            })
            .catch((error) => {

            });
    }
};