import React, {Component} from 'react';
import {Provider} from "react-redux";
import Store from './redux/store';
import AppNavigation from './navigation/AppNavigation';

const main = () => {
    return (
        <Provider store={Store}>
            <AppNavigation/>
        </Provider>
    )
}

export default main