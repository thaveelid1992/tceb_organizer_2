import React, { Component } from 'react'
import { color, fontSize } from '../utills/color';
import { Text, TextInput, Platform, StyleSheet, View, Image, TouchableOpacity, FlatList, Alert, AsyncStorage } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Input, Item, Content } from 'native-base';
import { Col, Row, Grid } from "react-native-easy-grid";
import { NavigationActions } from "react-navigation";
import * as actions from './DashboardAction';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Strings } from '../utills/StringConfig';
import { Medium, Light } from '../utills/Fonts';
import Loading from '../components/Loading';

let api_tokenMain

let paramsdata

class Dashboard extends Component {

    constructor(props) {
        super(props);
        this.state = {
            isZone: true,
            isOvarall: false
        }
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        paramsdata = this.props.navigation.state;
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const { api_token } = JSON.parse(result);
                api_tokenMain = api_token;
                this.props.actions.getReportUserZone(api_tokenMain, paramsdata.params.data.data.event.id, paramsdata.params.data.data.zone.key)
            }
        });
    }

    backPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    isReportZone() {
        this.setState({ isZone: true, isOvarall: false })
        this.props.actions.getReportUserZone(api_tokenMain, paramsdata.params.data.data.event.id, paramsdata.params.data.data.zone.key)
    }

    isReportOverall() {
        this.setState({ isZone: false, isOvarall: true })
        this.props.actions.getReportAllZone(api_tokenMain, paramsdata.params.data.data.event.id, paramsdata.params.data.data.zone.key)
    }
    isReportZoneRefesh() {
        this.setState({ isZone: true, isOvarall: false })
        this.props.actions.getReportUserZoneLoad(api_tokenMain, paramsdata.params.data.data.event.id, paramsdata.params.data.data.zone.key)
    }

    isReportOverallRefesh() {
        this.setState({ isZone: false, isOvarall: true })
        this.props.actions.getReportAllZoneLoad(api_tokenMain, paramsdata.params.data.data.event.id, paramsdata.params.data.data.zone.key)
    }


    renderItemZone({ item }) {
        const { attendees, check_in, remaining, zone } = item;
        return (
            <View >
                <Grid>
                    <Row>
                        <View style={styles.itemHeader}>
                            <Text style={styles.viewHeader}>{zone}</Text>
                        </View>
                    </Row>
                    <Row style={{ padding: 20 }}>
                        <Col>
                            <Text style={styles.textTitle}>{Strings.All}</Text>
                            <Text style={styles.textData}>{attendees}</Text>
                        </Col>
                        <Col>
                            <Text style={styles.textTitle}>{Strings.Check_in_count}</Text>
                            <Text style={styles.textData}>{check_in}</Text>
                        </Col>
                        <Col>
                            <Text style={styles.textTitle}>{Strings.Remaining_count}</Text>
                            <Text style={styles.textData}>{remaining}</Text>
                        </Col>
                    </Row>
                    <Row style={styles.rowLine}>
                        <View style={styles.line}></View>
                    </Row>
                </Grid>
            </View>
        )
    }

    render() {
        return (
            <Container >
                <Loading loading={this.props.dashboard.isLoading} />
                <Header style={{ backgroundColor: color.primary }}>
                    <Left>
                        <TouchableOpacity onPress={this.backPress.bind(this)}>
                            <Image source={require('../assets/images/ic_back.png')}
                                style={styles.btnImage} resizeMode='contain' />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.textTitleActionBar}>{Strings.Report}</Title>
                    </Body>
                    <Right />
                </Header>
                <Grid>
                    <Row style={styles.oneFlex}>
                        <Col style={[styles.viewCenter, { backgroundColor: this.state.isZone ? color.colorFilter : color.white }]}>
                            <TouchableOpacity onPress={this.isReportZone.bind(this)}>
                                <Text style={[styles.textFilterTitle, { color: this.state.isZone ? color.white : color.textFilterBlue }]}>{Strings.Report_Zone}</Text>
                            </TouchableOpacity>
                        </Col>
                        <Col style={[styles.viewCenter, { backgroundColor: this.state.isOvarall ? color.colorFilter : color.white }]}>
                            <TouchableOpacity onPress={this.isReportOverall.bind(this)}>
                                <Text style={[styles.textFilterTitle, { color: this.state.isOvarall ? color.white : color.textFilterBlue }]}>{Strings.Report_Overall}</Text>
                            </TouchableOpacity>
                        </Col>
                    </Row>
                </Grid>
                <View style={styles.nineFlex}>
                    {this.isfilterDataType()}
                </View>
            </Container >
        )
    }

    isfilterDataType() {
        if (this.state.isZone) {
            return (
                <FlatList
                    data={this.props.dashboard.data}
                    refreshing={this.props.dashboard.isLoadZone}
                    renderItem={this.renderItemZone.bind(this)}
                    onRefresh={this.isReportZoneRefesh.bind(this)} />
            )
        } else {
            return (
                <FlatList
                    data={this.props.dashboard.dataAll}
                    refreshing={this.props.dashboard.isLoadAllZone}
                    renderItem={this.renderItemZone.bind(this)}
                    onRefresh={this.isReportOverallRefesh.bind(this)} />
            )
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.white
    },
    btnImage: {
        height: 28,
        width: 28
    },
    textData: {
        color: color.primary,
        fontSize: fontSize.large,
        fontFamily: Light.font
    },
    viewCenter: {
        alignItems: 'center',
        justifyContent: 'center'
    },
    oneFlex: {
        flex: 1
    },
    nineFlex: {
        flex: 9
    },
    itemHeader: {
        backgroundColor: color.primary,
        flex: 1
    },
    viewHeader: {
        paddingHorizontal: 30,
        paddingVertical: 10,
        color: color.white,
        fontSize: fontSize.medium
    },
    textTitle: {
        color: color.textFilterBlue,
        fontFamily: Light.font
    },
    textTitleActionBar: {
        color: color.white,
        fontSize: fontSize.large,
        fontFamily: Medium.font
    },
    textFilterTitle: {
        fontSize: fontSize.medium,
        fontFamily: Medium.font
    },
    rowLine: {
        marginTop: 8
    },
    line: {
        borderBottomColor: color.textBlue,
        borderBottomWidth: 0.5,
        width: '100%',
    },
})

const mapStateToProps = ({ dashboard }) => {
    return {
        dashboard
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Dashboard)