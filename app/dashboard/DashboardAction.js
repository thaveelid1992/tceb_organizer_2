import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';
import { Strings } from '../utills/StringConfig';

export const getReportUserZone = (token, event_id, zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'DASH_ACTION_LOADING',
        })
        axios.get(`${baseUrl}/getReportUserZone?event_id=${event_id}&zone_id=${zone_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                dispatch({
                    type: 'GET_USERZONE_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};
export const getReportUserZoneLoad = (token, event_id, zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'GET_USERZONE_LOAD',
        })
        axios.get(`${baseUrl}/getReportUserZone?event_id=${event_id}&zone_id=${zone_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                dispatch({
                    type: 'GET_USERZONE_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const getReportAllZone = (token, event_id) => {
    return (dispatch) => {
        dispatch({
            type: 'DASH_ACTION_LOADING',
        })
        axios.get(`${baseUrl}/getReportAllZone?event_id=${event_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                dispatch({
                    type: 'GET_ALLZONE_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};
export const getReportAllZoneLoad = (token, event_id) => {
    return (dispatch) => {
        dispatch({
            type: 'GET_ALLZONE_LOAD',
        })
        axios.get(`${baseUrl}/getReportAllZone?event_id=${event_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                dispatch({
                    type: 'GET_ALLZONE_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};