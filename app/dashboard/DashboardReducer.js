const INITIAL_STATE = {
    data: [],
    dataAll: [],
    message: '',
    status: false,
    isLoadZone: false,
    isLoadAllZone: false,
    isLoading: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'DASH_ACTION_LOADING':
            return {
                ...state,
                isLoading: true,
            };
        case 'GET_USERZONE_LOAD':
            return {
                ...state,
                isLoadZone: true,
                isLoading: false,
            };
        case 'GET_USERZONE_SUCCESS':
            return {
                ...state,
                status: true,
                data: action.payload.attendee_lists,
                isLoadZone: false,
                isLoading: false,
            };
        case 'GET_ALLZONE_LOAD':
            return {
                ...state,
                isLoadAllZone: true,
                isLoading: false,
            };
        case 'GET_ALLZONE_SUCCESS':
            return {
                ...state,
                status: true,
                dataAll: action.payload.attendee_lists,
                isLoadAllZone: false,
                isLoading: false,
            };
        default:
            return state;
    }
};