import {StyleSheet} from 'react-native'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center'
    },
    progress: {
        margin: 10,
    },
    circles: {
        flexDirection: 'row',
        flex: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    line: {
        borderBottomWidth: 1,
        marginTop: 5,
        marginBottom: 5,
    },
});