export const color = {
    primary: '#4249E7',
    iconColor: '#FC9575',
    white : '#ffffff',
    line_gary : '#EEEEEE',
    buttonLoginUnSuscess : '#687793',
    buttonLoginSuscess : '#7BE0C3',
    textTitle: '#607D8B',
    textBlue:'#243FE2',
    lineblue :'#EDF8FE',
    colorFilter : '#F08D8B',
    textFilterBlue : '#3F596F',
    bg_topic_setting : '#F6F7F8',
    black : '#000000',
    bg_v2 : '#EEF1F6',
};
export const fontSize = {
    large: 20,
    medium: 15,
    small: 12
};