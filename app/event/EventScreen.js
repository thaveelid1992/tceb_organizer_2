import React, {Component} from 'react'
import {
    Text,
    StyleSheet,
    Dimensions,
    FlatList,
    View,
    TouchableOpacity,
    AsyncStorage,
    Platform,
    RefreshControl
} from 'react-native';
import {Container, Header, Left, Body, Right, Button, Icon, Title, Subtitle, Content} from 'native-base';
import {color, fontSize} from '../utills/color';
import FastImage from 'react-native-fast-image';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as actions from './EventAction'
import {Col, Row, Grid} from "react-native-easy-grid";
import ModalDropdown from 'react-native-modal-dropdown';
import {Medium, Light} from '../utills/Fonts';

const {width, height} = Dimensions.get('window');
import Loading from '../components/Loading';

const ic_notfound = require('../assets/images/ic_notfound.png');
const ic_eventfound = require('../assets/images/ic_empty_event.png');
import {Strings} from '../utills/StringConfig';

class EventScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            refreshing: false,
        };
    }

    _onRefresh = () => {
        this.setState({refreshing: false});
        this.getAllEven();
    }

    static navigationOptions = {
        header: null,
    };

    getAllEven() {
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                this.props.actions.getAllEven(api_token);
            }
        });
    }

    componentDidMount() {
        this.getAllEven();
    }

    render() {
        return (
            <Container>
                {/*<Header>*/}
                    {/*<Body>*/}
                    {/*<TouchableOpacity onPress={this.printerPage.bind(this)}>*/}
                        {/*<Text style={{*/}
                            {/*color: 'black',*/}
                            {/*fontSize: fontSize.large,*/}
                            {/*fontFamily: Medium.font*/}
                        {/*}}>printer</Text>*/}
                    {/*</TouchableOpacity>*/}
                    {/*</Body>*/}
                {/*</Header>*/}
                {/* <Header style={{backgroundColor: color.primary}}>
                    <Body>
                    <Text style={{color: 'white', fontSize: fontSize.large ,fontFamily:Medium.font}}>{Strings.event_title}</Text>
                    </Body>
                </Header> */}
                {this.renderList()}
            </Container>
        );
    }

    renderList() {
        const {data} = this.props.event;
        if (this.props.event.isLoading) {
            return (
                <Content>
                    <Loading loading={this.props.event.isLoading}/>
                </Content>
            );
        } else {
            if (data.length > 0 && this.props.event.status) {
                return (
                    // <Content style={{backgroundColor: 'white'}}>
                    <FlatList
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._onRefresh}
                            />
                        }
                        data={data}
                        keyExtractor={(item, index) => item.id}
                        renderItem={this.renderListItem.bind(this)}
                    />
                    // </Content>
                );
            } else if (data.length == 0 && this.props.event.status) {
                return (
                    <View style={{flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center'}}>
                        <FastImage source={ic_eventfound} style={{width: width * 0.5, height: height * 0.3}}
                                   resizeMode='stretch'/>
                        <Text style={{marginTop: height * 0.03, fontSize: 24, color: '#8CA0B3'}}>
                            {Strings.data_event_empty}
                        </Text>
                    </View>
                );
            } else {
                return (
                    <View style={{flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center'}}>
                        <FastImage source={ic_notfound} style={{width: width * 0.3, height: height * 0.3}}
                                   resizeMode='stretch'/>
                        <Text style={{marginTop: height * 0.03, fontSize: 24, color: '#8CA0B3'}}>
                            {Strings.not_found}
                        </Text>
                    </View>
                );
            }
        }

    }

    renderListItem({item}) {
        const {id, name, image_url, location, date_begin, time_begin, time_end} = item;
        return (
            <TouchableOpacity onPress={() => this.selectItem(item)}>
                <FastImage
                    style={styles.image}
                    source={{
                        uri: image_url,
                        priority: FastImage.priority.normal,
                    }}
                    resizeMode={FastImage.resizeMode.cover}
                />
                <Grid style={{paddingHorizontal: 20, marginTop: 10}}>
                    <Row>
                        <Text style={styles.textTitle}>{name}</Text>
                    </Row>
                    <View style={styles.line}></View>
                    <Row>

                    </Row>
                    <Row>
                        <Text style={styles.textSubtitle}>{Strings.location}</Text>
                    </Row>
                    <Row style={styles.space}>
                        <Text style={styles.textDetail}>{location}</Text>
                    </Row>
                    <Row style={styles.space}>
                        <Col size={2}>
                            <Text style={styles.textSubtitle}>{Strings.date}</Text>
                        </Col>
                        <Col size={1}>
                            <Text style={styles.textSubtitle}>{Strings.begins}</Text>
                        </Col>
                        <Col size={1}>
                            <Text style={styles.textSubtitle}>{Strings.ends}</Text>
                        </Col>

                    </Row>
                    <Row style={[styles.space, {marginBottom: 10}]}>
                        <Col size={2}>
                            <Text style={styles.textDetail}>{date_begin}</Text>
                        </Col>
                        <Col size={1}>
                            <Text style={styles.textDetail}>{time_begin}</Text>
                        </Col>
                        <Col size={1}>
                            <Text style={styles.textDetail}>{time_end}</Text>
                        </Col>

                    </Row>
                </Grid>
            </TouchableOpacity>

        );
    }

    selectItem(event) {
        this.props.navigation.navigate('CheckIn', {obj: event})
    }
    printerPage(){
        this.props.navigation.navigate('printer')
    }
}

const styles = StyleSheet.create({
    image: {
        width: width,
        height: height * 0.2,
        backgroundColor: 'grey'
    },
    textTitle: {
        fontSize: fontSize.large,
        color: color.textTitle
    },
    textSubtitle: {
        fontSize: fontSize.small,
        color: 'gray',
        fontFamily: Light.font
    },
    textDetail: {
        fontSize: fontSize.medium,
        color: color.textBlue
    },
    line: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        marginTop: 5,
        marginBottom: 5,
        width: '12%',
    },
    space: {
        marginTop: 1.5,
        marginBottom: 1.5
    }
});

const mapStateToProps = ({event}) => {
    return {
        event
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(EventScreen);