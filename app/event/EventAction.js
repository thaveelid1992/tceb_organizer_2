import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';

export const getAllEven = (token) => {
    return (dispatch) => {
        dispatch({
            type: 'EVENT_LOAD_DATA',
        })
        axios.get(`${baseUrl}/getAllEvent`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                dispatch({
                    type: 'EVENT_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    'Error',
                    error.message,
                    [
                        {text: 'OK', onPress: () => dispatch({
                                type: 'EVENT_FAILED',
                                payload: error.message
                            })},
                    ],
                    {cancelable: false}
                )
            });
    }


};