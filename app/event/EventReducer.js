const INITIAL_STATE = {
    data: [],
    message: '',
    status: false,
    isLoading: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'EVENT_LOAD_DATA':
            return {
                ...state,
                isLoading: true
            };
        case 'EVENT_SUCCESS':
            return {
                ...state,
                status: true,
                data: action.payload.events,
                isLoading: false
            };
        case 'EVENT_FAILED':
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
};