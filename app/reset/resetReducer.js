const INITIAL_STATE = {
    loading: false,
    message: '',
    success:true,
    resetSuccess:false,
};
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'RESET_ACTION_LOADING':
            return {
                ...state,
                loading: true,
                success: true,
            };
        case 'RESET_ACTION_FAILED':
            return {
                ...state,
                loading: false,
                success: false,
                message:action.payload,
                resetSuccess:action.payload.success,
            };
        case 'RESET_ACTION_SUCCESS':
            return {
                ...state,
                loading: false,
                message: '',
                success:true,
                resetSuccess:action.payload.success,
            };
        default:
            return state;
    }
};