/**
 * Created by jaskkit on 1/20/2018 AD.
 */
import React, { Component } from 'react'
import { Text, View, SafeAreaView, StyleSheet, TextInput, Keyboard, TouchableOpacity, Image, Platform, Alert, ActivityIndicator } from 'react-native'
import { Form, Item, Input, Label, Icon, Button } from 'native-base'
import { NavigationActions } from "react-navigation";
import * as actions from './resetAction';
import { connect } from 'react-redux'
import { bindActionCreators } from "redux";
import { color, fontSize } from '../utills/color';
import { Strings } from '../utills/StringConfig';
import Loading from '../components/Loading';
import { Medium, Light } from '../utills/Fonts';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
const duration = 500;
const ic_back = require('../assets/images/ic_back.png');

let isVaildText = false;

class resetpassword extends Component {
    constructor(props) {
        super(props);
        this.state = { ref_code: '', password: '', new_password: '', isVaild: false };
    }

    componentDidMount() {
        this.email = this.props.navigation.state.params.email
    }

    backPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    onRefChange(text) {
        this.setState({ ref_code: text });
        if (this.state.ref_code.length > 4 && this.state.password.length > 2 && this.state.new_password.length > 2) {
            this.isVaildText = true;
        } else {
            this.isVaildText = false;
        }
    }

    onNewPassChange(text) {
        this.setState({ password: text });
        if (this.state.ref_code.length > 4 && this.state.password.length > 2 && this.state.new_password.length > 2) {
            this.isVaildText = true;
        } else {
            this.isVaildText = false;
        }
    }

    onConNewPassChange(text) {
        this.setState({ new_password: text });
        if (this.state.ref_code.length > 4 && this.state.password.length > 2 && this.state.new_password.length > 2) {
            this.isVaildText = true;
        } else {
            this.isVaildText = false;
        }
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.resetpassword.resetSuccess) {
            const resetAction = NavigationActions.reset({
                index: 0,
                key: null,
                actions: [NavigationActions.navigate({ routeName: 'login' })],
            });
            this.props.navigation.dispatch(resetAction)
        } else if (!nextProps.resetpassword.success) {
            Alert.alert(
                Strings.Error,
                nextProps.resetpassword.message,
                [
                    { text: Strings.OK, onPress: () => '' },
                ],
                { cancelable: false }
            )
        }
    }

    resetPress() {
        Keyboard.dismiss();
        if (this.validation()) {
            this.props.actions.resetPassword(this.state.ref_code, this.state.password, this.props.navigation.state.params.email);
        }
    }

    validation() {
        if (this.state.ref_code.length === 0) {
            Alert.alert(
                Strings.Error,
                Strings.ref_code,
                [
                    { text: Strings.OK, onPress: () => '' },
                ],
                { cancelable: false }
            )
            return false;
        }
        if (this.state.password.length === 0) {
            Alert.alert(
                Strings.Error,
                Strings.password_is_required,
                [
                    { text: Strings.OK, onPress: () => '' },
                ],
                { cancelable: false }
            )
            return false;
        }
        if (this.state.password.length < 4) {
            Alert.alert(
                Strings.Error,
                Strings.password_less_than,
                [
                    { text: Strings.OK, onPress: () => '' },
                ],
                { cancelable: false }
            )
            return false;
        }
        if (this.state.new_password.length === 0) {
            Alert.alert(
                Strings.Error,
                Strings.confirm_password,
                [
                    { text: Strings.OK, onPress: () => '' },
                ],
                { cancelable: false }
            )
            return false;
        }
        if (this.state.password.length != this.state.new_password.length) {
            Alert.alert(
                Strings.Error,
                Strings.confirm_password_not_matched,
                [
                    { text: Strings.OK, onPress: () => '' },
                ],
                { cancelable: false }
            )
            return false;
        }
        return true;
    }

    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container}>
                    <Loading loading={this.props.resetpassword.loading} />
                    <KeyboardAwareScrollView style={styles.keyboard} >
                        <View style={styles.containerActionBar}>
                            <TouchableOpacity onPress={this.backPress.bind(this)} style={{ flex: 0.1 }}>
                                <Image source={ic_back}
                                    style={styles.btnImage} resizeMode='contain' />
                            </TouchableOpacity>
                            <View style={{ flex: 0.8 }}>
                                <Text style={[styles.textTitleActionBar, { textAlign: 'center' }]}>{Strings.reset_password}</Text>
                            </View>
                        </View>
                        <View style={styles.formContainer}>

                            <Text style={[styles.textTitle, { fontFamily: Medium.font }]}>
                                {Strings.Please_enter_reset_pass}
                            </Text>
                            <View style={{ marginTop: 30 }}>
                                <Item regular>
                                    <Input placeholder={Strings.Reference_Code}
                                        placeholderTextColor={color.white}
                                        value={this.state.ref_code}
                                        onChangeText={this.onRefChange.bind(this)}
                                        style={styles.textInput} />
                                </Item>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Item regular>
                                    <Input placeholder={Strings.New_Password}
                                        placeholderTextColor={color.white}
                                        secureTextEntry
                                        value={this.state.password}
                                        onChangeText={this.onNewPassChange.bind(this)}
                                        style={styles.textInput} />
                                </Item>
                            </View>
                            <View style={{ marginTop: 10 }}>
                                <Item regular>
                                    <Input placeholder={Strings.Confirm_New_Password}
                                        placeholderTextColor={color.white}
                                        secureTextEntry
                                        value={this.state.new_password}
                                        onChangeText={this.onConNewPassChange.bind(this)}
                                        style={styles.textInput} />
                                </Item>
                            </View>
                            <View style={{ marginTop: 20,marginBottom:20 }}>
                                <Button
                                    onPress={this.resetPress.bind(this)}
                                    style={this.isVaildText ? styles.buttonSuscess : styles.button}
                                    block>
                                    {this.renderButton()}
                                </Button>
                            </View>

                        </View>
                    </KeyboardAwareScrollView>
                </View>
            </SafeAreaView>
        );
    }
    renderButton() {
        if (this.props.resetpassword.loading) {
            return (
                <ActivityIndicator
                    size="small" color="white"
                    animating={true} />
            );
        }
        else {
            return (
                <Text style={[styles.textButton, { fontFamily: Medium.font }]}>{Strings.confirm}</Text>
            );
        }

    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.primary
    },
    containerActionBar: {
        flexDirection: 'row',
        marginTop: Platform.OS === 'ios' ? 20 : 10,
        marginLeft: 10,
    },
    textInput: {
        fontFamily: Light.font,
        color: color.white
    },
    formContainer: {
        paddingHorizontal: 30,
        marginTop: '20%'
    },
    textBoxBtnHolder: {
        position: 'relative',
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    textTitle: {
        color: color.white,
        fontSize: fontSize.large,
        textAlign: 'center',
        alignSelf: 'stretch',
    },
    textDetail: {
        marginTop: 10,
        fontSize: fontSize.medium,
        alignSelf: 'stretch',
        color: color.white,
    },
    textButton: {
        fontSize: fontSize.large,
        color: 'white'
    },
    keyboard: {
        alignSelf: 'stretch',
        // marginRight: 40,
        // marginLeft: 40
      },
    btnImage: {
        height: 28,
        width: 28
    },
    textTitleActionBar: {
        color: color.white,
        fontSize: fontSize.large,
        fontFamily: Medium.font,
    },
    button: {
        backgroundColor: color.buttonLoginUnSuscess
    },
    buttonSuscess: {
        backgroundColor: color.buttonLoginSuscess
    }
});
const mapStateToProps = ({ resetpassword }) => {
    return {
        resetpassword
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(resetpassword);
