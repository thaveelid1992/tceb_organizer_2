/**
 * Created by jaskkit on 1/21/2018 AD.
 */
/**
 * Created by jaskkit on 1/21/2018 AD.
 */
import axios from 'axios'
import {baseUrl} from '../navigation/AppNavigation';
export const resetPassword = (ref_code, password,email) => {
    return (dispatch) => {
        dispatch({
            type: 'RESET_ACTION_LOADING',
        })
        axios.post(`${baseUrl}/resetPassword`, {
            reference_code: ref_code,
            password: password,
            email: email
        }, {
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(function (response) {
            if (response.data.success) {
                dispatch({
                    type: 'RESET_ACTION_SUCCESS',
                    payload: response.data
                })
            } else {
                dispatch({
                    type: 'RESET_ACTION_FAILED',
                    payload: response.data.message
                })
            }
        }).catch(function (error) {
            dispatch({
                type: 'RESET_ACTION_FAILED',
                payload: error.message
            })
        });
    }
}