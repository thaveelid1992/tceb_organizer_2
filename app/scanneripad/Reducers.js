const INITIAL_STATE = {
    data: {},
    message: '',
    status: false,
    callback: false,
    isLoading: false,
    isError: false,
    profile: {},
    isProfile: true,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'SCANNER_LOADING':
            return {
                ...state,
                isLoading: true,
                isProfile: false,
            };
        case 'SCANNER_SUCCESS':
            return {
                ...state,
                status: true,
                callback: true,
                data: action.payload,
                isLoading: false,
                isError: false,
            };
        case 'SCANNER_FAILED':
            return {
                ...state,
                status: false,
                callback: false,
                isLoading: false,
                message: action.payload,
                isError: true,
            };
        case 'SCANNER_PROFILE_SUCCESS':
            return {
                ...state,
                isProfile: true,
                callback: true,
                profile: action.payload,
                isLoading: false,
                isError: false,
            };
        case 'SCANNER_PROFILE_FAILED':
            return {
                ...state,
                isProfile: false,
                callback: true,
                isLoading: false,
                message: action.payload,
                isError: true,
            };
        case 'SCANNER_RESET':
            return {
                ...state,
                data: {},
                message: '',
                isProfile: false,
                status: false,
                callback: false,
                isLoading: false,
                isError: false,
                profile: {}
            };
        default:
            return state;
    }
};
