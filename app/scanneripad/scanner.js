import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    AsyncStorage,
    Image,
    Alert,
    TouchableOpacity,
    Dimensions, NativeModules, ActivityIndicator
} from 'react-native';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as actions from './Actions'
import {color} from '../utills/color';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Container, Header, Left, Body, Right, Title, Content, Button, Input} from 'native-base';
import {fontSize} from "../utills/color";
import {Light, Medium} from "../utills/Fonts";
import {Strings} from '../utills/StringConfig';

const back = require('../assets/images/arrow_back.png');
const ic_search = require('../assets/images/ic_search.png');
const {width, height} = Dimensions.get('window');
import {NavigationActions} from "react-navigation";
import {RNCamera} from 'react-native-camera';

type Props = {};
import ViewFinder from './ViewFinder';
import Approve from '../components/ipadAlert'
import Failed from '../components/FailedPopup';

class Search extends Component<Props> {
    constructor(props) {
        super(props);
        console.disableYellowBox = true;
        this.state = {
            isActiveState: true,
            flash: 'off',
            zoom: 0,
            autoFocus: 'on',
            depth: 0,
            type: 'front',
            ref_code: '',
            isShow: false,
            ModalFailed: false,
            profile: {}
        }
    }

    componentDidMount() {
        // AsyncStorage.getItem('User', (err, result) => {
        //     if (result) {
        //         const {api_token} = JSON.parse(result);
        //         this.props.actions.getProfile(api_token, '514469810');
        //     }
        // });
    }

    _changeView(html) {
        this.setState({isActiveState: true});
        this.props.actions.resetState();
        NativeModules.ChangeViewBridge.changeToNativeView(html);
    }

    onBackPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    onPressSearch() {
        this.props.navigation.navigate('search', {
            event: this.props.navigation.state.params.event,
            ticket: this.props.navigation.state.params.ticket
        })
    }

    onComfirm() {
        this.print(this.state.profile.ref_code)
        this.setState({isShow: false, profile: {}})
    }

    onCancel() {
        this.setState({isActiveState: true, isShow: false,profile: {}})
    }

    render() {
        if (!this.done) {
            const TopComponent = this.state.isActiveState ? RNCamera : RNCamera;
            return (
                <Container>
                    <Header style={{backgroundColor: color.primary}}>
                        <Approve
                            loading={this.state.isShow}
                            close={this.onCancel.bind(this)}
                            eventName={this.props.navigation.state.params.event.name}
                            name={this.state.profile.fullname}
                            phone={this.state.profile.tel}
                            id={this.state.profile.ref_code}
                            email={this.state.profile.email}
                            approve={this.onComfirm.bind(this)}/>
                        <Failed
                            loading={this.state.ModalFailed}
                            close={this.onFailedClose.bind(this)}
                            close_button={this.onFailedClose.bind(this)}
                            message={this.props.scanneripad.message}
                        />
                        <Left style={{flex: 1}}>
                            <Button transparent onPress={this.onBackPress.bind(this)}>
                                <Image source={back} resizeMode='contain' style={styles.titleIcon}/>
                            </Button>
                        </Left>
                        <Body>
                        <Title style={{
                            fontSize: 30,
                            fontFamily: Light.font,
                            color: 'white'
                        }}>Qr Code</Title>
                        </Body>
                        <Right>
                            <TouchableOpacity transparent onPress={this.onPressSearch.bind(this)}>
                                <Image source={ic_search} resizeMode='contain' style={styles.printerIcon}/>
                            </TouchableOpacity>
                        </Right>
                    </Header>
                    <TopComponent
                        onBarCodeRead={this.barcodeReceived.bind(this)}
                        type={this.state.type}
                        style={{flex: 1}}
                    >
                        <ViewFinder/>
                    </TopComponent>
                </Container>
            );
        } else {
            return (<View></View>);
        }

    }

    onFailedClose() {
        this.props.actions.resetState();
        this.setState({ModalFailed: false, isActiveState: true});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.scanneripad.isProfile && nextProps.scanneripad.callback) {
            // Alert.alert(nextProps.scanneripad.profile.ref_code);
            this.setState({profile: nextProps.scanneripad.profile, isShow: true});
            // Alert.alert(
            //     nextProps.scanneripad.profile.ref_code,
            //     nextProps.scanneripad.profile.fullname,
            //     [
            //         {
            //             text: 'CONFIRM', onPress: () => this.print(nextProps.scanneripad.profile.ref_code)
            //         },
            //         {
            //             text: 'CANCEL', onPress: () => this.setState({isActiveState: true})
            //         },
            //     ],
            //     {cancelable: this.props.actions.resetState()}
            // )
        }
        if (nextProps.scanneripad.status && nextProps.scanneripad.callback) {
            /// call printer and set new state

            let html = '';
            if (nextProps.scanneripad.data.is_vip == 1) {
                html = `<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
span{
    display:inline-block;
    border-bottom:3px solid black;
    padding-bottom:2px;
},
p {
   line-height : 1em; //relative to this element, 14px
   margin-bottom : 1em; //relative to this element, 14px
  },
  font.b {
  font-family: Helvetica, sans-serif;
}
</style>
<body>
<p><font size="10" class="b" face="Helvetica"><center><b>${nextProps.scanneripad.data.first_name}  ${nextProps.scanneripad.data.lastname.substring(0, 1).toUpperCase()}. (Speaker)</b></font></p>
<p><font size="8" class="b" face="Helvetica"><center><b>${nextProps.scanneripad.data.company}</b></font></p>
<center>
<img id='barcode'
            src="https://api.qrserver.com/v1/create-qr-code/?data=${nextProps.scanneripad.data.private_reference_number}&amp;size=100x100"
            alt=""
            title="HELLO"
            width="300"
            height="300" /></center>

</body>
</html>`
            } else {
                html = `<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
span{
    display:inline-block;
    border-bottom:3px solid black;
    padding-bottom:2px;
},
p {
   line-height : 1em; //relative to this element, 14px
   margin-bottom : 1em; //relative to this element, 14px
  },
  font.b {
  font-family: Helvetica, sans-serif;
}
</style>
<body>
<p><font size="10" class="b" face="Helvetica"><center><b>${nextProps.scanneripad.data.first_name} ${nextProps.scanneripad.data.last_name.substring(0, 1).toUpperCase()}.</b></font></p>
<p><font size="8" class="b" face="Helvetica"><center><b>${nextProps.scanneripad.data.company}</b></font></p>
<center>
<img id='barcode'
            src="https://api.qrserver.com/v1/create-qr-code/?data=${nextProps.scanneripad.data.private_reference_number}&amp;size=100x100"
            alt=""
            title="HELLO"
            width="300"
            height="300" /></center>

</body>
</html>`
            }

            if (nextProps.scanneripad.status) {
                this.setPrinter(html, nextProps.scanneripad.status, nextProps.scanneripad.callback);
            }
        }

        if (nextProps.scanneripad.isError) {
            this.setState({isActiveState: false, ModalFailed: true});
        }

    }

    print(ref) {
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                this.qrCodeScanner(api_token, ref);
            }
        });
    }


    setCancel() {
        this.setState({isActiveState: true});
        this.props.actions.resetState();
    }

    setPrinter(html, status, callback) {
        this._changeView(html);
    }

    qrCodeScanner(api_token, data) {
        this.props.actions.qrCodeScanner(api_token, data,this.props.navigation.state.params.zone_id)
    }

    barcodeReceived(e) {
        if (this.state.isActiveState) {
            this.setState({isActiveState: false});
            AsyncStorage.getItem('User', (err, result) => {
                if (result) {
                    const {api_token} = JSON.parse(result);
                    this.props.actions.getProfile(api_token, e.data)
                    // Alert.alert(
                    //     Strings.confirm,
                    //     ``,
                    //     [
                    //         {
                    //             text: 'OK', onPress: () => this.props.actions.getProfile(api_token, e.data)
                    //         },
                    //         {
                    //             text: 'Cancel', onPress: this.setCancel.bind(this)
                    //         },
                    //     ],
                    //     {cancelable: false}
                    // )
                    // this.props.actions.qrCodeScanner(api_token, e.data);
                }
            });
        }
    }

    register() {
        this.props.navigation.navigate('register', {
            event: this.props.navigation.state.params.event,
            ticket: this.props.navigation.state.params.ticket
        })
    }


}

const styles = StyleSheet.create({
    image: {
        width: width,
        height: height * 0.25,
        backgroundColor: 'grey'
    },
    textTitle: {
        fontSize: fontSize.large,
        color: color.textTitle
    },
    textSubtitle: {
        fontSize: fontSize.small,
        color: 'gray',
        fontFamily: Light.font
    },
    textDetail: {
        fontSize: fontSize.medium,
        color: color.textBlue
    },
    line: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        marginTop: 5,
        marginBottom: 5,
        width: '12%',
    },
    space: {
        marginTop: 1.5,
        marginBottom: 1.5
    }
    , titleIcon: {
        width: width * 0.035,
        height: height * 0.028,
        tintColor: '#FFFFFF'
    },
    cardItem: {
        padding: 10,
        margin: 30,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#d6d7da',
    },
    textItemStyle: {
        fontSize: 20,
        color: 'black',
    },
    ButtonStyle: {
        borderRadius: 12,
        borderWidth: 1,
        borderColor: '#d6d7da',
        backgroundColor: color.primary,
        width: width * 0.4,
        height: height * 0.08
    },
    linemenu: {
        borderBottomColor: '#D8D8D8',
        borderBottomWidth: 0.5,
        marginTop: 5,
        marginBottom: 5,
    },
    searchIcon: {
        width: width * 0.05,
        height: width * 0.05,
        alignItems: 'center',
        alignSelf: 'center',
        tintColor: 'white'
    },
    printerIcon: {
        width: width * 0.033,
        height: height * 0.033,
        marginRight: 10,
        tintColor: 'white'
    },
});

const mapStateToProps = ({scanneripad}) => {
    return {
        scanneripad
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Search);
