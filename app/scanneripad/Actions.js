import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl, printerUrl} from '../navigation/AppNavigation';

export const qrCodeScanner = (token, private_reference_number,zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'SCANNER_LOADING',
        })
        axios.post(`${printerUrl}/ScanQrCode`, {
            private_reference_number: private_reference_number,
            zone_id: zone_id,
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'SCANNER_SUCCESS',
                        payload: response.data.data
                    })
                } else {
                    dispatch({
                        type: 'SCANNER_FAILED',
                        payload: response.data.message
                    })
                }

            })
            .catch((error) => {
                dispatch({
                    type: 'SCANNER_FAILED',
                    payload: error.message
                })
            });
    }


};
export const getProfile = (token, private_reference_number) => {
    return (dispatch) => {
        dispatch({
            type: 'SCANNER_LOADING',
        })
        axios.get(`${baseUrl}/getProfileFromQr?private_reference_number=${private_reference_number}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                if (response.data.success) {
                    response.data.data.ref_code = private_reference_number
                    dispatch({
                        type: 'SCANNER_PROFILE_SUCCESS',
                        payload: response.data.data
                    })
                } else {
                    Alert.alert(
                        'Error',
                        response.data.message,
                        [
                            {
                                text: 'OK', onPress: () => dispatch({
                                    type: 'SCANNER_PROFILE_FAILED',
                                    payload: 'Invalid QR CODE'
                                })
                            },
                        ],
                        {cancelable: false}
                    )
                }

            })
            .catch((error) => {
                Alert.alert(
                    'Error',
                    error.message,
                    [
                        {
                            text: 'OK', onPress: () => dispatch({
                                type: 'SCANNER_PROFILE_FAILED',
                                payload: 'Invalid QR CODE'
                            })
                        },
                    ],
                    {cancelable: false}
                )
            });
    }


};
export const resetState = () => {
    return (dispatch) => {
        dispatch({
            type: 'SCANNER_RESET',
        })
    }
};
