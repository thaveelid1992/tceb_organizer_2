import React, {Component} from 'react'
import {
    Text,
    StyleSheet,
    Dimensions,
    FlatList,
    View,
    Platform,
    Image,
    TouchableOpacity,
    ActivityIndicator,
    AsyncStorage,
    Alert,
    ImageBackground,
    SafeAreaView,
} from 'react-native';
import {
    Container,
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title,
    Subtitle,
    Content,
    Picker,
    Form,
    Input
} from 'native-base';
import {color, fontSize} from '../utills/color';
import FastImage from 'react-native-fast-image';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Col, Row, Grid} from "react-native-easy-grid";
import {Strings} from '../utills/StringConfig';
import {Medium, Light} from '../utills/Fonts';
import {NavigationActions} from "react-navigation";

const ic_agenda = require('../assets/images/ic_agenda.png')
const ic_floorplan = require('../assets/images/ic_floorplan.png')
const ic_qr = require('../assets/images/ic_qr.png')
const ic_list = require('../assets/images/ic_list.png')
const ic_qrDis = require('../assets/images/ic_qr_dis.png')
const ic_listDis = require('../assets/images/ic_list_dis.png')
const ic_dropdown = require('../assets/images/ic_dropdown.png');
const ic_qr_code = require('../assets/images/ic_button_qr.png');
const ic_back = require('../assets/images/ic_back.png');
import ModalFilterPicker from 'react-native-modal-filter-picker'
import * as actions from './CheckinAction'


const {width, height} = Dimensions.get('window');

class CheckinScreen extends Component {

    constructor(props) {
        super(props);
        this.state = {
            visible: false,
            picked: {key: null, label: Strings.select_check_point_zone},
            kiosk_mode: false,
            search: ''
        };
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        const {params} = this.props.navigation.state;
        let data = params.obj;
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token, configkiosk} = JSON.parse(result);
                this.setState({kiosk_mode: configkiosk});
                if (configkiosk) {
                    this.props.actions.getBackground(data.id, api_token)
                }
                this.props.actions.getEventZone(data.id, api_token)
            }
        });
    }

    componentWillReceiveProps(nextProps) {
        if (!nextProps.checkin.isLoading && nextProps.checkin.isGetZone) {
            if (nextProps.checkin.data[1]) {
                this.setState({
                    picked: nextProps.checkin.data[1],
                    visible: false
                })
            }
        }
        if (nextProps.checkin.isChange) {
            this.setState({kiosk_mode: nextProps.checkin.isMode})
            this.props.actions.resSetChangeMode();
        }

    }

    render() {
        const {params} = this.props.navigation.state;
        let data = params.obj;
        const {id, name, image_url} = data;
        return (
            <Container>
                {this.state.kiosk_mode ?
                    <Content style={{backgroundColor: 'white'}}>
                        {/*<ImageBackground source={{uri:'https://www.tomorrowland.com/src/Frontend/Themes/tomorrowland/Core/Layout/images/timeline/2014-1.jpg'}}*/}
                        {/*style={{width: width,height:height}}*/}
                        {/*resizeMode='stretch'>*/}
                        {/*</ImageBackground>*/}

                        <FastImage
                            style={{width: width, height: height, backgroundColor: color.primary}}
                            source={{
                                uri: this.props.checkin.background,
                                priority: FastImage.priority.normal,
                            }}
                            resizeMode={FastImage.resizeMode.stretch}
                        />
                        <View style={{
                            position: 'absolute',
                            flex: 1,
                            width: width,
                            height: height,
                            backgroundColor: 'rgba(52, 52, 52, 0.3)'
                        }}>
                            <View style={{alignSelf: 'center', marginTop: height * 0.01}}>
                                <Grid>
                                    <Row size={15}>
                                        <View style={{flex: 1, justifyContent: 'center'}}>
                                            <Text style={{
                                                color: 'white',
                                                fontSize: width * 0.05,
                                                fontFamily: Medium.font,
                                                textAlign: 'center',
                                            }}
                                                  numberOfLines={3} ellipsizeMode={'tail'}>{data.name}</Text>
                                        </View>
                                    </Row>
                                    <Row size={10}>
                                        <View style={{
                                            width: width,
                                            flex: 1,
                                            flexDirection: 'row',
                                            marginLeft: width * 0.2,
                                            marginRight: width * 0.2,
                                            marginTop: 20,
                                            marginBottom: 10,
                                            backgroundColor: 'white',
                                            padding: 5,
                                            borderRadius: 12
                                        }}>
                                            <View style={{flex: 0.15, justifyContent: 'center'}}>
                                                <Image source={ic_agenda} style={{
                                                    width: width * 0.04,
                                                    height: width * 0.04,
                                                    marginLeft: 10,
                                                    tintColor: 'black'
                                                }}/>
                                            </View>
                                            <View style={{flex: 0.83, justifyContent: 'center'}}>
                                                <TouchableOpacity onPress={this.onShow}>
                                                    <Text numberOfLines={2}
                                                          style={[styles.textSelected, {
                                                              color: 'black',
                                                              fontSize: width * 0.04,
                                                          }]}>{this.state.picked.label}</Text>
                                                </TouchableOpacity>
                                                <ModalFilterPicker
                                                    visible={this.state.visible}
                                                    onSelect={this.onSelect}
                                                    onCancel={this.onCancel}
                                                    options={this.props.checkin.data}
                                                    titleTextStyle={{color: 'black', fontSize: 14, textAlign: 'center'}}
                                                />
                                            </View>
                                            <View style={{flex: 0.12, justifyContent: 'center'}}>
                                                <TouchableOpacity onPress={this.onShow}>
                                                    <Image source={ic_dropdown} style={{
                                                        width: width * 0.03,
                                                        height: width * 0.03,
                                                        marginLeft: 10,
                                                        tintColor: 'black'
                                                    }}
                                                           resizeMode={'contain'}/>
                                                </TouchableOpacity>
                                            </View>
                                        </View>
                                        {/*<Col size={25} style={{justifyContent: 'center'}}>*/}
                                        {/*<Image source={ic_agenda} style={styles.iconStyle}/>*/}
                                        {/*</Col>*/}
                                        {/*<Col size={65} style={{justifyContent: 'center'}}>*/}
                                        {/*<TouchableOpacity onPress={this.onShow}>*/}
                                        {/*<Text numberOfLines={2} style={styles.textSelected}>{this.state.picked.label}</Text>*/}
                                        {/*</TouchableOpacity>*/}
                                        {/*<ModalFilterPicker*/}
                                        {/*visible={this.state.visible}*/}
                                        {/*onSelect={this.onSelect}*/}
                                        {/*onCancel={this.onCancel}*/}
                                        {/*options={this.props.checkin.data}*/}
                                        {/*titleTextStyle={{color: 'black', fontSize: 14, textAlign: 'center'}}*/}
                                        {/*/>*/}
                                        {/*</Col>*/}
                                        {/*<Col size={10} style={{justifyContent: 'center'}}>*/}
                                        {/*<TouchableOpacity onPress={this.onShow}>*/}
                                        {/*<Image source={ic_dropdown} style={{width: 14, height: 10}}*/}
                                        {/*resizeMode={'stretch'}/>*/}
                                        {/*</TouchableOpacity>*/}
                                        {/*</Col>*/}
                                    </Row>
                                    <Row size={25}>
                                        <View style={{width: width, justifyContent: 'center', marginTop: 10}}>
                                            <TouchableOpacity onPress={this.onQrPress.bind(this)}>
                                                <Image source={ic_qr_code}
                                                       style={{
                                                           width: width * 0.3,
                                                           height: width * 0.3,
                                                           alignSelf: 'center',
                                                       }} resizeMode='contain'/>
                                            </TouchableOpacity>
                                        </View>
                                    </Row>
                                    <Row size={60}>
                                        <View style={{width: width, marginTop: 10}}>
                                            <Text style={{
                                                color: 'white',
                                                fontSize: width * 0.04,
                                                fontFamily: Medium.font,
                                                textAlign: 'center'
                                            }}>Or Search</Text>
                                            <View style={[styles.line, {
                                                marginLeft: width * 0.2,
                                                marginRight: width * 0.2,
                                                marginTop: 20,
                                                marginBottom: 20
                                            }]}/>
                                            <Text style={{
                                                color: 'white',
                                                fontSize: width * 0.04,
                                                fontFamily: Medium.font,
                                                textAlign: 'center'
                                            }}>Enter Keyword such as Name,LastName</Text>

                                            <View>
                                                <View style={{
                                                    flexDirection: 'row',
                                                    backgroundColor: color.white,
                                                    borderRadius: 10,
                                                    borderWidth: 4,
                                                    borderColor: color.white,
                                                    marginTop: 10,
                                                    marginBottom: 20,
                                                    marginHorizontal: 10
                                                    ,
                                                    marginLeft: width * 0.1,
                                                    marginRight: width * 0.1
                                                }}>
                                                    <Input style={[styles.textBox, {fontSize: width * 0.04}]}
                                                           placeholder={Strings.search}
                                                           placeholderTextColor={color.textTitle}
                                                           onChangeText={(text) => this.setSearch(text)}
                                                           value={this.state.search}/>
                                                    <Image source={require('../assets/images/ic_search_gary.png')}
                                                           style={{
                                                               width: width * 0.04,
                                                               height: width * 0.04,
                                                               alignItems: 'center',
                                                               alignSelf: 'center',
                                                               justifyContent: 'center',
                                                               marginHorizontal: 4
                                                           }}/>
                                                </View>
                                                <TouchableOpacity style={{
                                                    flexDirection: 'row',
                                                    backgroundColor: color.white,
                                                    borderRadius: 10,
                                                    borderWidth: 4,
                                                    borderColor: color.white,
                                                    marginTop: 10,
                                                    marginBottom: 20,
                                                    marginHorizontal: 10,
                                                    justifyContent: 'center',
                                                    marginLeft: width * 0.1,
                                                    marginRight: width * 0.1
                                                }} onPress={this.onSearchKeiosk.bind(this)}>
                                                    <Text style={{
                                                        color: 'black',
                                                        fontSize: width * 0.04,
                                                        fontFamily: Medium.font,
                                                        textAlign: 'center'
                                                    }}>Search</Text>
                                                </TouchableOpacity>
                                            </View>
                                            <Text style={{
                                                color: 'white',
                                                fontSize: width * 0.04,
                                                fontFamily: Medium.font,
                                                textAlign: 'center'
                                            }}>Or</Text>
                                            <View style={[styles.line, {
                                                marginLeft: width * 0.2,
                                                marginRight: width * 0.2,
                                                marginTop: 10,
                                                marginBottom: 10
                                            }]}/>
                                            <TouchableOpacity style={{
                                                flexDirection: 'row',
                                                backgroundColor: color.white,
                                                borderRadius: 10,
                                                borderWidth: 4,
                                                borderColor: color.white,
                                                marginTop: 10,
                                                marginBottom: 20,
                                                marginHorizontal: 10,
                                                justifyContent: 'center',
                                                marginLeft: width * 0.1,
                                                marginRight: width * 0.1
                                            }} onPress={this.onRegister.bind(this)}>
                                                <Text style={{
                                                    color: 'black',
                                                    fontSize: width * 0.04,
                                                    fontFamily: Medium.font,
                                                    textAlign: 'center'
                                                }}>Registration Now</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </Row>
                                </Grid>

                            </View>

                        </View>
                        <View style={{position: 'absolute'}}>
                            <TouchableOpacity onPress={this.backPress.bind(this)}
                                              style={{marginTop: height * 0.043, marginLeft: 10}}>
                                <Image source={ic_back}
                                       style={styles.btnImage} resizeMode='contain'/>
                            </TouchableOpacity>
                        </View>
                    </Content>
                    : <Content style={{backgroundColor: 'white'}}>
                        <SafeAreaView style={{backgroundColor: color.primary}}>
                            <View style={{backgroundColor: 'white'}}>
                                <View style={styles.contentImage}>
                                    <View style={styles.backgroundContainer}>
                                        <FastImage
                                            style={styles.image}
                                            source={{
                                                uri: image_url,
                                                priority: FastImage.priority.normal,
                                            }}
                                            resizeMode={FastImage.resizeMode.cover}
                                        />
                                    </View>
                                    <View style={styles.overlay}>
                                        <TouchableOpacity onPress={this.backPress.bind(this)}>
                                            <Image source={ic_back}
                                                   style={styles.btnImage} resizeMode='contain'/>
                                        </TouchableOpacity>
                                        <View style={{flex: 1}}>
                                            {/*<Image source={ic_agenda} style={[styles.iconStyle]}/>*/}
                                            <Grid>
                                                <Row size={1} style={{justifyContent: 'center'}}>
                                                </Row>
                                                <Row size={4} style={{justifyContent: 'center'}}>
                                                    <Text style={styles.textTitle}>{name}</Text>
                                                </Row>
                                            </Grid>
                                        </View>
                                    </View>
                                </View>
                                <View style={{
                                    flex: 1,
                                    marginLeft: '10%',
                                    marginRight: '10%',
                                    marginTop: 30,
                                    marginBottom: 30
                                }}>
                                    <Text style={styles.textSubtitle}>{Strings.ticket_check}</Text>
                                    <View style={styles.line}></View>
                                    <View style={styles.cardActive}>
                                        {/*select Zone*/}
                                        {this.renderZone()}

                                    </View>
                                    <TouchableOpacity onPress={this.onQrPress.bind(this)}
                                                      style={[this.state.picked.key != null ? styles.cardDeActive : styles.cardDeActive, {marginTop: 10}]}
                                    >
                                        <Grid>
                                            <Col size={25} style={{justifyContent: 'center'}}>
                                                <Image source={this.state.picked.key != null ? ic_qr : ic_qr}
                                                       style={styles.iconStyle}/>
                                            </Col>
                                            <Col size={75} style={{justifyContent: 'center'}}>
                                                <Text style={styles.textSubtitle}>{Strings.Check_In_By_QR_Code}</Text>
                                            </Col>
                                        </Grid>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={this.onCheckInList.bind(this)}
                                                      style={[this.state.picked.key ? styles.cardDeActive : styles.cardDeActive, {marginTop: 10}]}
                                    >
                                        <Grid>
                                            <Col size={25} style={{justifyContent: 'center'}}>
                                                <Image source={this.state.picked.key ? ic_list : ic_list}
                                                       style={styles.iconStyle}/>
                                            </Col>
                                            <Col size={75} style={{justifyContent: 'center'}}>
                                                <Text style={styles.textSubtitle}>{Strings.Check_In_By_List}</Text>
                                            </Col>
                                        </Grid>
                                    </TouchableOpacity>
                                    {/*select Report*/}
                                    {this.renderReport()}
                                    {this.renderRegister()}
                                </View>
                                {/*select Info*/}
                                {this.renderInfo()}
                            </View>
                        </SafeAreaView>
                    </Content>}

            </Container>

        );
    }

    renderZone() {
        if (this.props.checkin.isLoading) {
            return (
                <Grid>
                    <Col style={{justifyContent: 'center'}}>
                        <ActivityIndicator
                            size="small" color="white"
                            animating={true}/>
                    </Col>
                </Grid>
            );
        } else {
            return (
                <Grid>
                    <Col size={25} style={{justifyContent: 'center'}}>
                        <Image source={ic_agenda} style={styles.iconStyle}/>
                    </Col>
                    <Col size={65} style={{justifyContent: 'center'}}>
                        <TouchableOpacity onPress={this.onShow}>
                            <Text numberOfLines={2} style={styles.textSelected}>{this.state.picked.label}</Text>
                        </TouchableOpacity>
                        <ModalFilterPicker
                            visible={this.state.visible}
                            onSelect={this.onSelect}
                            onCancel={this.onCancel}
                            options={this.props.checkin.data}
                            titleTextStyle={{color: 'black', fontSize: 14, textAlign: 'center'}}
                        />
                    </Col>
                    <Col size={10} style={{justifyContent: 'center'}}>
                        <TouchableOpacity onPress={this.onShow}>
                            <Image source={ic_dropdown} style={{width: 14, height: 10}}
                                   resizeMode={'stretch'}/>
                        </TouchableOpacity>
                    </Col>
                </Grid>
            );
        }
    }

    renderReport() {
        if (!this.state.kiosk_mode) {
            return (
                <View>
                    <TouchableOpacity onPress={this.onReport.bind(this)}
                                      style={[this.state.picked.key ? styles.cardDeActive : styles.cardDeActive, {marginTop: 10}]}>
                        <Grid>
                            <Col size={25} style={{justifyContent: 'center'}}>
                                <Image source={this.state.picked.key ? ic_list : ic_list}
                                       style={styles.iconStyle}/>
                            </Col>
                            <Col size={75} style={{justifyContent: 'center'}}>
                                <Text style={styles.textSubtitle}>{Strings.checkin_report_menu}</Text>
                            </Col>
                        </Grid>
                    </TouchableOpacity>
                </View>
            );
        }
    }

    setSearch(text) {
        this.setState({search: text})
    }

    renderRegister() {
        if (this.state.kiosk_mode) {
            return (
                <View>
                    <TouchableOpacity onPress={this.onRegister.bind(this)}
                                      style={[this.state.picked.key ? styles.cardDeActive : styles.cardDeActive, {marginTop: 10}]}>
                        <Grid>
                            <Col size={25} style={{justifyContent: 'center'}}>
                                <Image source={this.state.picked.key ? ic_list : ic_list}
                                       style={styles.iconStyle}/>
                            </Col>
                            <Col size={75} style={{justifyContent: 'center'}}>
                                <Text style={styles.textSubtitle}>{Strings.register_menu}</Text>
                            </Col>
                        </Grid>
                    </TouchableOpacity>
                </View>
            );
        }else{
            return (
                <View>
                    <TouchableOpacity onPress={this.onRegister.bind(this)}
                                      style={[this.state.picked.key ? styles.cardDeActive : styles.cardDeActive, {marginTop: 10}]}>
                        <Grid>
                            <Col size={25} style={{justifyContent: 'center'}}>
                                <Image source={this.state.picked.key ? ic_list : ic_list}
                                       style={styles.iconStyle}/>
                            </Col>
                            <Col size={75} style={{justifyContent: 'center'}}>
                                <Text style={styles.textSubtitle}>{Strings.registration_mark}</Text>
                            </Col>
                        </Grid>
                    </TouchableOpacity>
                </View>
            );
        }
    }

    renderInfo() {
        if (!this.state.kiosk_mode) {
            return (
                <View style={{
                    flex: 1,
                    marginLeft: '10%',
                    marginRight: '10%',
                    marginTop: 10,
                    marginBottom: 30
                }}>
                    <Text style={styles.textSubtitle}>{Strings.Event_Information}</Text>
                    <View style={styles.line}></View>
                    <TouchableOpacity onPress={this.onPressAgenda.bind(this)}>
                        <View style={[styles.cardDeActive]}>
                            <Grid>
                                <Col size={25} style={{justifyContent: 'center'}}>
                                    <Image source={ic_agenda} style={styles.iconStyle}/>
                                </Col>
                                <Col size={75} style={{justifyContent: 'center'}}>
                                    <Text style={styles.textSubtitle}>{Strings.Agenda}</Text>
                                </Col>
                            </Grid>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={this.onPressFloorPlan.bind(this)}>
                        <View style={[styles.cardDeActive, {marginTop: 10}]}>
                            <Grid>
                                <Col size={25} style={{justifyContent: 'center'}}>
                                    <Image source={ic_floorplan} style={styles.iconStyle}
                                           resizeMode='cover'/>
                                </Col>
                                <Col size={75} style={{justifyContent: 'center'}}>
                                    <Text style={styles.textSubtitle}>{Strings.Floor_Plan}</Text>
                                </Col>
                            </Grid>
                        </View>
                    </TouchableOpacity>
                </View>
            );
        }
    }

    backPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    onShow = () => {
        this.setState({visible: true});
    }

    onSelect = (picked) => {
        this.setState({
            picked: picked,
            visible: false
        })
    }

    onCancel = () => {
        this.setState({
            visible: false
        });
    }

    onRegister() {
        if (this.state.picked.key == null) {
            Alert.alert(
                Strings.err__zone_title,
                Strings.pls_chose_zone,
                [
                    {text: Strings.OK, onPress: () => ''},
                ],
                {cancelable: false}
            )
        } else {
            const {params} = this.props.navigation.state;
            if (this.state.kiosk_mode) {

                // this.props.navigation.navigate('ticket', {obj: params.obj,zoneid:this.state.picked.key})
                this.props.navigation.navigate('register', {
                    event: params.obj,
                    zoneid: this.state.picked.key,
                    zone_label: this.state.picked.label
                })
            } else {
                // this.props.navigation.navigate('ticket', {obj: params.obj,zoneid:this.state.picked.key})
                this.props.navigation.navigate('register_normal', {
                    event: params.obj,
                    zoneid: this.state.picked.key,
                    zone_label: this.state.picked.label
                })
            }

        }
    }

    onPressFloorPlan() {
        const {params} = this.props.navigation.state;
        this.props.navigation.navigate('floorplan', {data: {event: params.obj}})
    }

    onPressAgenda() {
        const {params} = this.props.navigation.state;
        data = params.obj;
        this.props.navigation.navigate('agenda', {data: data})
    }

    onReport() {
        if (this.state.picked.key == null) {
            Alert.alert(
                Strings.err__zone_title,
                Strings.pls_chose_zone,
                [
                    {text: Strings.OK, onPress: () => ''},
                ],
                {cancelable: false}
            )
        } else {
            const {params} = this.props.navigation.state;
            // this.props.navigation.navigate('Checkinlist', {data: {zone: this.state.picked, event: params.obj}})
            this.props.navigation.navigate('dashboard', {data: {data: {zone: this.state.picked, event: params.obj}}});
        }

    }

    onCheckInList() {
        if (this.state.picked.key == null) {
            Alert.alert(
                Strings.err__zone_title,
                Strings.pls_chose_zone,
                [
                    {text: Strings.OK, onPress: () => ''},
                ],
                {cancelable: false}
            )
        } else {
            const {params} = this.props.navigation.state;
            // this.props.navigation.navigate('Checkinlist', {data: {zone: this.state.picked, event: params.obj}})
            this.props.navigation.navigate('checkinlistv2', {
                data: {
                    zone: this.state.picked,
                    event: params.obj,
                    eventName: params.obj.name,
                    search: this.state.search
                }
            })
        }

    }

    onSearchKeiosk() {
        if (this.state.picked.key == null) {
            Alert.alert(
                Strings.err__zone_title,
                Strings.pls_chose_zone,
                [
                    {text: Strings.OK, onPress: () => ''},
                ],
                {cancelable: false}
            )
        } else {
            const {params} = this.props.navigation.state;
            // // this.props.navigation.navigate('Checkinlist', {data: {zone: this.state.picked, event: params.obj}})
            // this.props.navigation.navigate('checkinlistv2', {
            //     data: {
            //         zone: this.state.picked,
            //         event: params.obj,
            //         eventName: params.obj.name,
            //         search: this.state.search
            //     }
            // })
            this.props.navigation.navigate('search', {
                event: params.obj,
                search: this.state.search,
                zoneid: this.state.picked.key
            })
        }

    }

//     this.props.navigation.navigate('register', {
//     event: this.props.navigation.state.params.event,
//     ticket: this.props.navigation.state.params.ticket
// })

    onQrPress() {
        if (this.state.picked.key == null) {
            Alert.alert(
                Strings.err__zone_title,
                Strings.pls_chose_zone,
                [
                    {text: Strings.OK, onPress: () => ''},
                ],
                {cancelable: false}
            )
        } else {
            const {params} = this.props.navigation.state;
            if (!this.state.kiosk_mode) {
                this.props.navigation.navigate('Scanner', {
                    data: {
                        zone: this.state.picked,
                        eventName: params.obj.name,
                        event: params.obj
                    }
                })
            } else {
                this.props.navigation.navigate('scanneripad', {
                    zone_id: this.state.picked.key,
                    event: params.obj,
                    ticket: {}
                })
            }

        }
    }
}

const styles = StyleSheet.create({
    btnImage: {
        marginTop: Platform.OS === 'ios' ? 20 : 10,
        marginLeft: 10,
        height: 28,
        width: 28
    },
    contentImage: {
        width: width,
        height: height * 0.24,
    },
    backgroundContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
    },
    image: {
        width: width,
        height: height * 0.24,
        backgroundColor: 'grey'
    },
    textTitle: {
        fontSize: fontSize.large,
        color: 'white',
        textAlign: 'center'
    },
    textSelected: {
        fontSize: fontSize.medium,
        color: 'white'
    },
    textSubtitle: {
        fontSize: fontSize.medium,
        color: '#677897'
    },
    textDetail: {
        fontSize: fontSize.medium,
        color: color.textBlue
    },
    textBox: {
        fontSize: fontSize.medium,
        alignSelf: 'stretch',
        color: color.textTitle,
    },
    overlay: {
        flex: 1,
        backgroundColor: 'rgba(71,69,239,.5)'
    },
    line: {
        borderBottomColor: '#CBE3FA',
        borderBottomWidth: 1,
        marginTop: 10,
        marginBottom: 10,
    },
    space: {
        marginTop: 1.5,
        marginBottom: 1.5
    },
    cardActive: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.06,
        backgroundColor: color.primary,
        borderRadius: 8,
    },
    cardDisable: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.1)',
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.06,
        backgroundColor: '#D8D8D8',
        borderRadius: 8,
    },
    cardDeActive: {
        borderWidth: 1,
        borderColor: 'rgba(0,0,0,0.2)',
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.06,
        backgroundColor: '#fff',
        borderRadius: 8,
    },
    iconStyle: {
        width: 18,
        height: 18,
        alignItems: 'center',
        alignSelf: 'center',
    },
    dropDownStyle: {
        flex: 1,
        backgroundColor: 'red'
    }
});

const mapStateToProps = ({checkin}) => {
    return {
        checkin
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(CheckinScreen);
