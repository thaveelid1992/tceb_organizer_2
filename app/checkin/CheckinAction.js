import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';
import {Strings} from '../utills/StringConfig';

export const resSetChangeMode = (id, token) => {
    return (dispatch) => {
        dispatch({
            type: 'RESET_MODE',
        })
    }
}
export const getEventZone = (id, token) => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKIN_LOADING',
        })
        axios.get(`${baseUrl}/getAllZone?event_id=${id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        }).then((response) => {
            if (response.data.success) {
                dispatch({
                    type: 'CHECKIN_SUCCESS',
                    payload: response.data
                })
            } else {
                dispatch({
                    type: 'CHECKIN_FAILED',
                })
                Alert.alert(
                    Strings.Error,
                    response.data.message,
                    [
                        {text: Strings.OK, onPress: () => ''},
                    ],
                    {cancelable: false}
                )
            }

        })
            .catch((error) => {
                dispatch({
                    type: 'CHECKIN_FAILED',
                })
                // Alert.alert(
                //     Strings.Error,
                //     error.message,
                //     [
                //         {text: Strings.OK, onPress: () => ''},
                //     ],
                //     {cancelable: false}
                // )
            });
    }


};
export const getBackground = (id, token) => {
    return (dispatch) => {
        axios.get(`${baseUrl}/getBackground?event_id=${id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        }).then((response) => {
            if (response.data.success) {
                dispatch({
                    type: 'SCREEN_SUCCESS',
                    payload: response.data.data.image_url
                })
            } else {
                dispatch({
                    type: 'SCREEN_FALEID',
                })
            }

        })
            .catch((error) => {
                dispatch({
                    type: 'SCREEN_FALEID',
                })
            });
    }


};
