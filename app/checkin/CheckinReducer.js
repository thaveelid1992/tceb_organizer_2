const INITIAL_STATE = {
    data: [],
    isLoading: false,
    isGetZone: false,
    message: '',
    status: false,
    isMode: false,
    isChange: false,
    background: null
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'CHECKIN_LOADING':
            return {
                ...state,
                isLoading: true,
            };
        case 'CHECKIN_SUCCESS':
            return {
                ...state,
                isLoading: false,
                isGetZone: true,
                data: action.payload.zones
            };
        case 'CHECKIN_FAILED':
            return {
                ...state,
                isLoading: false,
                isGetZone: false,
            };
        case 'SET_SWITCH_MODE':
            return {
                ...state,
                isMode: action.payload,
                isChange: true
            };
        case 'SCREEN_SUCCESS':
            return {
                ...state,
                background: action.payload
            }
        case 'RESET_MODE':
            return {
                ...state,
                isChange: false
            };
        default:
            return state;
    }
};
