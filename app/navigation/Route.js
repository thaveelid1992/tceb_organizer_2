/**
 * Created by jaskkit on 12/12/2017 AD.
 */
import React from 'react'
import {StyleSheet, Image} from 'react-native'
import {TabNavigator, StackNavigator, TabBarBottom} from 'react-navigation'
import {color} from '../utills/color';
import Login from '../Login/LoginScreen';
import Scanner from '../scanner/ScanQrScreen';
import Forgot from '../Forget/ForgetScreen';
import Event from '../event/EventScreen';
import Setting from '../setting/Setting';
import Notification from '../notification/Notifaction';
import CheckIn from '../checkin/CheckinScreen';
import CheckInByList from '../checkinbylist/CheckInByListScreen';
import CheckInTicketType from '../checkintickettype/CheckinTicketTypeScreen';
import DashBoard from '../dashboard/DashboardScreen';
import FloorPlan from '../FloorPlan/FloorPlanScreen';
import Agenda from '../agenda/AgendaScreen';
import AgendaDetaill from '../agenda/AgendaDetail';
import contactus from '../setting/ContactUsScreen';
import ResetPass from '../reset/resetpassword';
import splash from '../splash/SplashScreen';
import policy from '../setting/PolicyScreen';
import term from '../setting/TermScreen';
import printer from '../printer/Printer';
import register from '../register/register';
import register_normal from '../register_normal/register';
import ticket from '../ticket/Ticket';
import scanneripad from '../scanneripad/scanner';
import search from '../search/Search';
import checkinlistv2 from '../checkinlistv2/CheckinListv2';

// export const MemberStack = StackNavigator({
//     main: {
//         screen: Member,
//     },
//     language: {
//         screen: language,
//     },
//     setting: {
//         screen: setting
//     },
//     memberlist: {
//         screen: MemberList
//     },
//     countries: {
//         screen: countries
//     },
//     cities: {
//         screen: cities
//     }
//
// });
export const EventStack = StackNavigator({
    Event: {
        screen: Event,
    },
    CheckIn: {
        screen: CheckIn,
    },
    Scanner: {
        screen: Scanner
    },
    Checkinlist: {
        screen: CheckInByList
    },
    printer: {
        screen: printer
    }
    // checkinlist: {
    //     screen: CheckInByList,
    //     navigationOptions: {
    //         headerMode: 'none',
    //         header: null,
    //         gesturesEnabled: false,
    //         headerLeft: null,
    //     }
    // },


}, {headerMode: 'screen'});
export const SettingStack = StackNavigator({
    Setting: {
        screen: Setting,
    },
}, {headerMode: 'screen'});
export const NotiStack = StackNavigator({
    Notification: {
        screen: Notification,
    },
}, {headerMode: 'screen'});
export const Tabs = TabNavigator({
    Main: {
        screen: EventStack,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={require('../assets/images/ic_main.png')}
                    style={[styles.icon, {tintColor: tintColor}]}
                    resizeMode='stretch'
                />
            )
        }
    },
    // Notification: {
    //     screen: NotiStack,
    //     navigationOptions: {
    //         tabBarIcon: ({tintColor}) => (
    //             <Image
    //                 source={require('../assets/images/ic_grid.png')}
    //                 style={[styles.icon, {tintColor: tintColor}]}
    //                 resizeMode='stretch'
    //             />
    //         )
    //     }
    //
    // },
    Setting: {
        screen: SettingStack,
        navigationOptions: {
            tabBarIcon: ({tintColor}) => (
                <Image
                    source={require('../assets/images/ic_setting.png')}
                    style={[styles.icon, {tintColor: tintColor}]}
                    resizeMode='stretch'
                />
            )
        }

    }
}, {
    tabBarComponent: TabBarBottom,
    swipeEnabled: false,
    tabBarPosition: 'bottom',
    animationEnabled: false,
    tabBarOptions: {
        showLabel: false,
        showIcon: true,
        activeTintColor: color.iconColor,
        inactiveTintColor: 'white',
        style: {
            backgroundColor: color.primary,
        },
    }
});
export const Navigator = StackNavigator({
    splash: {
        screen: splash,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    login: {
        screen: Login,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    forgot: {
        screen: Forgot,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    Tabs: {
        screen: Tabs,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    checkinlist: {
        screen: CheckInByList,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    checkintickettype: {
        screen: CheckInTicketType,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    dashboard: {
        screen: DashBoard,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    floorplan: {
        screen: FloorPlan,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    agenda: {
        screen: Agenda,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    AgendaDetaill: {
        screen: AgendaDetaill,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    contactus: {
        screen: contactus,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    resetpass: {
        screen: ResetPass,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    policy: {
        screen: policy,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    term: {
        screen: term,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    register: {
        screen: register,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    register_normal: {
        screen: register_normal,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    ticket: {
        screen: ticket,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    scanneripad: {
        screen: scanneripad,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    search: {
        screen: search,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
    checkinlistv2: {
        screen: checkinlistv2,
        navigationOptions: {
            headerMode: 'none',
            header: null,
            gesturesEnabled: false,
            headerLeft: null,
        }
    },
});
const styles = StyleSheet.create({
    icon: {
        width: 19,
        height: 19
    }
})
export default Navigator;
