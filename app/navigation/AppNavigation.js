/**
 * Created by jaskkit on 12/18/2017 AD.
 */
import React, {Component} from 'react'
import {Navigator} from './Route';
import {BackHandler, Platform, View, Text,Dimensions,Image,ActivityIndicator,StyleSheet} from 'react-native'
import {connect} from 'react-redux';
import firebase from '../config/firebase';
import {Medium, Light} from '../utills/Fonts';
import { color, fontSize } from '../utills/color'
const {width, height} = Dimensions.get('window');

let db_app = firebase.database().ref('config_app').child('application');
import {addNavigationHelpers, NavigationActions} from 'react-navigation';
import {
    createReduxBoundAddListener,
    createReactNavigationReduxMiddleware,
    createNavigationReducer,
} from 'react-navigation-redux-helpers';

export const AppNavigator = Navigator;
const middleware = createReactNavigationReduxMiddleware(
    "Tabs",
    state => state.nav,
);
const addListener = createReduxBoundAddListener("Tabs");
export let baseUrl = ''
export let policy = ''
export let term = ''
export let printerUrl = ''

class AppNavigation extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isFirstLaunch: false
        }
    }

    async componentDidMount() {
       // let status = await this.getConfig();
        BackHandler.addEventListener("hardwareBackPress", this.onBackPress);
        baseUrl = 'https://tceb-uat2.jenosize.com/api/v1';
        policy = 'https://tceb-uat2.jenosize.com/contents/policy';
        term = 'https://tceb-uat2.jenosize.com/contents/term';
        printerUrl = 'https://tceb-uat2.jenosize.com/api';
        this.setState({isFirstLaunch: true});
    }

    componentWillUnmount() {
        BackHandler.removeEventListener("hardwareBackPress", this.onBackPress);
    }

    getConfig = async () => {

        await db_app.once('value').then(function (snapshot) {
            // baseUrl = snapshot.val().organize_url;
            // policy = snapshot.val().policy;
            // term = snapshot.val().term;
            // printerUrl = snapshot.val().printerUrl;
            baseUrl = 'https://tceb-uat2.jenosize.com/api/v1';
            policy = 'https://tceb-uat2.jenosize.com/contents/policy';
            term = 'https://tceb-uat2.jenosize.com/contents/term';
            printerUrl = 'https://tceb-uat2.jenosize.com/api';
            this.setState({isFirstLaunch: true});


            return true;
        }.bind(this));
        // return true;
    };
    onBackPress = () => {
        const {dispatch, nav} = this.props;
        if (nav.routes[nav.index].routeName === 'login' || nav.routes[nav.index].routeName === 'splash') {
            return false;
        }
        if (nav.routes[nav.index].routeName === 'Tabs') {
            if (nav.routes[nav.index].routes[0].routes.length <= 1) {
                return false;
            }
        }
        if (nav.routes[0].routeName === 'splash' && nav.routes[1].routeName === 'login' && nav.routes[2].routeName === 'Tabs' && nav.index === 2) {
            return false;
        }
        dispatch(NavigationActions.back());
        return true;
    };

    render() {
        const {nav, dispatch} = this.props;
        if (this.state.isFirstLaunch) {
            return (
                <AppNavigator
                    navigation={addNavigationHelpers({
                        dispatch,
                        state: nav,
                        addListener,
                    })} // passing our navigation prop (consisting of dispatch and state) to AppNavigator.
                />
            )
        } else {
            return (
                <View style={{flex: 1, backgroundColor: color.white, justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{marginBottom: 30}}>
                        <View style={{
                            flex: 1,
                            alignSelf: 'center',
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: height * 0.05
                        }}>
                            <Image source={require('../assets/images/ic_logo_brand.png')} style={styles.logoStyle} resizeMode='contain'/>
                        </View>
                        <View style={{marginBottom: 10}}>
                            <ActivityIndicator
                                size="small" color=""
                                animating={true}/>
                        </View>
                        <Text style={{color: 'black', fontFamily: Medium.font}}>
                            powered by Bizconnect
                        </Text>

                    </View>
                </View>
            )
        }
    }
}

// const AppWithNavigationState = ({dispatch, nav}) => (
//     <AppNavigator navigation={addNavigationHelpers({dispatch, state: nav})}/>
// );

const styles = StyleSheet.create({
    logoStyle: {
        width: width * 0.2,
        height: height * 0.2
    }
});
const mapStateToProps = state => (
    {
        nav: state.nav,
    });

export default connect(mapStateToProps)(AppNavigation);
