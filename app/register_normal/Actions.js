import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {printerUrl} from '../navigation/AppNavigation';
import {baseUrl} from '../navigation/AppNavigation';

export const registration = (token, user,zone_id,event_id) => {
    return (dispatch) => {
        dispatch({
            type: 'REGIS_LOADING',
        })
        axios.post(`${printerUrl}/postCheckout`, {
            zone_id : zone_id,
            zone_list : user.selectedItems,
            title : user.title,
            ticket_id: user.ticketid,
            event_id: event_id,
            first_name: user.first_name,
            lastname: user.lastname,
            email: user.email,
            phone: user.phone,
            position:user.position,
            company:user.company,
            industry_sector: user.business,
            know_about : user.know,
            level : user.level,
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'REGIS_SUCCESS',
                        payload: response.data.data
                    })
                } else {
                    Alert.alert(
                        'Error',
                        response.data.message,
                        [
                            {
                                text: 'OK', onPress: () => dispatch({
                                    type: 'REGIS_FAILED',
                                    payload: response.data.message
                                })
                            },
                        ],
                        {cancelable: false}
                    )
                }

            })
            .catch((error) => {
                Alert.alert(
                    'Error',
                    error.message,
                    [
                        {
                            text: 'OK', onPress: () => dispatch({
                                type: 'REGIS_FAILED',
                                payload: error.message
                            })
                        },
                    ],
                    {cancelable: false}
                )
            });
    }


};

export const getAllTicket = (token,event_id,zoneid) => {
    return (dispatch) => {
        dispatch({
            type: 'TICKET_LOAD_DATA',
        })
        axios.get(`${printerUrl}/getTicket?event_id=${event_id}&zone_id=${zoneid}&mode_regis=1`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                dispatch({
                    type: 'TICKET_SUCCESS',
                    // payload: response.data.data
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    'Error',
                    error.message,
                    [
                        {text: 'OK', onPress: () => dispatch({
                                type: 'TICKET_FAILED',
                                payload: error.message
                            })},
                    ],
                    {cancelable: false}
                )
            });
    }


};

export const getEventZone = (token,id,ticket_id,zoneid)=> {
    return (dispatch) => {
        dispatch({
            type: 'ZONE_LOADING',
        })
        axios.get(`${printerUrl}/getZoneTicket?event_id=${id}&ticket_id=${ticket_id}&zone_id=${zoneid}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        }).then((response) => {
            if (response.data.success) {
                dispatch({
                    type: 'ZONE_SUCCESS',
                    payload: response.data
                })
            } else {
                dispatch({
                    type: 'ZONE_FAILED',
                })
            }

        })
            .catch((error) => {
                dispatch({
                    type: 'ZONE_FAILED',
                })
            });
    }


};

export const resetState = () => {
    return (dispatch) => {
        dispatch({
            type: 'REGIS_RESET',
        })
    }
};
