import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    AsyncStorage,
    Image,
    Keyboard,
    Alert,
    TouchableOpacity,
    NativeEventEmitter,
    Dimensions, NativeModules, ActivityIndicator, TouchableHighlight
} from 'react-native';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {color} from '../utills/color';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Container, Header, Left, Body, Right, Title, Content, Button, Label, Item, Input} from 'native-base';
import {fontSize} from "../utills/color";
import {Light, Medium} from "../utills/Fonts";
import * as actions from './Actions'
import {Strings} from '../utills/StringConfig';
import ModalFilterPicker from 'react-native-modal-filter-picker'

const back = require('../assets/images/arrow_back.png');
const {width, height} = Dimensions.get('window');
import {NavigationActions} from "react-navigation";
import MultiSelect from 'react-native-multiple-select';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';

let api_token;

type Props = {};
const validate_String = {
    title: 'Title is required .',
    username: 'Email is required .',
    email_invalid: 'Email is invalid .',
    password: 'password is required .',
    password_less_than: 'password is less than 6',
    email: 'email is required .',
    first_name: 'First name is required .',
    last_name: 'Last name is required .',
    telephone: 'Mobile is required .',
    position: 'Position is required .',
    company: 'Company is required .',
    telephone_less10: 'Mobile Less than 10 .',
    level: 'Level of Responsibility is required .',
    business: 'What is your type of business is required .',
    know: 'How did you know is required .',
    ticket_id: 'Ticket Type is required .',
    zoneid: 'Zone is required .',
}


class register extends Component<Props> {
    constructor(props) {
        super(props)
        this.state = {
            title: '',
            first_name: '',
            lastname: '',
            email: '',
            phone: '',
            position: '',
            company: '',
            done: false,
            isOnToggleList: false,
            visible_title: false,
            visible_level: false,
            visible_business: false,
            visible_know: false,
            visible_ticket: false,
            picked_title: {key: null, label: ''},
            picked_level: {key: null, label: ''},
            picked_business: {key: null, label: ''},
            picked_know: {key: null, label: ''},
            picked_ticket: {id: null, label: ''},
            level: '',
            business: '',
            know: '',
            ticketid: '',
            zoneid: '',
            list_ticket: {},
            selectedItems: [],
            itemMulti: []
        };
        // this.props.navigation.state.params.event
        // this.props.navigation.state.params.ticket
    }

    subscription() {
        // subscription = changeViewManagerEmitter.addListener(
        //     'closeView',
        //     (reminder) => {
        //         if (reminder.name === 'backReact') {
        //             this.setState({
        //                 title :'',
        //                 first_name: '',
        //                 lastname: '',
        //                 email: '',
        //                 phone: '',
        //                 position: '',
        //                 company: '',
        //                 done: false,
        //                 isOnToggleList: false,
        //                 visible_title: false,
        //                 visible_level: false,
        //                 visible_business: false,
        //                 visible_know: false,
        //                 visible_ticket: false,
        //                 picked_title: {key:null , label : ''},
        //                 picked_level: {key:null , label : ''},
        //                 picked_business: {key:null , label : ''},
        //                 picked_know: {key:null , label : ''},
        //                 picked_ticket: {id: null, label: ''},
        //                 level : '',
        //                 business : '',
        //                 know : '',
        //                 ticketid : '',
        //                 zoneid:'',
        //                 list_ticket: {},
        //                 selectedItems : [],
        //                 itemMulti : []
        //             })
        //         }
        //
        //     }
        // );
    }

    componentWillUnmount() {
        // subscription.remove();
    }

    componentDidMount() {
        // this.subscription();
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                this.api_token = api_token;
                // this.props.actions.getAllTicket(api_token, this.props.navigation.state.params.obj.id,this.props.navigation.state.params.ticketid);
                this.props.actions.getAllTicket(api_token, this.props.navigation.state.params.event.id, this.props.navigation.state.params.zoneid)
            }
        });
    }


    onBackPress() {
        this.props.actions.resetState();
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    registration() {
        if (this.validate()) {
            AsyncStorage.getItem('User', (err, result) => {
                if (result) {
                    const {api_token} = JSON.parse(result);
                    this.props.actions.registration(api_token, this.state, this.props.navigation.state.params.zoneid, this.props.navigation.state.params.event.id);
                }
            });

        }
    }

    validateEmail(email) {
        let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    validate = () => {
        if (this.state.ticketid.length === 0) {
            Alert.alert(validate_String.ticket_id);
            return false;
        }
        if (this.state.selectedItems.length === 0) {
            Alert.alert(validate_String.zoneid);
            return false;
        }
        if (this.state.title.length === 0) {
            Alert.alert(validate_String.title);
            return false;
        }
        if (this.state.first_name.length === 0) {
            Alert.alert(validate_String.first_name);
            return false;
        }
        if (this.state.lastname.length === 0) {
            Alert.alert(validate_String.last_name);
            return false;
        }
        if (this.state.email.length === 0) {
            Alert.alert(validate_String.email);
            return false;
        }
        if (!this.validateEmail(this.state.email)) {
            Alert.alert(validate_String.email_invalid);
            return false;
        }
        if (this.state.phone.length === 0) {
            Alert.alert(validate_String.telephone);
            return false;
        }
        // if (this.state.level.length === 0) {
        //     Alert.alert(validate_String.level);
        //     return false;
        // }
        if (this.state.business.length === 0) {
            Alert.alert(validate_String.business);
            return false;
        }
        // if (this.state.know.length === 0) {
        //     Alert.alert(validate_String.know);
        //     return false;
        // }
        // if (this.state.position.length === 0) {
        //     Alert.alert(validate_String.position);
        //     return false;
        // }
        // if (this.state.company.length === 0) {
        //     Alert.alert(validate_String.company);
        //     return false;
        // }
        return true;
    }

    onFirstChange(text) {
        this.setState({first_name: text});
    }

    onLastChange(text) {
        this.setState({lastname: text});
    }

    onEmailChange(text) {
        this.setState({email: text});
    }

    onPhoneChange(text) {
        this.setState({phone: text});
    }

    onPositionChange(text) {
        this.setState({position: text});
    }

    onCompapyChange(text) {
        this.setState({company: text});
    }

    onLevelChange(text) {
        this.setState({level: text});
    }

    onBusinessChange(text) {
        this.setState({business: text});
    }

    onKnowChange(text) {
        this.setState({know: text});
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.register.isGetTicket) {
            this.setState({
                // picked_ticket: nextProps.register.data[1],
                visible_ticket: false,
                list_ticket: nextProps.register.data_ticket
            })
        }

        if (nextProps.register.isGetZone) {
            this.setState({
                itemMulti: nextProps.register.data_zone
            })
        }

        if (nextProps.register.status && nextProps.register.callback) {
            /// call printer and set new state

            let html = `<!DOCTYPE html>
<html>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<style>
span{
    display:inline-block;
    border-bottom:3px solid black;
    padding-bottom:2px;
},
p {
   line-height : 1em; //relative to this element, 14px
   margin-bottom : 1em; //relative to this element, 14px
  },
  font.b {
  font-family: Helvetica, sans-serif;
}
</style>
<body>
<p><font size="10" class="b" face="Helvetica"><center><b>${nextProps.register.data.firstname}  ${nextProps.register.data.lastname.substring(0, 1).toUpperCase()}.</b></font></p>
<p><font size="8" class="b" face="Helvetica"><center><b>${nextProps.register.data.company}</b></font></p>
<center>
<img id='barcode'
            src="https://api.qrserver.com/v1/create-qr-code/?data=${nextProps.register.data.private_reference_number}&amp;size=100x100"
            alt=""
            title="HELLO"
            width="300"
            height="300" /></center>

</body>
</html>`
            //this._changeView(html);

            Alert.alert('Thank you!', 'Your registration was successful!')
            this.setState({
                title: '',
                first_name: '',
                lastname: '',
                email: '',
                phone: '',
                position: '',
                company: '',
                done: false,
                isOnToggleList: false,
                visible_title: false,
                visible_level: false,
                visible_business: false,
                visible_know: false,
                visible_ticket: false,
                picked_title: {key: null, label: ''},
                picked_level: {key: null, label: ''},
                picked_business: {key: null, label: ''},
                picked_know: {key: null, label: ''},
                picked_ticket: {id: null, label: ''},
                level: '',
                business: '',
                know: '',
                ticketid: '',
                zoneid: '',
                list_ticket: {},
                selectedItems: [],
                itemMulti: []

            })
            this.props.actions.resetState();
            //
        }
    }

    render() {
        if (!this.done) {
            return (
                <Container>
                    <Header style={{backgroundColor: color.primary}}>
                        <Left style={{flex: 1}}>
                            <Button transparent onPress={this.onBackPress.bind(this)}>
                                <Image source={back} resizeMode='contain' style={styles.titleIcon}/>
                            </Button>
                        </Left>
                        <Body>
                        <Title style={{
                            fontSize: 18,
                            fontFamily: Light.font,
                            color: 'white'
                        }}>{this.props.navigation.state.params.event.name}</Title>
                        </Body>
                        <Right/>
                    </Header>
                    <Content style={{backgroundColor: color.primary}}>
                        <View style={{paddingHorizontal: 30, alignItems: 'center', marginTop: 10}}>
                            <View>
                                <Label
                                    style={styles.textLabelBox}>{Strings.tickt_type_mark}</Label>
                                <View style={[styles.containerInput, {marginTop: 10, marginBottom: 20}]}>
                                    <TouchableOpacity style={{flex: 1}} onPress={this.onShow_ticket}>
                                        <Text style={[styles.textBox,{marginTop:12,marginLeft:10}]}>{this.state.picked_ticket.label}</Text>
                                    </TouchableOpacity>
                                    <ModalFilterPicker
                                        visible={this.state.visible_ticket}
                                        onSelect={this.onSelect_ticket}
                                        onCancel={this.onCancel}
                                        options={this.state.list_ticket}/>

                                </View>
                            </View>

                            <View>
                                <Label
                                    style={styles.textLabelBox}>{Strings.zone_mark} default zone your buy
                                    ({this.props.navigation.state.params.zone_label})</Label>
                                <View style={[styles.containerInput, {
                                    backgroundColor: 'white',
                                    marginTop: 10,
                                    marginBottom: 20
                                }]}>
                                    {/*<MultiSelect*/}
                                    {/*styleDropdownMenuSubsection={{height:height*0.1,alignSelf : 'center',borderRadius: 5,borderColor: 'white',backgroundColor: 'white'}}*/}
                                    {/*styleDropdownMenu={{borderRadius: 5,borderColor: 'white',backgroundColor: 'white',}}*/}
                                    {/*styleInputGroup={{height:height*0.05,borderRadius: 5,borderColor: 'white',backgroundColor: 'white',}}*/}
                                    {/*styleItemsContainer={{borderRadius: 5,borderColor: 'white',backgroundColor: 'white',}}*/}
                                    {/*styleListContainer={{borderRadius: 5,borderColor: 'white',backgroundColor: 'white',}}*/}
                                    {/*styleRowList={{borderRadius: 5,borderColor: 'white',backgroundColor: 'white',}}*/}
                                    {/*styleSelectorContainer={{borderRadius: 5,borderColor: 'white',backgroundColor: 'white',}}*/}
                                    {/*hideTags*/}
                                    {/*altFontFamily = {Light.font}*/}
                                    {/*fontFamily = {Light.font}*/}
                                    {/*fontSize = {18}*/}
                                    {/*itemFontSize = {18}*/}
                                    {/*itemFontFamily = {Light.font}*/}
                                    {/*selectedItemFontFamily = {Light.font}*/}
                                    {/*items={this.state.itemMulti}*/}
                                    {/*uniqueKey="id"*/}
                                    {/*ref={(component) => { this.multiSelect = component }}*/}
                                    {/*onSelectedItemsChange={this.onSelectedItemsChange}*/}
                                    {/*onToggleList={this.onToggleList}*/}
                                    {/*selectedItems={this.state.selectedItems}*/}
                                    {/*selectText=""*/}
                                    {/*searchInputPlaceholderText={Strings.filter}*/}
                                    {/*onChangeInput={ (text)=> console.log(text)}*/}
                                    {/*tagRemoveIconColor="#000000"*/}
                                    {/*tagBorderColor="#000000"*/}
                                    {/*tagTextColor="#000000"*/}
                                    {/*textColor="#000000"*/}
                                    {/*itemTextColor="#CCC"*/}
                                    {/*selectedItemTextColor="#01a374"*/}
                                    {/*selectedItemIconColor="#01a374"*/}
                                    {/*itemTextColor="#000000"*/}
                                    {/*displayKey="name"*/}
                                    {/*searchInputStyle={{ color: '#000000' }}*/}
                                    {/*submitButtonColor="#7BE0C3"*/}
                                    {/*submitButtonText={Strings.confirm}/>*/}
                                    <SectionedMultiSelect
                                        ref={(MultiSelect) => this._MultiSelect = MultiSelect}
                                        items={this.state.itemMulti}
                                        uniqueKey="id"
                                        selectText="Choose Zone"
                                        showDropDowns={true}
                                        searchPlaceholderText="Search Zone name"
                                        readOnlyHeadings={false}
                                        onSelectedItemsChange={this.onSelectedItemsChange}
                                        selectedItems={this.state.selectedItems}
                                        itemFontFamily={Light.font}
                                        showChips={false}
                                    />
                                </View>
                            </View>

                            {this.renderHideMulti()}
                        </View>

                    </Content>
                </Container>
            );
        } else {
            return (<View></View>);
        }


    }

    renderHideMulti() {
        if (!this.state.isOnToggleList) {
            return (
                <View>
                    <View>
                        <Label
                            style={styles.textLabelBox}>{Strings.title_mark}</Label>
                        <View style={[styles.containerInput, {marginTop: 10, marginBottom: 20}]}>
                            <TouchableOpacity style={{flex: 1}} onPress={this.onShow}>
                                <Text style={[styles.textBox,{marginTop:12,marginLeft:10}]}>{this.state.picked_title.label}</Text>
                            </TouchableOpacity>
                            <ModalFilterPicker
                                visible={this.state.visible_title}
                                onSelect={this.onSelect}
                                onCancel={this.onCancel}
                                options={Strings.data_title}/>

                        </View>
                    </View>
                    <View>
                        <Label
                            style={styles.textLabelBox}>{Strings.frstname_mark}</Label>
                        <View style={[styles.containerInput, {marginTop: 10, marginBottom: 20}]}>
                            <Item>
                                <Input style={styles.textBox}
                                       onChangeText={this.onFirstChange.bind(this)}
                                       value={this.state.first_name}
                                />
                            </Item>
                        </View>
                    </View>
                    <View>
                        <Label
                            style={styles.textLabelBox}>{Strings.lastname_mark}</Label>
                        <View style={[styles.containerInput, {marginTop: 10, marginBottom: 20}]}>
                            <Item>
                                <Input style={styles.textBox}
                                       onChangeText={this.onLastChange.bind(this)}
                                       value={this.state.lastname}
                                />
                            </Item>
                        </View>
                    </View>
                    <View>
                        <Label
                            style={styles.textLabelBox}>{Strings.email_mark}</Label>
                        <View style={[styles.containerInput, {marginTop: 10, marginBottom: 20}]}>
                            <Item>
                                <Input style={styles.textBox}
                                       onChangeText={this.onEmailChange.bind(this)}
                                       value={this.state.email}
                                />
                            </Item>
                        </View>
                    </View>
                    <View>
                        <Label
                            style={styles.textLabelBox}>{Strings.phone_mark}</Label>
                        <View style={[styles.containerInput, {marginTop: 10, marginBottom: 20}]}>
                            <Item>
                                <Input style={styles.textBox}
                                       keyboardType='numeric'
                                       onChangeText={this.onPhoneChange.bind(this)}
                                       value={this.state.phone}
                                />
                            </Item>
                        </View>
                    </View>
                    <View>
                        <Label
                            style={styles.textLabelBox}>{Strings.position_mark}</Label>
                        <View style={[styles.containerInput, {marginTop: 10, marginBottom: 20}]}>
                            <Item>
                                <Input style={styles.textBox}
                                       keyboardType='numeric'
                                       onChangeText={this.onPositionChange.bind(this)}
                                       value={this.state.position}
                                />
                            </Item>
                        </View>
                    </View>
                    <View>
                        <Label
                            style={styles.textLabelBox}>{Strings.company_mark}</Label>
                        <View style={[styles.containerInput, {marginTop: 10, marginBottom: 20}]}>
                            <Item>
                                <Input style={styles.textBox}
                                       keyboardType='numeric'
                                       onChangeText={this.onCompapyChange.bind(this)}
                                       value={this.state.company}
                                />
                            </Item>
                        </View>
                    </View>
                    {/* <View>
                                    <Label
                                        style={styles.textLabelBox}>{Strings.level_mark}</Label>
                                    <View style={[styles.containerInput, {marginTop: 20, marginBottom: 30}]}>
                                    <TouchableOpacity style={{flex : 1}}  onPress={this.onShow_level}>
                                                <Text style={styles.textBox}>{this.state.picked_level.label}</Text>
                                            </TouchableOpacity>
                                            <ModalFilterPicker
                                                visible={this.state.visible_level}
                                                onSelect={this.onSelect_level}
                                                onCancel={this.onCancel}
                                                options={Strings.data_level}/>
                                    </View>
                                </View> */}
                    <View>
                        <Label
                            style={styles.textLabelBox}>{Strings.business_mark}</Label>
                        <View style={[styles.containerInput, {marginTop: 10, marginBottom: 20}]}>
                            <TouchableOpacity style={{flex: 1}} onPress={this.onShow_business}>
                                <Text style={[styles.textBox,{marginTop:12,marginLeft:10}]}>{this.state.picked_business.label}</Text>
                            </TouchableOpacity>
                            <ModalFilterPicker
                                visible={this.state.visible_business}
                                onSelect={this.onSelect_business}
                                onCancel={this.onCancel}
                                options={Strings.data_business}/>
                        </View>
                    </View>
                    {/* <View>
                                    <Label
                                        style={styles.textLabelBox}>{Strings.know_mark}</Label>
                                    <View style={[styles.containerInput, {marginTop: 20, marginBottom: 30}]}>
                                    <TouchableOpacity style={{flex : 1}}  onPress={this.onShow_know}>
                                                <Text style={styles.textBox}>{this.state.picked_know.label}</Text>
                                            </TouchableOpacity>
                                            <ModalFilterPicker
                                                visible={this.state.visible_know}
                                                onSelect={this.onSelect_know}
                                                onCancel={this.onCancel}
                                                options={Strings.data_know}/>
                                    </View>
                                </View> */}
                    <View style={styles.layoutlogincontainer}>
                        <TouchableOpacity
                            onPress={this.registration.bind(this)}
                            style={styles.loginSuccess}
                            underlayColor={color.white}>
                            {this.props.register.isLoading ? <ActivityIndicator
                                    size="large" color="#FC9575"
                                    animating={this.props.register.isLoading}/> :
                                <Text style={styles.loginText}>{Strings.registration_mark}</Text>}
                        </TouchableOpacity>
                    </View>
                </View>
            )
        }
    }

    onSelectedItemsChange = selectedItems => {
        this.setState({selectedItems});
    };

    onToggleList = () => {
        this.setState({isOnToggleList: !this.state.isOnToggleList});
    };

    onShow = () => {
        this.setState({visible_title: true});
    }

    onShow_ticket = () => {
        this.setState({visible_ticket: true});
    }

    onSelect = (picked) => {
        this.setState({
            picked_title: picked,
            visible_title: false,
            title: picked.key,
        })
    }

    onSelect_ticket = (picked) => {
        this.setState({
            picked_ticket: picked,
            visible_ticket: false,
            ticketid: picked.key,
        })
        this._MultiSelect._removeAllItems();
        this.props.actions.getEventZone(this.api_token, this.props.navigation.state.params.event.id, picked.key, this.props.navigation.state.params.zoneid)
    }

    onShow_business = () => {
        this.setState({visible_business: true});
    }

    onSelect_business = (picked) => {
        this.setState({
            picked_business: picked,
            visible_business: false,
            business: picked.key,
        })
    }

    onShow_know = () => {
        this.setState({visible_know: true});
    }

    onSelect_know = (picked) => {
        this.setState({
            picked_know: picked,
            visible_know: false,
            know: picked.key,
        })
    }

    onShow_level = () => {
        this.setState({visible_level: true});
    }

    onSelect_level = (picked) => {
        this.setState({
            picked_level: picked,
            visible_level: false,
            level: picked.key,
        })
    }

    onCancel = () => {
        this.setState({
            visible_title: false,
            visible_know: false,
            visible_level: false,
            visible_business: false,
            visible_ticket: false,
        });
    }


}

const styles = StyleSheet.create({
    image: {
        width: width,
        height: height * 0.25,
        backgroundColor: 'grey'
    },
    textTitle: {
        fontSize: fontSize.large,
        color: color.textTitle
    },
    textSubtitle: {
        fontSize: fontSize.small,
        color: 'gray',
        fontFamily: Light.font
    },
    textDetail: {
        fontSize: fontSize.medium,
        color: color.textBlue
    },
    line: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        marginTop: 5,
        marginBottom: 5,
        width: '12%',
    },
    space: {
        marginTop: 1.5,
        marginBottom: 1.5
    }
    , titleIcon: {
        width: width * 0.035,
        height: height * 0.028,
        tintColor: '#FFFFFF'
    },
    cardItem: {
        padding: 10,
        margin: 30,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#d6d7da',
    },
    textItemStyle: {
        fontSize: 20,
        color: 'black',
    },
    ButtonStyle: {
        borderRadius: 12,
        borderWidth: 1,
        borderColor: '#d6d7da',
        backgroundColor: color.primary,
        width: width * 0.4,
        height: height * 0.08
    },
    containerInput: {
        borderRadius: 5,
        borderColor: 'white',
        backgroundColor: 'white',
        width: width * 0.92,
        height: height * 0.08
    },
    textBox: {
        fontSize: 18,
        alignSelf: 'stretch',
        fontFamily: Light.font,
        marginTop: 2,
        marginLeft: 2
    },
    textLabelBox: {
        fontSize: 18,
        alignSelf: 'stretch',
        color: 'white',
        fontFamily: Light.font,
    },
    layoutlogincontainer: {
        marginTop: 20,
        justifyContent: 'center',
        flexDirection: 'row',
        marginBottom: 60
    },
    loginText: {
        color: color.white,
        textAlign: 'center',
        fontFamily: Medium.font,
        fontSize: 18
    },
    loginSuccess: {
        flex: 1,
        marginTop: 10,
        paddingTop: 10,
        paddingBottom: 10,
        backgroundColor: color.buttonLoginSuscess,
        borderRadius: 5,
    },

});

const mapStateToProps = ({register}) => {
    return {
        register
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(register);
