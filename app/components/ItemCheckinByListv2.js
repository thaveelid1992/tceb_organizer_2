import React, {Component} from 'react';
import {TouchableOpacity, Image, Text, View, StyleSheet} from 'react-native';
import {Col, Row, Grid} from "react-native-easy-grid";
import {color, fontSize} from '../utills/color';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Approve from '../components/ApprovePopup'
import Failed from '../components/FailedPopup';
import { Strings } from '../utills/StringConfig';
import {Medium, Light} from '../utills/Fonts';
import { Thumbnail } from 'native-base';

// const {width, height} = Dimensions.get('window');

class ItemListV2 extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            event: this.props.event,
            zone: this.props.zone,
            ModalApprove: false,
            ModalSuccess: false,
            ModalFailed: false,
            message:'',
        }
    }

    render() {
        let {id, full_name, tel, ticket_type, email, has_arrived, ticket_id,profile_image} = this.state.data
        return (
            <TouchableOpacity onPress={() => this.selectItem(this.state.data)}>
                <Approve loading={this.state.ModalApprove}
                         close={this.onApproveClose.bind(this)}
                         message={'APPROVE'}
                         eventName={this.state.event.name}
                         name={full_name}
                         phone={tel}
                         type={ticket_type}
                         id={ticket_id}
                         email={email}
                         attendee_id={id}
                         zone_id={this.state.zone.key}
                         token={this.props.token}
                         callback={this.callback}
                         event_id={this.props.event.id}/>
                <Failed
                    loading={this.state.ModalFailed}
                    close={this.onFailedClose.bind(this)}
                    close_button={this.onFailedClose.bind(this)}
                    message={this.state.message}
                />
                <View style={{height:60}}>
                    <Grid>
                    <Col size={15}style={styles.colImage}>
                            <Thumbnail small style={styles.imageSize} source={{ uri: profile_image }} />
                        </Col>
                        <Col size={60}style={styles.rowData}>
                            <Row>
                                <Text numberOfLines={1} style={styles.rowTextTitle}>{full_name}</Text>
                            </Row>
                        </Col>
                        <Col size={25}style={styles.rowDataCheck}>
                            <Row >
                                {/*select Report*/}
                                {this.renderCheckin(has_arrived)}
                            </Row>
                        </Col>
                    </Grid>
                    <View style={styles.line}></View>
                </View>
            
            </TouchableOpacity>

        );
    }

    renderCheckin(has_arrived) {
        if (has_arrived === 1) {
            return (
              <View style={{flexDirection:"row"}}>
                  <Image
                    source={require('../assets/images/ic_checklist_active.png')}
                    style={styles.imageCheck} resizeMode="contain"/>
                  <Text style={styles.textcheck}>{Strings.checked_in}</Text>
              </View>
            );
        }else{
            return (
                <View style={{flexDirection:"row"}}>
                    <TouchableOpacity
                    onPress={() => this.selectItem(this.state.data)}
                    style={styles.ApproveButton}
                    underlayColor='#fff'>
                    <Text style={styles.textchecked}>{Strings.checktit_in}</Text>
                    </TouchableOpacity>
                </View>
              ); 
        }
    }

    selectItem(item) {
        if (this.state.data.has_arrived === 0) {
            this.setState({ModalApprove: true});
        }
    }
    callback = (data, type) => {
        if (type) {
            this.setState({data:data.data,ModalApprove: false, ModalSuccess: true});
        }else{
            this.setState({ModalFailed: true, message: data});
        }
    }
    onApproveClose() {
        this.setState({ModalApprove: false});
    }

    onFailedClose() {
        this.setState({ModalFailed: false, message: ''});
    }
}

const styles = StyleSheet.create({
    textBox: {
        fontSize: fontSize.medium,
        alignSelf: 'stretch',
        color: color.white,
    },
    grid: {
    },
    rowImage: {
        paddingHorizontal: 20
    },
    imageSize: {
        justifyContent: 'center',
    },
    imageCheck: {
        width: '25%',
        height: '40%',
        justifyContent: 'center',
        marginRight : 5,
        alignSelf: 'center'
    },
    colImage: {
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8,
    },
    rowTextTitle: {
        flex: 1,
        fontSize: fontSize.small,
        alignSelf: 'center',
        fontWeight: 'bold'
    },
    rowText: {
        flex: 3,
        paddingLeft: 8,
        fontSize: fontSize.medium,
        color: color.primary
    },
    rowLine: {
        marginTop: 8
    },
    rowData: {
    },
    rowDataCheck: {
        alignItems: 'center',
        justifyContent: 'center',
    },
    line: {
        borderBottomColor: color.line_gary,
        borderBottomWidth: 1,
        width: '100%',
    },
    textcheck: {
        alignSelf: 'center',
        color: color.black,
        fontSize: fontSize.small,
        fontFamily: Light.font,
    },
    textchecked: {
        alignSelf: 'center',
        color: color.white,
        fontSize: fontSize.small,
        fontFamily: Light.font,
    },
    ApproveButton: {
        marginTop: 8,
        marginBottom: 8,
        paddingTop: 8,
        paddingBottom: 8,
        paddingLeft: 8,
        paddingRight: 8,
        backgroundColor: '#50E3C2',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#50E3C2',
        flexDirection:"row",
    },
});
export default ItemListV2;