import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Modal,
    ActivityIndicator,
    Dimensions,
    Image,
    TouchableOpacity,
    Text
} from 'react-native';

const {width, height} = Dimensions.get('window');
const ic_close = require('../assets/images/ic_close.png');
const ic_success = require('../assets/images/ic_success.png');
const ic_line_popup = require('../assets/images/ic_line_popup.png');
import {fontSize, color} from '../utills/color';
import {Container} from 'native-base'
import {Col, Row, Grid} from "react-native-easy-grid";

const Loader = props => {
    const {
        loading,
        close,
        eventName,
        close_button,
        name,
        phone,
        type,
        id,
        email,
        approve,
        message,
        ...attributes
    } = props;

    return (
        <Modal
            transparent={true}
            animationType={"slide"}
            visible={loading}
            onRequestClose={() => {
                console.log('close modal')
            }}>
            <View style={styles.modalBackground}>
                <View style={styles.activityIndicatorWrapper}>
                    <TouchableOpacity onPress={close}>
                        <Image source={ic_close} style={{
                            width: 30,
                            height: 30,
                            alignSelf: 'flex-end',
                            alignItems: 'flex-end',
                            marginTop: 10,
                            marginRight: 10
                        }}/>
                    </TouchableOpacity>
                    <View>
                        <Image source={ic_success} style={styles.popupStyle} resizeMode='stretch'/>
                    </View>
                    <View>
                        <Text style={[styles.title]}>{message}</Text>
                    </View>
                    <View>
                        <Text style={[styles.subTitle]}>{eventName}</Text>
                    </View>
                    <View>
                        <Image source={ic_line_popup} style={[styles.line, {alignSelf: 'center'}]}
                               resizeMode='stretch'/>
                    </View>
                    <View style={{marginTop: '5%', flex: 1}}>
                        <View style={styles.containerDetail}>
                            <View style={{flex: 0.4}}>
                                <Text style={styles.detailSmall}>Name</Text>
                            </View>
                            <View>
                                <Text style={styles.detailMedium}>{name}</Text>
                            </View>
                        </View>
                        <View style={styles.containerDetail}>
                            <View style={{flex: 0.4}}>
                                <Text style={styles.detailSmall}>Tel</Text>
                            </View>
                            <View>
                                <Text style={styles.detailMedium}>{phone}</Text>
                            </View>
                        </View>
                        <View style={styles.containerDetail}>
                            <View style={{flex: 0.4}}>
                                <Text style={styles.detailSmall}>Ticket type</Text>
                            </View>
                            <View>
                                <Text style={styles.detailMedium}>{type}</Text>
                            </View>
                        </View>
                        <View style={styles.containerDetail}>
                            <View style={{flex: 0.4}}>
                                <Text style={styles.detailSmall}>Ticket ID</Text>
                            </View>
                            <View>
                                <Text style={styles.detailMedium}>{id}</Text>
                            </View>
                        </View>
                        <View style={styles.containerDetail}>
                            <View style={{flex: 0.4}}>
                                <Text style={styles.detailSmall}>Email</Text>
                            </View>
                            <View>
                                <Text style={styles.detailMedium}>{email}</Text>
                            </View>
                        </View>
                        <View>
                            <TouchableOpacity
                                onPress={close_button}
                                style={styles.ApproveButton}
                                underlayColor='#fff'
                            >
                                <Text style={styles.submitText}>Close</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        width: width * 0.8,
        height: height * 0.7,
        borderRadius: 10,
    },
    popupStyle: {
        width: width * 0.20,
        height: height * 0.11,
        alignSelf: 'center',
    },
    title: {
        color: '#4F677D',
        fontSize: fontSize.large,
        textAlign: 'center',
        marginTop: '5%'
    },
    subTitle: {
        color: '#677897',
        fontSize: fontSize.medium,
        textAlign: 'center',
        marginTop: '5%'
    },
    line: {
        width: width * 0.8,
        marginTop: 15,
    },
    ApproveButton: {
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: '#50E3C2',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#50E3C2',
        marginLeft: '15%',
        marginTop: '3%',
        marginRight: '15%'
    },
    PrintButton: {
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: '#CBE3FA',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#CBE3FA',
        marginLeft: '15%',
        marginTop: '3%',
        marginRight: '15%'
    },
    submitText: {
        fontSize: 16,
        color: 'white',
        textAlign: 'center',
    },
    detailSmall: {
        fontSize: 10, marginTop: 2, color: '#8CA0B3'
    },
    detailMedium: {
        fontSize: 14, textAlign: 'left', color: '#3D3BEE'
    },
    containerDetail: {
        marginLeft: '25%', flexDirection: 'row', marginTop: '2%'
    }
});

export default Loader;