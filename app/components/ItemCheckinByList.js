import React, {Component} from 'react';
import {TouchableOpacity, Image, Text, View, StyleSheet} from 'react-native';
import {Col, Row, Grid} from "react-native-easy-grid";
import {color, fontSize} from '../utills/color';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import Approve from '../components/ApprovePopup'
import Failed from '../components/FailedPopup';

class ItemList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: this.props.data,
            event: this.props.event,
            zone: this.props.zone,
            ModalApprove: false,
            ModalSuccess: false,
            ModalFailed: false,
            message:'',
        }
    }

    render() {
        let {id, full_name, tel, ticket_type, email, has_arrived, ticket_id} = this.state.data
        return (
            <TouchableOpacity onPress={() => this.selectItem(this.state.data)}>
                <Approve loading={this.state.ModalApprove}
                         close={this.onApproveClose.bind(this)}
                         message={'APPROVE'}
                         eventName={this.state.event.name}
                         name={full_name}
                         phone={tel}
                         type={ticket_type}
                         id={ticket_id}
                         email={email}
                         attendee_id={id}
                         zone_id={this.state.zone.key}
                         token={this.props.token}
                         callback={this.callback}
                         event_id={this.props.event.id}/>
                <Failed
                    loading={this.state.ModalFailed}
                    close={this.onFailedClose.bind(this)}
                    close_button={this.onFailedClose.bind(this)}
                    message={this.state.message}
                />

                <Grid style={styles.grid}>
                    <Row style={styles.rowImage}>
                        <Col style={styles.colImage}>
                            <Image
                                source={has_arrived === 0 ? require('../assets/images/ic_checklist_deactive.png') : require('../assets/images/ic_checklist_active.png')}
                                style={styles.imageSize}/>
                        </Col>
                        <Col style={styles.rowData}>
                            <Row>
                                <Text style={styles.rowTextTitle}>Name</Text>
                                <Text numberOfLines={2} style={styles.rowText}>{full_name}</Text>
                            </Row>
                            <Row>
                                <Text style={styles.rowTextTitle}>Tel</Text>
                                <Text numberOfLines={2} style={styles.rowText}>{tel}</Text>
                            </Row>
                            <Row>
                                <Text style={styles.rowTextTitle}>Ticket type</Text>
                                <Text numberOfLines={2} style={styles.rowText}>{ticket_type}</Text>
                            </Row>
                            <Row>
                                <Text style={styles.rowTextTitle}>Ticket ID</Text>
                                <Text numberOfLines={2} style={styles.rowText}>{ticket_id}</Text>
                            </Row>
                            <Row>
                                <Text style={styles.rowTextTitle}>Email</Text>
                                <Text numberOfLines={2} style={styles.rowText}>{email}</Text>
                            </Row>
                        </Col>
                    </Row>
                    <Row style={styles.rowLine}>
                        <View style={styles.line}></View>
                    </Row>
                </Grid>
            </TouchableOpacity>

        );
    }

    selectItem(item) {
        if (this.state.data.has_arrived === 0) {
            this.setState({ModalApprove: true});
        }
    }
    callback = (data, type) => {
        if (type) {
            this.setState({data:data.data,ModalApprove: false, ModalSuccess: true});
        }else{
            this.setState({ModalFailed: true, message: data});
        }
    }
    onApproveClose() {
        this.setState({ModalApprove: false});
    }

    onFailedClose() {
        this.setState({ModalFailed: false, message: ''});
    }
}

const styles = StyleSheet.create({
    textBox: {
        fontSize: fontSize.medium,
        alignSelf: 'stretch',
        color: color.white,
    },
    grid: {
        marginTop: 10,
    },
    rowImage: {
        paddingHorizontal: 20
    },
    imageSize: {
        width: '75%',
        height: '75%',
        resizeMode: 'contain'
    },
    colImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8,
    },
    rowTextTitle: {
        flex: 1,
        fontSize: fontSize.small,
        alignSelf: 'center'
    },
    rowText: {
        flex: 3,
        paddingLeft: 8,
        fontSize: fontSize.medium,
        color: color.primary
    },
    rowLine: {
        marginTop: 8
    },
    rowData: {
        flex: 4
    },
    line: {
        borderBottomColor: color.textBlue,
        borderBottomWidth: 1,
        marginTop: 5,
        marginBottom: 5,
        width: '100%',
    },
});
// const mapStateToProps = ({ checkByList }) => {
//     return {
//         checkByList
//     };
// };

// const mapDispatchToProps = (dispatch) => {
//     return {
//         actions: bindActionCreators(actions, dispatch)
//     }
// };
// export default connect(mapStateToProps, mapDispatchToProps)(ItemList)
export default ItemList;