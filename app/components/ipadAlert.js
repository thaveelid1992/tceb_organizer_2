import React, {Component} from 'react';
import {
    StyleSheet,
    View,
    Modal,
    ActivityIndicator,
    Dimensions,
    Image,
    TouchableOpacity,
    Text,
} from 'react-native';

const {width, height} = Dimensions.get('window');
const ic_close = require('../assets/images/ic_close.png');
const ic_success = require('../assets/images/ic_success.png');
const ic_line_popup = require('../assets/images/ic_line_popup.png');
import {fontSize, color} from '../utills/color';
import {Container} from 'native-base'
import {Col, Row, Grid} from "react-native-easy-grid";
import axios from 'axios'
import {baseUrl} from '../navigation/AppNavigation';

class Approve extends Component {
    constructor(props) {
        super(props);
        this.state = {isLoading: false};
    }

    render() {
        const {
            loading,
            close,
            eventName,
            name,
            phone,
            type,
            id,
            email,
            approve,
            ...attributes
        } = this.props;
        return (

            <Modal
                transparent={true}
                animationType={"slide"}
                visible={loading}
                onRequestClose={() => {
                    console.log('close modal')
                }}>
                <View style={styles.modalBackground}>
                    <View style={styles.activityIndicatorWrapper}>
                        <TouchableOpacity onPress={close}>
                            <Image source={ic_close} style={{
                                width: 40,
                                height: 40,
                                alignSelf: 'flex-end',
                                alignItems: 'flex-end',
                                marginTop: 10,
                                marginRight: 10
                            }}/>
                        </TouchableOpacity>
                        <View>
                            <Image source={ic_success} style={styles.popupStyle} resizeMode='stretch'/>
                        </View>
                        <View>
                            <Text style={[styles.subTitle]}>{eventName}</Text>
                        </View>
                        <View>
                            <Image source={ic_line_popup} style={[styles.line, {alignSelf: 'center'}]}
                                   resizeMode='stretch'/>
                        </View>
                        <View style={{marginTop:height*0.05, flex: 1}}>
                            <View style={styles.containerDetail}>
                                <View style={{flex: 0.3}}>
                                    <Text style={styles.detailSmall}>Name</Text>
                                </View>
                                <View style={{flex: 0.7}}>
                                    <Text numberOfLines={1} style={styles.detailMedium}>{name}</Text>
                                </View>
                            </View>
                            <View style={styles.containerDetail}>
                                <View style={{flex: 0.3}}>
                                    <Text style={styles.detailSmall}>Tel</Text>
                                </View>
                                <View style={{flex: 0.7}}>
                                    <Text style={styles.detailMedium}>{phone}</Text>
                                </View>
                            </View>
                            <View style={styles.containerDetail}>
                                <View style={{flex: 0.3}}>
                                    <Text style={styles.detailSmall}>Ticket ID</Text>
                                </View>
                                <View style={{flex: 0.7}}>
                                    <Text style={styles.detailMedium}>{id}</Text>
                                </View>
                            </View>
                            <View style={styles.containerDetail}>
                                <View style={{flex: 0.3}}>
                                    <Text style={styles.detailSmall}>Email</Text>
                                </View>
                                <View style={{flex: 0.7}}>
                                    <Text numberOfLines={1} style={styles.detailMedium}>{email}</Text>
                                </View>
                            </View>
                            <View>
                                {this.renderButton()}
                            </View>
                        </View>
                    </View>
                </View>
            </Modal>
        )
    }

    renderButton() {
        return (
            <TouchableOpacity
                onPress={this.props.approve}
                style={styles.ApproveButton}
                underlayColor='#fff'>
                <Text style={styles.submitText}>Confirm</Text>
            </TouchableOpacity>
        );
    }
}

const styles = StyleSheet.create({
    modalBackground: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row',
        justifyContent: 'space-around',
        backgroundColor: '#00000040'
    },
    activityIndicatorWrapper: {
        backgroundColor: '#FFFFFF',
        width: width * 0.8,
        height: height * 0.7,
        borderRadius: 10,
    },
    popupStyle: {
        width: width * 0.20,
        height: height * 0.13,
        alignSelf: 'center',
    },
    title: {
        color: '#4F677D',
        fontSize: fontSize.large,
        textAlign: 'center',
        marginTop: '5%'
    },
    subTitle: {
        color: '#677897',
        fontSize: 30,
        textAlign: 'center',
        marginTop: '5%'
    },
    line: {
        width: width * 0.1,
        marginTop: 15,
    },
    ApproveButton: {
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: '#50E3C2',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#50E3C2',
        marginLeft: '15%',
        marginTop: height*0.1,
        marginRight: '15%'
    },
    PrintButton: {
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: '#CBE3FA',
        borderRadius: 5,
        borderWidth: 2,
        borderColor: '#CBE3FA',
        marginLeft: '15%',
        marginTop: '3%',
        marginRight: '15%'
    },
    submitText: {
        fontSize: 20,
        color: 'white',
        textAlign: 'center',
    },
    detailSmall: {
        fontSize: 20, marginTop: 2, color: '#8CA0B3'
    },
    detailMedium: {
        fontSize: 18, textAlign: 'left', color: '#3D3BEE'
    },
    containerDetail: {
        marginLeft: '10%', flexDirection: 'row', marginTop: height*0.02
    }
});
export default Approve;
