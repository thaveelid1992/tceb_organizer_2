import React, {Component} from 'react';
import {
    Dimensions,
    StyleSheet,
    SafeAreaView,
    View,
    Text,
    Image,
    TouchableOpacity,
    FlatList
} from 'react-native';
import {Header  as sizeHeader} from 'react-navigation';
import {Container, Header, Content, List, ListItem,Button, Left, Right, Icon,Body,Title} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import { Strings } from '../utills/StringConfig';
import { Medium, Light } from '../utills/Fonts';
import { color, fontSize } from '../utills/color';
import {NavigationActions} from "react-navigation";
const {width, height} = Dimensions.get('window');
import FastImage from 'react-native-fast-image';

const bg_header = require('../assets/images/title_bg.png');
const ic_location = require('../assets/images/ic_location.png');
class FloorPlanDetail extends Component {
    constructor(props) {
        super(props);
    }

    backPress(){
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    render() {
        const {data} = this.props.navigation.state.params;
        return (
            <Container>
                <Header style={{ backgroundColor: color.primary }}>
                    <Left>
                        <TouchableOpacity onPress={this.backPress.bind(this)}>
                            <Image source={require('../assets/images/ic_back.png')}
                                style={styles.btnImage} resizeMode='contain' />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.textTitleActionBar}>{Strings.Agenda_title}</Title>
                    </Body>
                    <Right />
                </Header>
                <Content style={{backgroundColor:'white',flex:1}}>
                    <View style={{height:height*0.2}}>
                    <Image  source={bg_header} resizeMode='stretch' style={{width:width,height:height*0.15}}/>
                        <View style={styles.backgroundContainer}>
                            <View style={{flexDirection:'row'}}>
                                <Text style={styles.WhiteNormalStyle} numberOfLines={2}>{data.date}</Text>
                                <View style={{flex:1,flexDirection:'row',justifyContent:'flex-end'}}>
                                    <Text style={styles.WhiteNormalStyle} numberOfLines={2}>{`${data.time_begin} - ${data.time_end}`}</Text>
                                </View>
                            </View>
                            <Text style={styles.WhiteNormalStyle} numberOfLines={2}>{data.title}</Text>
                        </View>
                    </View>
                    <View style={{paddingHorizontal:20}}>
                        <Text style={styles.NormalStyle}>{data.detail}</Text>
                    </View>
                    <View style={{marginTop: 20, marginBottom: 10}}>
                            <Grid>
                                <View>
                                    <View style={styles.line}/>
                                    <View style={{paddingHorizontal: 20}}>
                                        <Row style={{marginTop: 10, marginBottom: 10}}>
                                            <Col size={15}>
                                                <Image source={ic_location} style={styles.iconDetailSize}/>
                                            </Col>
                                            <Col size={85}>
                                                <Text style={[styles.NormalStyle, {marginRight: 30}]}>{data.location}</Text>
                                            </Col>
                                        </Row>
                                    </View>
                                    <View style={styles.line}/>
                                </View>
                            </Grid>
                        </View>
                </Content>
            </Container>
        );
    }
    onBackPress(){
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: 'black'
    },
    btnImage: {
        height: 28,
        width: 28
    },
    textTitleActionBar: {
        color: color.white,
        fontSize: fontSize.large,
        fontFamily: Medium.font
    },
    HeaderTextStyle: {
        fontSize: 20,
        fontFamily: Medium.font,
        color: '#273D52',
        textAlign:'center',
        marginRight:'10%',
        marginTop:'2%'
    },
    backgroundContainer: {
        position: 'absolute',
        top: 0,
        bottom: 0,
        left: 0,
        right: 0,
        padding:20
    },
    image: {
        width: width, height: height * 0.45, backgroundColor: 'grey',
    },
    titleIcon: {
        width: width * 0.035,
        height: height * 0.028
    },
    titleStyle: {
        color: '#273D52',
        fontSize: 16,
        fontFamily: Light.font,
        fontWeight: 'bold'
    },
    NormalStyle: {
        color: '#273D52',
        fontSize: 14,
        fontFamily: Light.font,
    },
    WhiteNormalStyle: {
        color: 'white',
        fontSize: 14,
        fontFamily: Light.font,
        fontWeight: 'bold'
    },
    iconSize: {
        width: width * 0.04,
        height: width * 0.04,
    },
    iconDetailSize: {
        width: width * 0.06,
        height: width * 0.06,
    },
    line: {
        borderBottomColor: '#D8D8D8',
        borderBottomWidth: 1,
        marginTop: '5%',
        marginBottom: '5%',
    },
    banner:{
        width:width,
        height:height*0.25,
    },
    txtDate:{
        color: '#273D52',
        fontSize: 11,
        fontFamily: Light.font,
        marginTop: '1%'
    },
    line: {
        borderBottomColor: 'black',
        borderBottomWidth: 0.3,
        marginTop: '8%',
        marginBottom: '5%',
    },
    iconButtonSize:{
        width: width * 0.15,
        height: width * 0.15,
        alignItems:'center',
        alignSelf:'center'
    },
    footerTitleStyle: {
        fontSize: fontSize.large,
        fontFamily: Light.font,
        fontWeight: 'bold',
    },
    regisButton: {
        paddingTop: 8,
        paddingBottom: 8,
        backgroundColor: 'white',
        borderRadius: 5,
        width:width*0.35
    },
    line: {
        borderBottomColor: '#F6F6F6',
        borderBottomWidth: 1,
        marginTop: 5,
        marginBottom: 5,
        width:width
    },
});

export default FloorPlanDetail;