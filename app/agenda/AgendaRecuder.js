const INITIAL_STATE = {
    data: [],
    isLoading: false,
    isSuccess: false,
    message: '',
    status: false
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'AGENDA_SUCCESS':
            return {
                ...state,
                isLoading: false,
                data:action.payload.data
            };
        case 'AGENDA_LOADING':
            return {
                ...state,
                isLoading: true,
            };
        case 'AGENDA_FAILED':
            return {
                ...state,
                isLoading: false,
                message:''
            };
        default:
            return state;
    }
};