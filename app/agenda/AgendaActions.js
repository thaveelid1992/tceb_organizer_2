import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';
import {Strings} from '../utills/StringConfig';

export const getEventAgenda = (id, token) => {
    return (dispatch) => {
        dispatch({
            type: 'AGENDA_LOADING',
        })
        axios.get(`${baseUrl}/getEventAgenda?event_id=${id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        }).then((response) => {
            if(response.data.success){
                dispatch({
                    type: 'AGENDA_SUCCESS',
                    payload: response.data
                })
            }else{
                dispatch({
                    type: 'AGENDA_FAILED',
                    payload: response.data.message
                })
            }

        })
            .catch((error) => {
                dispatch({
                    type: 'AGENDA_FAILED',
                    payload:error.message
                })
            });
    }


};