import React, {Component} from 'react'
import {color, fontSize} from '../utills/color';
import {
    Text,
    TextInput,
    Platform,
    StyleSheet,
    View,
    Image,
    TouchableOpacity,
    FlatList,
    Alert,
    AsyncStorage,
    ActivityIndicator,
    Dimensions
} from 'react-native';
import {
    Container,
    Header,
    Left,
    Body,
    Right,
    Button,
    Icon,
    Title,
    Input,
    Item,
    Content,
    List,
    ListItem
} from 'native-base';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import {Strings} from '../utills/StringConfig';
import {Medium, Light} from '../utills/Fonts';
import {NavigationActions} from "react-navigation";
import FastImage from 'react-native-fast-image';
const ic_notfound = require('../assets/images/ic_notfound.png');
const {width, height} = Dimensions.get('window');

const data_list = require('../agenda/data');
import * as actions from './AgendaActions'

class Agenda extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isZone: true,
            isOvarall: false
        }
    }

    componentDidMount() {
        const {data} = this.props.navigation.state.params;
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                this.props.actions.getEventAgenda(data.id, api_token)
            }
        });
    }

    backPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    render() {
        return (
            <Container>
                <Header style={{backgroundColor: color.primary}}>
                    <Left>
                        <TouchableOpacity onPress={this.backPress.bind(this)}>
                            <Image source={require('../assets/images/ic_back.png')}
                                   style={styles.btnImage} resizeMode='contain'/>
                        </TouchableOpacity>
                    </Left>
                    <Body>
                    <Title style={styles.textTitleActionBar}>{Strings.Agenda_title}</Title>
                    </Body>
                    <Right/>
                </Header>
                <Content  style={{backgroundColor:'white'}}>
                    {this.renderContent()}
                </Content>
            </Container>
        )
    }

    renderContent() {
        const {isLoading, data} = this.props.agenda;
        if (isLoading) {
            return (
                <View style={{flex: 1, alignItems: 'center', alignSelf: 'center', justifyContent: 'center',marginTop:height*0.2}}>
                    <ActivityIndicator
                        size="large" color="#FC9575"
                        animating={isLoading}/>
                </View>
            );
        } else {
            if (data.length === 0) {
                return (
                    <View style={{flex:1,justifyContent:'center',alignItems:'center',alignSelf:'center',marginTop:height*0.2}}>
                        <FastImage source={ic_notfound} style={{width: width * 0.3, height: height * 0.3}}
                                   resizeMode='stretch'/>
                        <Text style={{marginTop: height * 0.03, fontSize: 24, color: '#8CA0B3'}}>
                            {Strings.not_found}
                        </Text>
                    </View>
                );
            }
            return (
                <List>
                    <FlatList
                        data={data}
                        keyExtractor={(item, index) => index}
                        renderItem={this.renderListItem.bind(this)}
                    />
                </List>
            );
        }
    }

    renderListItem({item}) {
        return (
            <ListItem key={item} onPress={() => this.onItemSelected(item)} style={{marginBottom: 5, marginTop: 5}}>
                <Left>
                    <View>
                        <Text style={styles.NormalStyle}>{`${item.date}  ${item.time_begin} - ${item.time_end}`}</Text>
                        <Text style={[styles.BoldStyle]}
                              numberOfLines={2}>{item.title}</Text>
                    </View>
                </Left>
                <Right>
                    <Icon name="arrow-forward"/>
                </Right>
            </ListItem>
        );
    }

    onItemSelected(item) {
        this.props.navigation.navigate('AgendaDetaill', {data: item});
    }
}

const styles = StyleSheet.create({
    btnImage: {
        height: 28,
        width: 28
    },
    textTitleActionBar: {
        color: color.white,
        fontSize: fontSize.large,
        fontFamily: Medium.font
    },
    NormalStyle: {
        color: color.textTitle,
        fontSize: 14,
        fontFamily: Light.font,
    },
    BoldStyle: {
        color: color.textTitle,
        fontSize: 14,
        fontFamily: Light.font,
        fontWeight: 'bold'
    },
})
const mapStateToProps = ({agenda}) => {
    return {
        agenda
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Agenda);