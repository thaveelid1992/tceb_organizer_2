const INITIAL_STATE = {
    data: [],
    datatickettype: [],
    datastat:[],
    message: '',
    status: false,
    checkin: false,
    isgetattendee: false,
    attendee_checkin: {},
    attendee_count: 0,
    isLoading: false,
    isSearch:false,
    isLoading_LoadMore:false,
    data_search: [],
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'CHECKTYPEINBYLIST_ACTION_LOADING':
            return {
                ...state,
                isLoading: true,
                data_search: [],
                datatickettype: [],
                datastat : []
            };
        case 'Attendee_TICKETTYPE_SUCCESS':
            return {
                ...state,
                status: true,
                checkin: false,
                isLoading: false,
                isgetattendee: true,
                attendee_count: action.payload.attendees_count,
                data: action.payload.attendees,
                datastat : action.payload.stat
            };
        case 'Attendee_TICKETTYPE_SEARCH_SUCCESS':
            return {
                ...state,
                status: true,
                checkin: false,
                isLoading: false,
                isgetattendee: true,
                isSearch:true,
                attendee_count: action.payload.attendees_count,
                data_search: action.payload.attendees,
                datastat : action.payload.stat
            };
        case 'Attendee_TYPE_SEARCH_RESET':
            return {
                ...state,
                isLoading: true,
                isSearch:false,
                data_search: [],
            };
            case 'CHECKTYPEINBYLIST_ACTION_LOADING_MORE':
            return{
                ...state,
                isLoading_LoadMore: true,
            }
        case 'Attendee_TICKETTYPE_LOADMORE_SUCCESS':
            return {
                ...state,
                status: true,
                checkin: false,
                isLoading_LoadMore: false,
                isgetattendee: true,
                attendee_count: action.payload.attendees_count,
                data: state.data.concat(action.payload.attendees),
                datastat : action.payload.stat,
            };
        case 'CHECKINBY_TICKETTYPE_SUCCESS':
            return {
                ...state,
                status: true,
                checkin: true,
                isLoading: false,
                attendee_checkin: action.payload
            };
        case 'CHECKIN_TICKET_DEFAULT':
            return {
                ...state,
                data: [],
                datatickettype: [],
                message: '',
                status: false,
                checkin: false,
                isgetattendee: false,
                attendee_checkin: {},
                attendee_count: 0,
                isLoading: false,
                isSearch:false,
                data_search: [],
                datastat : [],
            };
        case 'CHECKIN_TICKET_RESET_SEARCH':
            return {
                ...state,
                isSearch:false,
                data_search: [],
            };
        default:
            return state;
    }
};