import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';
import {Strings} from '../utills/StringConfig';

export const getEventAttendeeByTicketID = (token, event_id,start_position,ticket_id,zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKTYPEINBYLIST_ACTION_LOADING',
        })
        axios.get(`${baseUrl}/getEventAttendee?event_id=${event_id}&ticket_id=${ticket_id}&start=${start_position}&zone_id=${zone_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'Attendee_TICKETTYPE_SUCCESS',
                        payload: response.data
                    })
                } else {
                    dispatch({
                        type: 'CHECKIN_TICKET_DEFAULT',
                    })
                }
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};
export const getEventAttendeeByTicketIDLOADMORE = (token, event_id,start_position,ticket_id,zone_id) => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKTYPEINBYLIST_ACTION_LOADING_MORE',
        })
        axios.get(`${baseUrl}/getEventAttendee?event_id=${event_id}&ticket_id=${ticket_id}&start=${start_position}&zone_id=${zone_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'Attendee_TICKETTYPE_LOADMORE_SUCCESS',
                        payload: response.data
                    })
                } else {
                    dispatch({
                        type: 'CHECKIN_TICKET_DEFAULT',
                    })
                }
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};
export const getEventAttendeeSearch = (token, event_id, start_position,ticket_id, zone_id, que) => {
    return (dispatch) => {
        dispatch({
            type: 'Attendee_TYPE_SEARCH_RESET',
        })
        axios.get(`${baseUrl}/getEventAttendee?event_id=${event_id}&start=${start_position}&ticket_id=${ticket_id}&zone_id=${zone_id}&q=${que}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                dispatch({
                    type: 'Attendee_TICKETTYPE_SEARCH_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const setCheckInByID = (token, id,zone_id) => {
    return (dispatch) => {
        axios.post(`${baseUrl}/CheckInByID`, {
            attendee_id: id,
            zone_id : zone_id,
        }, {
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': `Bearer ${token}`
                }
            })
            .then((response) => {
                dispatch({
                    type: 'CHECKINBY_TICKETTYPE_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const setDefault = () => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKIN_TICKET_DEFAULT',
        })
    }
};
export const resetSearch = () => {
    return (dispatch) => {
        dispatch({
            type: 'CHECKIN_TICKET_RESET_SEARCH',
        })
    }
};