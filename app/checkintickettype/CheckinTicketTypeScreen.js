import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    FlatList,
    Alert,
    AsyncStorage,
    Dimensions,
    SafeAreaView,
    ActivityIndicator,
    RefreshControl
} from 'react-native';
import {Container, Header, Item, Input, Icon, Button, Left} from 'native-base';
import {NavigationActions} from "react-navigation";
import {Col, Row, Grid} from "react-native-easy-grid";
import {color, fontSize} from '../utills/color';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as actions from './CheckinTicketTypeAction';
import ItemCheckinByList from '../components/ItemCheckinByList';
import {Strings} from '../utills/StringConfig';
import {Medium, Light} from '../utills/Fonts';
import Approve from '../components/ApprovePopup'
import Failed from '../components/FailedPopup';
import Auto from '../components/SuccessPopup';
import ItemList from '../components/ItemCheckinByList';
import Loading from '../components/Loading';
import FastImage from 'react-native-fast-image';

const datatickettype = {};
iscomponentDidMount = false
let api_tokenMain
let defincome = '0/0'

const {width, height} = Dimensions.get('window');

const ic_notfound = require('../assets/images/ic_notfound.png');

class checkintickettype extends Component {
    constructor(props) {
        super(props);
        this.state = {
            start_position: 1,
            isSearch: false,
            refreshing: false,
        };
    }

    componentWillMount() {
        if (!this.iscomponentDidMount) {
            this.props.actions.setDefault()
            if (!this.props.checkinbytickettype.isgetattendee) {
                this.props.actions.setDefault();
                this.datatickettype = this.props.navigation.state.params.datatickettype
                this.getEventAttendee(1)
            }
            this.iscomponentDidMount = true
        } else {
            this.iscomponentDidMount = false
        }
        // this.getEventAttendee(this.state.start_position)
    }

    _onRefresh = () => {
        this.setState({refreshing: false});
        this.getEventAttendee(1)
    }

    getEventAttendee(position) {
        const {event, zone} = this.props.navigation.state.params.dataevent;
        const {id, title} = this.props.navigation.state.params.datatickettype;
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                api_tokenMain = api_token;
                this.props.actions.getEventAttendeeByTicketID(api_tokenMain, event.id, position, id, zone.key);
            }
        });

    }

    backPress() {
        this.props.actions.setDefault()
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    selectItem(event) {
        Alert.alert(
            Strings.Confirm_Check_in,
            event.full_name,
            [
                {text: Strings.OK, onPress: () => this.userCheckinByID(event.id)},
                {text: Strings.Cancel, onPress: () => ''},
            ],
            {cancelable: false}
        )
    }

    userCheckinByID(id) {
        this.props.actions.setCheckInByID(api_tokenMain, id);
    }

    handleLoadMore = () => {
        const {attendee_count, data} = this.props.checkinbytickettype;
        const {event, zone} = this.props.navigation.state.params.dataevent;
        const {id, title} = this.props.navigation.state.params.datatickettype;
        if (attendee_count > data.length) {
            this.props.actions.getEventAttendeeByTicketIDLOADMORE(api_tokenMain, event.id, this.state.start_position + 1, id, zone.key);
            this.setState({start_position: this.state.start_position + 1});
        }
    };

    renderHeader() {
        const {id, title} = this.props.navigation.state.params.datatickettype;
        return (
            <View style={styles.viewFilter}>
                <Text numberOfLines={1} style={styles.textFilter}>{title}</Text>
                <Text numberOfLines={1} style={styles.textBox2}>{defincome}</Text>
            </View>
        );
    }

    renderListItem({item}) {
        const {id, full_name, email, tel, ticket_type, ticket_id, has_arrived} = item;
        const {params} = this.props.navigation.state;
        return (
            <ItemList data={item}
                      event={params.dataevent.event}
                      zone={params.dataevent.zone}
                      token={api_tokenMain}/>

        );
    }

    renderFooter = () => {
        const {attendee_count, data} = this.props.checkinbytickettype;
        const {event, zone} = this.props.navigation.state.params.dataevent;
        const {id, title} = this.props.navigation.state.params.datatickettype;
        if (attendee_count > data.length) {
            return (
                <View style={{flex: 1, alignItems: 'center', alignSelf: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator
                        size="large" color="#FC9575"
                        style={{marginTop: 10, marginBottom: 10}}
                        animating={true}/>
                </View>
            );
        }
        return null;

    }


    render() {
        if (this.props.checkinbytickettype.checkin) {
            let messCheckinStatus = '';
            if (this.props.checkinbytickettype.attendee_checkin.success) {
                messCheckinStatus = Strings.Success;
            } else {
                messCheckinStatus = Strings.UnSuccess;
            }
            Alert.alert(
                messCheckinStatus,
                this.props.checkinbytickettype.attendee_checkin.message,
                [
                    {text: Strings.OK, onPress: () => this.getEventAttendee(this.state.start_position)}
                ],
                {cancelable: false}
            )
        }

        const {datastat} = this.props.checkinbytickettype;
        if (datastat.length > 0) {
            const {total_attendee, total_has_arrived} = datastat[0];
            defincome = `${total_has_arrived}/${total_attendee}`
        } else {
            defincome = '0/0';
        }

        return (
            <SafeAreaView style={{backgroundColor: color.primary, flex: 1}}>
                <View style={styles.container}>
                    <View style={styles.actionBar}>
                        <Grid>
                            <Col size={12} style={{marginTop: '2%', alignItems: 'center'}}>
                                <TouchableOpacity onPress={this.backPress.bind(this)}>
                                    <Image source={require('../assets/images/ic_back.png')}
                                           style={styles.btnImage} resizeMode='contain'/>
                                </TouchableOpacity>
                            </Col>
                            <Col size={88} style={{marginTop: '1%', marginRight: '15%'}}>
                                <Item>
                                    <TouchableOpacity>
                                        <Image source={require('../assets/images/ic_search.png')}
                                               style={{width: 18, height: 18}}/>
                                    </TouchableOpacity>
                                    <Input style={styles.textBox} placeholder={Strings.search}
                                           placeholderTextColor={color.white} underlineColorAndroid={color.white}
                                           onChangeText={(text) => this.onSearchChange(text)}/>
                                </Item>
                            </Col>
                        </Grid>
                    </View>
                    {this.renderContainer()}
                </View>
            </SafeAreaView>
        );
    }

    renderContainer() {
        const {data, isSearch, data_search} = this.props.checkinbytickettype
        if (this.props.checkinbytickettype.isLoading) {
            return (
                <View style={{flex: 1, alignItems: 'center', alignSelf: 'center', justifyContent: 'center'}}>
                    <ActivityIndicator
                        size="large" color="#FC9575"
                        animating={true}/>
                </View>
            );
        } else {
            if (this.state.isSearch) {
                if (data_search.length === 0) {
                    return (
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <FastImage source={ic_notfound} style={{width: width * 0.3, height: height * 0.3}}
                                       resizeMode='stretch'/>
                            <Text style={{marginTop: height * 0.03, fontSize: 24, color: '#8CA0B3'}}>
                                {Strings.data_attendee_empty}
                            </Text>
                        </View>
                    );
                }
                else {
                    return (
                        <View>
                            <FlatList
                                data={data_search}
                                renderItem={this.renderListItem.bind(this)}
                                ListHeaderComponent={this.renderHeader.bind(this)}
                            />
                        </View>
                    );
                }
            } else {
                if (data.length === 0) {
                    return (
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <FastImage source={ic_notfound} style={{width: width * 0.3, height: height * 0.3}}
                                       resizeMode='stretch'/>
                            <Text style={{marginTop: height * 0.03, fontSize: 24, color: '#8CA0B3'}}>
                                {Strings.data_attendee_empty}
                            </Text>
                        </View>
                    );
                } else {
                    return (
                        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                            <FlatList
                                refreshControl={
                                    <RefreshControl
                                        refreshing={this.state.refreshing}
                                        onRefresh={this._onRefresh}
                                    />
                                }
                                data={data}
                                renderItem={this.renderListItem.bind(this)}
                                onEndReached={this.handleLoadMore}
                                onEndReachedThreshold={0.5}
                                ListFooterComponent={this.renderFooter}
                                ListHeaderComponent={this.renderHeader.bind(this)}/>
                        </View>
                    );
                }
            }
        }
    }

    onSearchChange(text) {
        const {event, zone} = this.props.navigation.state.params.dataevent;
        const {id, title} = this.props.navigation.state.params.datatickettype;
        if (text.length > 0) {
            this.setState({isSearch: true})
            this.props.actions.getEventAttendeeSearch(api_tokenMain, event.id, 1, id, zone.key, text);
            //this.props.actions.getEventAttendeeSearch(api_tokenMain, event.id,1,id, zone.key,text);
        } else {
            this.setState({isSearch: false})
            this.getEventAttendee(1);
            //this.props.actions.resetSearch();
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.white
    },
    actionBar: {
        height: Platform.OS === 'ios' ? 64 : 54,
        backgroundColor: color.primary,
    },
    btnImage: {
        marginTop: Platform.OS === 'ios' ? 10 : 0,
        height: 28,
        width: 28
    },
    btnImagehead: {
        height: 28,
        width: 28
    },
    textBox: {
        fontSize: fontSize.medium,
        alignSelf: 'stretch',
        fontFamily: Light.font,
        color: color.white,
    },
    textBox2: {
        flex: 0.3,
        fontSize: fontSize.medium,
        alignSelf: 'stretch',
        fontFamily: Light.font,
        color: color.white,
        textAlign: 'center',
        paddingLeft: 10,
        paddingRight: 10
    },
    grid: {
        marginTop: 10,
    },
    rowImage: {
        paddingHorizontal: 20
    },
    imageSize: {
        width: '75%',
        height: '75%',
        resizeMode: 'contain'
    },
    colImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 8,
    }, rowTextTitle: {
        flex: 1,
        fontSize: fontSize.small,
        fontFamily: Light.font,
        alignSelf: 'center'
    },
    rowText: {
        flex: 3,
        paddingLeft: 8,
        fontSize: fontSize.medium,
        fontFamily: Light.font,
        color: color.primary
    },
    rowLine: {
        marginTop: 8
    },
    rowData: {
        flex: 4
    },
    line: {
        borderBottomColor: color.textBlue,
        borderBottomWidth: 0.5,
        marginTop: 5,
        marginBottom: 5,
        width: '100%',
    },
    viewFilter: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: color.colorFilter,
        paddingHorizontal: 20,
        paddingVertical: 5
    },
    textFilter: {
        flex: 0.7,
        color: color.white,
        fontSize: fontSize.medium,
        fontFamily: Light.font,
    }
});

const mapStateToProps = ({checkinbytickettype}) => {
    return {
        checkinbytickettype
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(checkintickettype)
