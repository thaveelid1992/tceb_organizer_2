const INITIAL_STATE = {
    data: {},
    message: '',
    title_error: '',
    status: false,
    isError: false,
    isApprove: false,
    internet: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'GET_APPROVE_SUCCESS':
            return {
                ...state,
                status: true,
                data: action.payload
            };
        case 'APPROVE_SUCCESS':
            return {
                isApprove: true,
            }
        case 'RESET_TICKET':
            return {
                ...state,
                status: false,
                isError: false,
                isApprove: false,
                message: '',
                data: {}
            };
        case 'SCANNER_FAILED':
            return {
                ...state,
                isError: true,
                message: action.payload
            };
        default:
            return state;
    }
};