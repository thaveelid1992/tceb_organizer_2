import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';

export const getTicket = (token, qr_number, zone,event_id) => {
    return (dispatch) => {

        axios.post(`${baseUrl}/ScanQrCode`, {
            reference_number: qr_number,
            zone_id: zone,
            event_id:event_id
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'GET_APPROVE_SUCCESS',
                        payload: response.data
                    })
                } else {
                    dispatch({
                        type: 'SCANNER_FAILED',
                        payload: response.data.message
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: 'SCANNER_FAILED',
                    payload: error.message
                })
            });
    }
};
export const approveTicket = (token, id, zone,event_id) => {
    return (dispatch) => {
        axios.post(`${baseUrl}/CheckInByID`, {
            attendee_id: id,
            zone_id: zone,
            event_id:event_id
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'APPROVE_SUCCESS',
                    })
                } else {
                    dispatch({
                        type: 'SCANNER_FAILED',
                        payload: response.data.message
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: 'SCANNER_FAILED',
                    payload: error.message
                })
            });
    }
};
export const resetData = () => {
    return (dispatch) => {
        dispatch({
            type: 'RESET_TICKET',
        })
    }
};
export const resetApprove = () => {
    return (dispatch) => {
        dispatch({
            type: 'APPROVE_RESET',
        })
    }
};
export const autoApprove = (token, qr_number, zone,event_id) => {
    return (dispatch) => {

        axios.post(`${baseUrl}/ScanQrCodeAuto`, {
            reference_number: qr_number,
            zone_id: zone,
            event_id:event_id
        }, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`

            }
        })
            .then((response) => {
                if (response.data.success) {
                    dispatch({
                        type: 'GET_APPROVE_SUCCESS',
                        payload: response.data
                    })
                } else {
                    dispatch({
                        type: 'SCANNER_FAILED',
                        payload: response.data.message
                    })
                }
            })
            .catch((error) => {
                dispatch({
                    type: 'SCANNER_FAILED',
                    payload: error.message
                })
            });
    }
};