import React from 'react';
import {
    AppRegistry,
    StyleSheet,
    Text,
    NavigatorIOS,
    TouchableOpacity,
    Linking,
    View,
    Platform,
    Alert,
    Dimensions,
    Image,
    AsyncStorage
} from 'react-native';
import Camera from 'react-native-camera';
import ViewFinder from './ViewFinder';
import {Container, Header, Left, Body, Right, Button, Icon, Title, Subtitle, Content} from 'native-base';
import {color, fontSize} from "../utills/color";
import {Col, Row, Grid} from "react-native-easy-grid";
import {NavigationActions} from "react-navigation";
import Approve from '../components/ApprovePopup'
import Failed from '../components/FailedPopup';
import Auto from '../components/SuccessPopup';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as actions from './ScanActions';

const ic_swich = require('../assets/images/ic_swich_camera.png')
const ic_flash = require('../assets/images/ic_flash.png')
const ic_back = require('../assets/images/ic_back.png')
const ic_autocheck = require('../assets/images/ic_autocheck.png');
const ic_print = require('../assets/images/ic_print.png');
const {width, height} = Dimensions.get('window');
let key = '';


class ScanQrScreen extends React.Component {
    constructor(props) {
        super(props);

        console.disableYellowBox = true;

        this.state = {
            isActiveState: true,
            isAutoCheckIn: false,
            flashlightEnabled: false,
            switchCameraEnabled: false,
            torchMode: Camera.constants.TorchMode.off,
            switchCamera :Camera.constants.Type.back, 
            ModalApprove: false,
            ModalSuccess: false,
            ModalFailed: false,
            isButtonLoad: true,
            api_token: ''
        }
        ;
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        const {params} = this.props.navigation.state;
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                key = api_token;
            }
        });
        //this.props.actions.getTicket('jt6Za0NWWQ3Z8il5NfsaFxyRt6tMBDB6WTCxtKfXhsH4nWYMmAMetDaA78Cq', '115001682', params.data.zone.key);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.scanner.status) {
            if (this.state.isAutoCheckIn) {
                this.setState({isActiveState: false, ModalSuccess: true});
            } else {
                if (nextProps.scanner.data.status === 200) {
                    this.setState({isActiveState: false, ModalSuccess: true});
                } else {
                    this.setState({isActiveState: false, ModalApprove: true});
                }
            }
        }
        if (nextProps.scanner.isError) {
            this.setState({isActiveState: false, ModalFailed: true});
        }
    }

    render() {
        const {params} = this.props.navigation.state;
        const TopComponent = this.state.isActiveState ? Camera : Camera;
        const {data} = this.props.scanner;
        return (
            <Container style={{backgroundColor:'white'}}>
                <Approve
                    loading={this.state.ModalApprove}
                    close={this.onApproveClose.bind(this)}
                    eventName={params.data.eventName}
                    message={data.message}
                    name={data.full_name}
                    phone={data.tel}
                    type={data.ticket_type}
                    id={data.ticket_id}
                    email={data.email}
                    callback={this.callback}
                    attendee_id={data.id}
                    zone_id={params.data.zone.key}
                    event_id={params.data.event.id}
                    token={key}
                    approve={this.sendApprove.bind(this)}/>
                <Failed
                    loading={this.state.ModalFailed}
                    close={this.onFailedClose.bind(this)}
                    close_button={this.onFailedClose.bind(this)}
                    message={this.props.scanner.message}
                />
                <Auto
                    loading={this.state.ModalSuccess}
                    close={this.onAutoApproveClose.bind(this)}
                    eventName={params.data.eventName}
                    message={data.message}
                    name={data.full_name}
                    phone={data.tel}
                    type={data.ticket_type}
                    id={data.ticket_id}
                    email={data.email}
                    close_button={this.onAutoApproveClose.bind(this)}
                    approve={this.onApproveClose.bind(this)}
                />
                <Header style={{backgroundColor: color.primary}}>
                    <Left>
                        <TouchableOpacity onPress={this.onBackPress.bind(this)}
                                          style={{flex: 1, justifyContent: 'center'}}>
                            <Image source={ic_back} style={{width: 28, height: 28}} resizeMode='contain'/>
                        </TouchableOpacity>
                    </Left>
                    <Body>
                    <Text style={{color: 'white', fontSize: 18}}>Scanning</Text>
                    <Text numberOfLines={1} style={{color: 'white', fontSize: 13}}>{`Zone ${params.data.zone.label}`}</Text>
                    </Body>
                    <Right>
                    <TouchableOpacity onPress={this.toggleSwitch.bind(this)}>
                            <Image source={ic_swich}
                                   style={[styles.iconStyle2, {tintColor: !this.state.switchCameraEnabled ? 'white' : '#FF8787'}]}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.toggleFlash.bind(this)}>
                            <Image source={ic_flash}
                                   style={[styles.iconStyle, {tintColor: !this.state.flashlightEnabled ? 'white' : '#FF8787'}]}/>
                        </TouchableOpacity>
                    </Right>
                </Header>
                <TopComponent
                    onBarCodeRead={this.barcodeReceived.bind(this)}
                    type={this.state.switchCamera}
                    style={{alignItems:'center',alignSelf:'center',justifyContent:'center',backgroundColor: !this.state.isActiveState ? 'black' : null,height:height,width:width}}
                    torchMode={this.state.torchMode}
                >
                    <ViewFinder/>
                
                </TopComponent>
                <View style={styles.buttonsContainer}>
                        <Grid style={{alignSelf: 'center', alignItems: 'center', justifyContent: 'center',marginTop:'40%'}}>

                            <Col style={{marginRight: 6}}>
                                <TouchableOpacity
                                    style={!this.state.flashlightEnabled ? styles.cardDefault : styles.cardActive}
                                    onPress={this.toggleFlash.bind(this)}>
                                    <Grid>
                                        <Col size={25} style={{alignItems: 'center', justifyContent: 'center'}}>
                                            <Image source={ic_flash}
                                                   style={[styles.iconButtonSize, {tintColor: !this.state.flashlightEnabled ? 'white' : '#FF8787'}]}
                                                   resizeMode='contain'/>
                                        </Col>
                                        <Col size={75} style={{justifyContent: 'center'}}>
                                            <Text style={{
                                                fontSize: fontSize.medium,
                                                color: this.state.flashlightEnabled ? '#FF8787' : 'white'
                                            }}>Flash Light</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>
                            </Col>
                            <Col style={{marginLeft: 6}}>
                                <TouchableOpacity onPress={this.autoCheckIn.bind(this)}
                                                  style={!this.state.isAutoCheckIn ? styles.cardDefault : styles.cardActive}>
                                    <Grid>
                                        <Col size={25} style={{alignItems: 'center', justifyContent: 'center'}}>
                                            <Image source={ic_autocheck}
                                                   style={[styles.iconButtonSize, {tintColor: !this.state.isAutoCheckIn ? 'white' : '#FF8787'}]}
                                                   resizeMode='contain'/>
                                        </Col>
                                        <Col size={75} style={{justifyContent: 'center'}}>
                                            <Text style={{
                                                fontSize: fontSize.medium,
                                                color: this.state.isAutoCheckIn ? '#FF8787' : 'white'
                                            }}>Auto check in</Text>
                                        </Col>
                                    </Grid>
                                </TouchableOpacity>
                            </Col>
                        </Grid>

                    </View>
            </Container>
        )
    }

    onApproveClose() {
        this.props.actions.resetData();
        this.setState({ModalApprove: false, isActiveState: true});
    }

    onAutoApproveClose() {
        this.props.actions.resetData();
        this.setState({ModalSuccess: false, isActiveState: true});
    }

    onFailedClose() {
        this.props.actions.resetData();
        this.setState({ModalFailed: false, isActiveState: true});
    }

    onBackPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    callback = (data, type) => {
        if (type) {
            this.setState({ModalApprove: false, ModalSuccess: true});
        }
    }

    sendApprove() {
        debugger
        const {params} = this.props.navigation.state;
        const {data} = this.props.scanner;
        this.props.actions.approveTicket(key, data.id, params.data.zone.key,params.data.event.id);
    }

    toggleSwitch() {
        this.setState({
            switchCameraEnabled: !this.state.switchCameraEnabled,
            switchCamera: this.state.switchCamera == Camera.constants.Type.back ?
            Camera.constants.Type.front : Camera.constants.Type.back,
            torchMode :Camera.constants.TorchMode.off
        });
    }

    toggleFlash() {
        this.setState({
            flashlightEnabled: !this.state.flashlightEnabled,
            torchMode: this.state.torchMode == Camera.constants.TorchMode.off ?
                Camera.constants.TorchMode.on : Camera.constants.TorchMode.off
        });
    }

    autoCheckIn() {
        this.setState({isAutoCheckIn: !this.state.isAutoCheckIn});
    }

    barcodeReceived(e) {
        if (this.state.isActiveState) {
            this.setState({isActiveState: false});
            const {params} = this.props.navigation.state;
            if (this.state.isAutoCheckIn) {
                this.props.actions.autoApprove(key, e.data, params.data.zone.key,params.data.event.id);
            } else {
                this.props.actions.getTicket(key, e.data, params.data.zone.key,params.data.event.id);
            }
        }
    }

    setActiveState() {
        this.setState({isActiveState: false, ModalApprove: true});
    }


}


const styles = StyleSheet.create({
    centerText: {
        flex: 1,
        fontSize: 18,
        padding: 32,
        color: '#777',
    },
    textBold: {
        fontWeight: '500',
        color: '#000',
    },
    buttonText: {
        fontSize: 21,
        color: 'rgb(0,122,255)',
    },
    buttonTouchable: {
        padding: 16,
    },
    iconStyle: {
        width: 35,
        height: 35,
    },
    iconStyle2: {
        width: 35,
        height: 35,
        marginRight : 10
    },
    buttonsContainer: {
        position: 'absolute',
        bottom: 0,
        height: '50%',
        left: 0,
        right: 0,
        flex:1,
        paddingHorizontal: '10%',
        // opacity: 0.3
    },
    cardDefault: {
        borderWidth: 1,
        borderColor: 'rgba(255,255,255,1)',
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.08,
        backgroundColor: '#261919',
        borderRadius: 18,
    },
    cardActive: {
        borderWidth: 1,
        borderColor: 'rgba(255,135,135,1)',
        alignItems: 'center',
        justifyContent: 'center',
        height: height * 0.08,
        backgroundColor: '#261919',
        borderRadius: 18,
    },
    TextButtonSize: {
        fontSize: 12,
    },
    iconButtonSize: {
        width: 16,
        height: 16
    }
});
const mapStateToProps = ({scanner}) => {
    return {
        scanner
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(ScanQrScreen);