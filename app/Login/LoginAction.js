import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';

export const LoginByUser = (state) => {
    const {username, password} = state;
    return (dispatch) => {
        dispatch({
            type: 'LOGIN_LOADING',
        })
        axios.post(`${baseUrl}/authentication`, {
            email: username,
            password: password,
        }, {
            headers: {
                'Content-Type': 'application/json',
            }
        })
            .then((response) => {
                if (response.data.status == '200') {
                    response.data.configkiosk = false;
                    AsyncStorage.setItem('User', JSON.stringify(response.data), () => {
                        dispatch({
                            type: 'LOGIN_SUCCESS',
                            payload: response.data
                        })
                    });
                } else {
                    Alert.alert(
                        'Error',
                        response.data.message,
                        [
                            {text: 'OK', onPress: () => dispatch({
                                    type: 'LOGIN_FAILED',
                                })},
                        ],
                        {cancelable: false}
                    )

                }
            })
            .catch((error) => {
                Alert.alert(
                    'Error',
                    error.message,
                    [
                        // {text: 'OK', onPress: () => console.log(response.data.message)},
                        {text: 'OK', onPress: () =>  dispatch({
                                type: 'LOGIN_FAILED',
                            })},
                    ],
                    {cancelable: false}
                )
            });
    }


};
export const setDefautl = () => {
    return (dispatch) => {
        dispatch({
            type: 'LOGIN_DEFUALT',
        })
    }


};
