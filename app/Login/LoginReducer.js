const INITIAL_STATE = {
    data: [],
    message: '',
    status: false,
    isLoading:false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'LOGIN_LOADING':
            return {
                ...state,
                message: 'LOADING',
                isLoading:true,
            };
        case 'LOGIN_SUCCESS':
            return {
                ...state,
                status: true,
                isLoading:false,
            };
        case 'LOGIN_FAILED':
            return {
                ...state,
                status: false,
                isLoading:false,
            };
        case 'LOGIN_DEFUALT':
            return {
                data: [],
                message: '',
                status: false,
                isLoading:false,
            };
        default:
            return state;
    }
};