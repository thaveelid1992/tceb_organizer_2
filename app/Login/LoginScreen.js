import React from 'react';
import {
    View,
    Button,
    Alert,
    StyleSheet,
    Image,
    TouchableOpacity,
    TouchableHighlight,
    Text,
    TextInput,
    Keyboard,
    AsyncStorage,
    Platform,
} from 'react-native'
import * as Progress from 'react-native-progress';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from './LoginAction'
import { color, fontSize } from '../utills/color'
import { Form, Item, Input, Label, Icon, Left } from 'native-base'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import { Col, Row, Grid } from "react-native-easy-grid";
import { Strings } from '../utills/StringConfig';
import { Medium, Light } from '../utills/Fonts';
import Loading from '../components/Loading';
const tcelogo = require('../assets/images/tceb-logo.png');

class LoginScreen extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            hidePassword: true,
            progress: 0,
            indeterminate: true,
            isProgress: false,
            username: '',
            password: '',
            buttonSuccess: false,
        };
        alertText = {
            alerttitle: Strings.invaild_login,
            alertmsg: Strings.invaild_login_msg
        }
        validate_String = {
            username: Strings.user_is_required,
            password: Strings.pass_is_required
        }
    }
    LoninEvent() {
        Keyboard.dismiss();
        // this.props.navigation.navigate('checkinlist')
        // this.props.navigation.navigate('Tabs')
        if (this.validTextLogin()) {
            this.props.actions.LoginByUser(this.state)
            //this.props.navigation.navigate('Tabs')
        }
    }

    OnForgetPress = () => {
        this.props.navigation.navigate('forgot');
    }

    alertMes(titlemsg, mes) {
        Alert.alert(
            titlemsg,
            mes,
            [
                { text: 'OK', onPress: () => console.log('OK Pressed') },
            ],
            { cancelable: false }
        )
    }

    managePasswordVisibility = () => {
        this.setState({ hidePassword: !this.state.hidePassword });
    }

    validTextLogin() {
        if (this.state.username.length === 0) {
            this.alertMes(alertText.alerttitle, alertText.alertmsg);
            return false;
        }
        if (this.state.password.length === 0) {
            this.alertMes(alertText.alerttitle, alertText.alertmsg);
            return false;
        }
        return true;
    }

    validTextDigit(type, text) {
        if (type === validate_String.username) {
            this.setState({ username: text })
        } else if (type === validate_String.password) {
            this.setState({ password: text })
        }
        if (this.state.username.length > 5 && this.state.password.length > 3) {
            this.setState({ buttonSuccess: true })
        } else {
            this.setState({ buttonSuccess: false })
        }
    }

    componentDidMount() {
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.login.status) {
            this.props.navigation.navigate('Tabs')
        }
    }
    componentWillUnmount() {
        this.props.actions.setDefautl();
    }

    render() {
        return (
            <View style={styles.container}>
                <Loading loading={this.props.login.isLoading} />
                <KeyboardAwareScrollView style={styles.keyboard} >
                    <View style={{
                        justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Text style={styles.headerText}>
                            {Strings.login_w_username}
                        </Text>
                    </View>
                    <View style={{
                        marginTop: 20, marginBottom: 40, justifyContent: 'center',
                        alignItems: 'center'
                    }}>
                        <Image
                            style={{ width: 100, height: 100 }}
                            source={tcelogo}
                        />
                    </View>
                    <View style={[styles.itemInputText, { marginBottom: 20 }]}>
                        <Label
                            style={this.state.buttonSuccess ? styles.textTitleSuscess : styles.textTitle}>{Strings.username}</Label>
                        <Item>
                            <Input style={styles.textBox}
                                onChangeText={(text) => this.validTextDigit(validate_String.username, text)}
                                value={this.state.username}
                            />
                        </Item>
                    </View>
                    <View style={styles.itemInputText}>
                        <Label
                            style={this.state.buttonSuccess ? styles.textTitleSuscess : styles.textTitle}>{Strings.password}</Label>
                        <Item>
                            <Input style={styles.textBox}
                                secureTextEntry={this.state.hidePassword}
                                onChangeText={(text) => this.validTextDigit(validate_String.password, text)}
                                value={this.state.password} />
                            <TouchableOpacity
                                onPress={this.managePasswordVisibility}>
                                <Image source={require('../assets/images/ic_showpass.png')}
                                    style={styles.btnImage} />
                            </TouchableOpacity>
                        </Item>
                    </View>
                    <View style={styles.layoutlogincontainer}>
                        <TouchableHighlight
                            disabled={this.props.login.isLoading}
                            style={this.state.buttonSuccess ? styles.loginSuccess : styles.login}
                            onPress={this.LoninEvent.bind(this)}
                            underlayColor={color.white}>
                            <Text style={styles.loginText}>{Strings.loginbutton}</Text>
                        </TouchableHighlight>
                    </View>
                    <View style={{ marginTop: 20, marginBottom: 20 }}>
                        <Text onPress={this.OnForgetPress}
                            style={[styles.ForgetUnderline]}>
                            {Strings.forgot_pass}
                        </Text>
                    </View>
                </KeyboardAwareScrollView>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.primary,
        alignItems: 'center',
    },
    layoutlogincontainer: {
        marginTop: 20,
        justifyContent: 'center',
        flexDirection: 'row'
    },
    headerText: {
        marginTop: '15%',
        fontSize: fontSize.large,
        color: color.white,
        justifyContent: 'center',
        alignItems: 'center',
        fontFamily: Light.font,
    },
    imagelogo: {
        justifyContent: 'center',
        alignItems: 'center',
    },
    textinput: {
        height: 40,
        flex: 0.8,
        borderColor: color.white,
        borderWidth: 1,
        backgroundColor: color.white
    },
    progress: {
        margin: 10,
    },
    circles: {
        flexDirection: 'row',
        flex: 1,
        backgroundColor: color.white,
        justifyContent: 'center',
        alignItems: 'center',
    },
    login: {
        flex: 1,
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: color.buttonLoginUnSuscess,
        borderRadius: 5,
    },
    loginSuccess: {
        flex: 1,
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: color.buttonLoginSuscess,
        borderRadius: 5,
    },
    loginText: {
        color: color.white,
        textAlign: 'center',
        fontFamily: Medium.font,
    },
    ForgetUnderline: {
        fontSize: fontSize.medium,
        color: color.white,
        textDecorationLine: 'underline',
        textAlign: 'center',
        backgroundColor: 'rgba(0,0,0,0)',
        fontFamily: Light.font,
    },
    itemInputText: {
        alignItems: 'flex-start',
    },
    keyboard: {
        alignSelf: 'stretch',
        marginRight: '7%',
        marginLeft: '7%'
    },
    textTitle: {
        fontSize: fontSize.medium,
        marginLeft: 3,
        marginTop: Platform.OS === 'ios' ? 5 : 3,
        backgroundColor: 'rgba(0,0,0,0)',
        color: color.white,
        fontFamily: Light.font,
    },
    textTitleSuscess: {
        fontSize: fontSize.medium,
        marginLeft: 3,
        marginTop: Platform.OS === 'ios' ? 5 : 3,
        backgroundColor: 'rgba(0,0,0,0)',
        color: color.buttonLoginSuscess,
        fontFamily: Light.font,
    },
    textBox: {
        fontSize: fontSize.medium,
        alignSelf: 'stretch',
        color: color.white,
        fontFamily: Light.font,
    },
    textBoxSuscess: {
        fontSize: fontSize.medium,
        alignSelf: 'stretch',
        color: color.buttonLoginSuscess,
        fontFamily: Light.font,
    },
    btnImage: {
        resizeMode: 'contain',
        height: 25,
        width: 25
    },
});
const mapStateToProps = ({ login }) => {
    return {
        login
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(LoginScreen);
