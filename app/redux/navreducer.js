import {AppNavigator} from '../navigation/AppNavigation';
import {AsyncStorage} from 'react-native'
import {NavigationActions} from 'react-navigation';
const firstAction = AppNavigator.router.getActionForPathAndParams('splash');
const secondAction = AppNavigator.router.getActionForPathAndParams('login');
const main = AppNavigator.router.getActionForPathAndParams('Tabs');
const tempNavState = AppNavigator.router.getStateForAction(firstAction);
var getAccessApp = () => {
    AsyncStorage.getItem('User', (err, result) => {
        return AppNavigator.router.getStateForAction(AppNavigator.router.getActionForPathAndParams('Tabs'));
}); };
const initialState = AppNavigator.router.getStateForAction(
    NavigationActions.back(),
    tempNavState
);
export default (state = initialState, action) => {
    let nextState;
    nextState = AppNavigator.router.getStateForAction(action, state);
    return nextState || state;
};