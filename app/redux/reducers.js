import {combineReducers} from "redux";
import navReducer from './navreducer';
import LoginReducer from '../Login/LoginReducer';
import EventReducer from '../event/EventReducer';
import CheckInReducer from '../checkin/CheckinReducer'
import checkinByListReducer from '../checkinbylist/CheckInByListReducer'
import ScanReducer from '../scanner/ScanReducer';
import CheckinByTicketTypeReducer from '../checkintickettype/CheckinTicketTypeReducer';
import DashBoardReducer from '../dashboard/DashboardReducer';
import forgetReducer from '../Forget/ForgetReducer';
import ResetPassReducer from '../reset/resetReducer';
import FloorPlanReducer from '../FloorPlan/FloorPlanReducer';
import AgendaReducer from '../agenda/AgendaRecuder';
import setting from '../setting/Reducers';
import RegisterReducer from '../register/Reducers';
import TicketReducer from '../ticket/Reducers';
import ScannerIpad from '../scanneripad/Reducers';
import SearchReducer from '../search/Reducers';
import CheckinListv2Reducer from '../checkinlistv2/CheckinListv2Reducer';

const appReducer = combineReducers({
    nav: navReducer,
    login: LoginReducer,
    event: EventReducer,
    checkin: CheckInReducer,
    checkinByList: checkinByListReducer,
    scanner: ScanReducer,
    checkinbytickettype: CheckinByTicketTypeReducer,
    dashboard: DashBoardReducer,
    forgot: forgetReducer,
    resetpassword: ResetPassReducer,
    floorplanreducer: FloorPlanReducer,
    agenda: AgendaReducer,
    setting: setting,
    register: RegisterReducer,
    ticket: TicketReducer,
    scanneripad: ScannerIpad,
    search: SearchReducer,
    checkinlistv2: CheckinListv2Reducer,
});

export default appReducer;
