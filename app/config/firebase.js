import * as firebase from 'firebase';

let config = {
    apiKey: "AIzaSyBkflkW8N9ZpixfxuAirKHdcqKeSGtVaVw",
    authDomain: "tceb-6770e.firebaseapp.com",
    databaseURL: "https://tceb-6770e.firebaseio.com",
    projectId: "tceb-6770e",
    storageBucket: "tceb-6770e.appspot.com",
    messagingSenderId: "386145539313"
};
firebase.initializeApp(config);
module.exports = firebase;
