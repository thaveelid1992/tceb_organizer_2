const INITIAL_STATE = {
    data: [],
    message: '',
    status: false,
    isLoading: false,
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'TICKET_LOAD_DATA':
            return {
                ...state,
                isLoading: true
            };
        case 'TICKET_SUCCESS':
            return {
                ...state,
                status: true,
                data: action.payload,
                isLoading: false
            };
        case 'TICKET_FAILED':
            return {
                ...state,
                isLoading: false
            };
        default:
            return state;
    }
};