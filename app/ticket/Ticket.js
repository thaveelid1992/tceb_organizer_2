import React, {Component} from 'react';
import {
    Platform,
    StyleSheet,
    Text,
    View,
    FlatList,
    AsyncStorage,
    Image,
    TouchableOpacity,
    Dimensions, NativeModules, ActivityIndicator
} from 'react-native';
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as actions from './Actions'
import {color} from '../utills/color';
import {Col, Row, Grid} from "react-native-easy-grid";
import {Container, Header, Left, Body, Right, Title, Content, Button} from 'native-base';
import home from "./Reducers";
import {fontSize} from "../utills/color";
import {Light, Medium} from "../utills/Fonts";

const back = require('../assets/images/arrow_back.png');
const {width, height} = Dimensions.get('window');
import {NavigationActions} from "react-navigation";

type Props = {};

class Home extends Component<Props> {
    constructor(props) {
        super(props)
        this.done = false;
    }

    getAllEven() {
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {api_token} = JSON.parse(result);
                this.props.actions.getAllTicket(api_token, this.props.navigation.state.params.obj.id,this.props.navigation.state.params.zoneid);
            }
        });
    }

    _changeView() {
        this.done = true;
        this.render();
        NativeModules.ChangeViewBridge.changeToNativeView('');
    }

    componentDidMount() {
        this.getAllEven();
    }

    onBackPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    render() {
        const {data} = this.props.ticket;
        if (!this.done) {
            return (
                <Container>
                    <Header style={{backgroundColor: color.primary}}>
                        <Left style={{flex: 1}}>
                            <Button transparent onPress={this.onBackPress.bind(this)}>
                                <Image source={back} resizeMode='contain' style={styles.titleIcon}/>
                            </Button>
                        </Left>
                        <Body>
                        <Title style={{fontSize: 30, fontFamily: Light.font, color: 'white'}}>Ticket</Title>
                        </Body>
                        <Right/>
                    </Header>
                    <Content>
                        {this.renderLoading()}
                    </Content>
                </Container>
            );
        } else {
            return (<View></View>);
        }

    }

    renderLoading() {
        const {data, isLoading} = this.props.ticket;
        if (isLoading) {
            return (
                <View style={{marginTop: height * 0.4}}>
                    <ActivityIndicator
                        size="large" color="#FC9575"
                        animating={true}/>
                </View>
            );
        }
        return (
            <FlatList
                data={data}
                keyExtractor={(item, index) => item.id}
                renderItem={this.renderListItem.bind(this)}
            />
        );
    }

    renderListItem({item}) {
        const {title, start_sale_date,zone_list} = item;
        return (
            <TouchableOpacity style={styles.cardItem} onPress={() => this.selectItem(item)}>
                <View>
                    <Text style={[styles.textItemStyle, {textAlign: 'center'}]}>
                        {`Ticket  :  ${title}`}
                    </Text>
                    <View style={{marginBottom: 10, flexDirection: 'row', marginTop: 20}}>
                        <Text style={[styles.textItemStyle, {marginLeft: 20}]}>
                            Date :
                        </Text>
                        <Text style={[styles.textItemStyle, {marginLeft: 20}]}>
                            {start_sale_date}
                        </Text>
                    </View>
                    <Text style={[styles.textItemStyle,{marginLeft:20}]}>
                        {`Zone  :  ${zone_list}`}
                    </Text>
                </View>
            </TouchableOpacity>

        );
    }

    selectItem(event){
        // this.props.navigation.navigate('option', {event: this.props.navigation.state.params.obj, ticket: event})
        this.props.navigation.navigate('register', {
            event: this.props.navigation.state.params.obj,
            ticket: event,
            zoneid: this.props.navigation.state.params.zoneid
        })
    }

}

const styles = StyleSheet.create({
    image: {
        width: width,
        height: height * 0.2,
        backgroundColor: 'grey'
    },
    textTitle: {
        fontSize: fontSize.large,
        color: color.textTitle
    },
    textSubtitle: {
        fontSize: fontSize.small,
        color: 'gray',
        fontFamily: Light.font
    },
    textDetail: {
        fontSize: fontSize.medium,
        color: color.textBlue
    },
    line: {
        borderBottomColor: 'black',
        borderBottomWidth: 1,
        marginTop: 5,
        marginBottom: 5,
        width: '12%',
    },
    space: {
        marginTop: 1.5,
        marginBottom: 1.5
    }
    , titleIcon: {
        width: width * 0.035,
        height: height * 0.028,
        tintColor: '#FFFFFF'
    },
    cardItem: {
        padding: 10,
        margin: 30,
        borderRadius: 5,
        borderWidth: 1,
        borderColor: '#d6d7da',
    },
    textItemStyle: {
        fontSize: 30,
        color: 'black',
        fontFamily: Medium.font,
    }
});
const mapStateToProps = ({ticket}) => {
    return {
        ticket
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Home);
