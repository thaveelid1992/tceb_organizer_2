import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {printerUrl} from '../navigation/AppNavigation';

export const getAllTicket = (token,event_id,zoneid) => {
    return (dispatch) => {
        dispatch({
            type: 'TICKET_LOAD_DATA',
        })
        axios.get(`${printerUrl}/getTicket?event_id=${event_id}&zone_id=${zoneid}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                dispatch({
                    type: 'TICKET_SUCCESS',
                    payload: response.data.data
                })
            })
            .catch((error) => {
                Alert.alert(
                    'Error',
                    error.message,
                    [
                        {text: 'OK', onPress: () => dispatch({
                                type: 'TICKET_FAILED',
                                payload: error.message
                            })},
                    ],
                    {cancelable: false}
                )
            });
    }


};
