import axios from 'axios'
import {
    AsyncStorage,
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';

export const setSwitchMode = (status) => {
    return (dispatch) => {
      dispatch({
          type: 'SET_SWITCH_MODE',
          payload: status
      })
    }
};
