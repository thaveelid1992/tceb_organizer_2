import React, { Component } from 'react'
import { color, fontSize } from '../utills/color';
import { Text, TextInput, Platform, StyleSheet, View, Image, TouchableOpacity, FlatList, Alert, WebView } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Input, Item, Content } from 'native-base';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Strings } from '../utills/StringConfig';
import { Medium, Light } from '../utills/Fonts';
import { NavigationActions } from "react-navigation";
import {termUrl} from '../navigation/AppNavigation';

export default class term extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isZone: true,
            isOvarall: false
        }
    }

    backPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    render() {
        return (
            <Container style={{backgroundColor : color.white}}>
                <Header style={{ backgroundColor: color.primary }}>
                    <Left>
                        <TouchableOpacity onPress={this.backPress.bind(this)}>
                            <Image source={require('../assets/images/ic_back.png')}
                                style={styles.btnImage} resizeMode='contain' />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.textTitleActionBar}>{Strings.title_term}</Title>
                    </Body>
                    <Right />
                </Header>
                <WebView
                    source={{ uri: `${termUrl}` }}
                />
            </Container >
        )
    }
}

const styles = StyleSheet.create({
    btnImage: {
        height: 28,
        width: 28
    },
    textTitleActionBar: {
        color: color.white,
        fontSize: fontSize.large,
        fontFamily: Medium.font
    },
})