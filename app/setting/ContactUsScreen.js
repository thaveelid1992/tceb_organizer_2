import React, { Component } from 'react'
import { color, fontSize } from '../utills/color';
import { Text, TextInput, Platform, StyleSheet, View, Image, TouchableOpacity, FlatList, Alert } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Input, Item, Content } from 'native-base';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { Strings } from '../utills/StringConfig';
import { Medium, Light } from '../utills/Fonts';
import { NavigationActions } from "react-navigation";

export default class Agenda extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isZone: true,
            isOvarall: false
        }
    }

    backPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.navigation.dispatch(backAction)
    }

    render() {
        return (
            <Container >
                <Header style={{ backgroundColor: color.primary }}>
                    <Left>
                        <TouchableOpacity onPress={this.backPress.bind(this)}>
                            <Image source={require('../assets/images/ic_back.png')}
                                style={styles.btnImage} resizeMode='contain' />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.textTitleActionBar}>{Strings.contact_title}</Title>
                    </Body>
                    <Right />
                </Header>
                <View style={styles.ViewMain}>
                    <View style={styles.ViewText}>
                        <Image source={require('../assets/images/call3x.png')}
                            style={styles.btnImage} />
                        <Text style={styles.TextTopic}>{Strings.set_call}</Text>
                    </View>
                    <View style={styles.ViewLine}>
                        <View style={styles.line}></View>
                    </View>
                    <View style={styles.ViewText}>
                        <Image source={require('../assets/images/mail3x.png')}
                            style={styles.btnImage} />
                        <Text style={styles.TextTopic}>{Strings.set_mail}</Text>
                    </View>
                    <View style={styles.ViewLine}>
                        <View style={styles.line}></View>
                    </View>
                    <View style={styles.ViewText}>
                        <Image source={require('../assets/images/langage3x.png')}
                            style={styles.btnImage} />
                        <Text style={styles.TextTopic}>{Strings.set_web}</Text>
                    </View>
                    <View style={styles.ViewLine}>
                        <View style={styles.line}></View>
                    </View>
                    <View style={styles.ViewText}>
                        <Image source={require('../assets/images/facebook3x.png')}
                            style={styles.btnImage} />
                        <Text style={styles.TextTopic}>{Strings.set_face}</Text>
                    </View>
                    <View style={styles.ViewLine}>
                        <View style={styles.line}></View>
                    </View>
                </View>
            </Container >
        )
    }
}

const styles = StyleSheet.create({
    btnImage: {
        height: 28,
        width: 28
    },
    textTitleActionBar: {
        color: color.white,
        fontSize: fontSize.large,
        fontFamily: Medium.font
    },
    ViewMain: {
        flex: 1,
        padding: 20,
    },
    ViewText: {
        flexDirection: 'row',
    },
    ViewLine: {
        flexDirection: 'row',
        marginVertical: 15,
    },
    line: {
        borderBottomColor: color.black,
        borderBottomWidth: 0.5,
        width: '100%',
    },
    btnImage: {
        resizeMode: 'contain',
        height: 25,
        width: 25
    },
    TextTopic: {
        marginLeft: 10,
        color: color.black,
        textAlign: 'center',
        fontFamily: Light.font,
        fontSize: fontSize.medium
    }
})