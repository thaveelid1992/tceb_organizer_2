import React, {Component} from 'react'
import {
    AsyncStorage,
    Text,
    StyleSheet,
    TouchableOpacity,
    Image,
    Platform,
    Alert,
    SafeAreaView,
    Switch,
    NativeModules
} from 'react-native';
import {Container, Header, Left, Body, Right, Button, Icon, Title, Subtitle, Content, View} from 'native-base';
import {Col, Row, Grid} from "react-native-easy-grid";
import {color, fontSize} from '../utills/color'
import {Strings} from '../utills/StringConfig';
import {Medium, Light} from '../utills/Fonts';
import {NavigationActions} from "react-navigation";
import DeviceInfo from 'react-native-device-info';
import OpenAppSettings from 'react-native-app-settings';
let configkiosk;
import {bindActionCreators} from "redux";
import {connect} from "react-redux";
import * as actions from './Actions';

class Setting extends Component {
    constructor(props) {
        super(props);
        this.state = {
            configkiosk: false,
            isIpad: false,
        };
        alertText = {
            alerttitle: Strings.alert_title_bluetooth,
            alertmsg: Strings.alert_des_bluetooth
        };
    }

    static navigationOptions = {
        header: null,
    };

    componentDidMount() {
        // Alert.alert(multiSearchOr(DeviceInfo.getDeviceId().toString()),'iPad')
        this.setState({isIpad: this.multiSearchOr(DeviceInfo.getDeviceId().toString(), ["iPad", "IPad", "ipad", "IPAD", "Ipad"])});
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const {configkiosk} = JSON.parse(result);
                this.props.actions.setSwitchMode(configkiosk);
                this.setState({configkiosk: configkiosk});
            }
        });
    }

    multiSearchOr(text, searchWords) {
        var searchExp = new RegExp(searchWords.join("|"), "gi");
        return (searchExp.test(text)) ? true : false;
    }

    onPressSettingApp() {
        Alert.alert(
            Strings.alert_title_bluetooth,
            Strings.alert_des_bluetooth,
            [
                {text: Strings.con_blue, onPress: () => OpenAppSettings.open()},
                {text: Strings.dismiss, onPress: () => ''},
            ],
            {cancelable: true}
        )
    }


    onPressLogout() {
        Alert.alert(
            Strings.Confirm_logout_msg,
            Strings.logout_alter_msg,
            [
                {text: Strings.Logout, onPress: () => this.clearLocal()},
                {text: Strings.Cancel, onPress: () => ''},
            ],
            {cancelable: true}
        )
    }

    clearLocal() {
        // Asyncstorage.clear();
        AsyncStorage.removeItem('User', () => {
            const resetAction = NavigationActions.reset({
                index: 0,
                key: null,
                actions: [NavigationActions.navigate({routeName: 'login'})],
            });
            this.props.navigation.dispatch(resetAction);
        })
    }

    onPressContact() {
        this.props.navigation.navigate('contactus')
    }

    onPressPolicy() {
        this.props.navigation.navigate('policy')
    }

    onPressTerm() {
        this.props.navigation.navigate('term')
    }

    toggleSwitch1() {
        this.setState({configkiosk: !this.state.configkiosk});
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                let object = JSON.parse(result);
                object.configkiosk = this.state.configkiosk;
                this.props.actions.setSwitchMode(this.state.configkiosk);
                AsyncStorage.setItem('User', JSON.stringify(object));
            }
        });


    }

    alertMes() {
        Alert.alert(
            alertText.alerttitle,
            alertText.alertmsg,
            [
                {text: 'OK', onPress: () => console.log('OK Pressed')},
            ],
            {cancelable: false}
        )
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1, backgroundColor: color.bg_topic_setting}}>
                <View style={styles.container}>
                    <Text style={styles.headerSetting}>{Strings.setting}</Text>
                    <View style={styles.line}></View>
                    {/* <TouchableOpacity>
                        <View style={styles.viewRow}>
                            <Image style={styles.viewImage}
                                source={require('../assets/images/ic_language_change3x.png')} />
                            <Text style={[styles.textview, { flex: 3 }]}>
                                {Strings.Change_Language}
                            </Text>
                            <Text style={[styles.textview]}>
                                {Strings.English}
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.line}></View> */}
                    <View>
                        <TouchableOpacity onPress={this.onPressContact.bind(this)}>
                            <View style={styles.viewRow}>
                                <Image style={styles.viewImage}
                                       source={require('../assets/images/ic_contact_us3x.png')}/>
                                <Text style={styles.textview}>
                                    {Strings.Contact_Us}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.line}></View>
                    </View>
                    <View>
                        <TouchableOpacity onPress={this.onPressTerm.bind(this)}>
                            <View style={styles.viewRow}>
                                <Image style={styles.viewImage}
                                       source={require('../assets/images/ic_tc3x.png')}/>
                                <Text style={styles.textview}>
                                    {Strings.Terms_and_Conditions}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.line}></View>
                    </View>
                    <View>
                        <TouchableOpacity onPress={this.onPressPolicy.bind(this)}>
                            <View style={styles.viewRow}>
                                <Image style={styles.viewImage}
                                       source={require('../assets/images/ic_privacy_policy3x.png')}/>
                                <Text style={styles.textview}>
                                    {Strings.Privacy_Policy}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.line}></View>
                    </View>
                    <TouchableOpacity>
                        <View style={styles.viewRow}>
                            <Image style={styles.viewImage}
                                   source={require('../assets/images/information.png')}/>
                            <Text style={[styles.textview, {flex: 3}]}>
                                {Strings.version_app}
                            </Text>
                            <Text style={[styles.textview]}>
                                1.0.8
                            </Text>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.line}></View>
                    {/*select Info*/}
                    {this.renderIpad()}
                    <View>
                        <TouchableOpacity onPress={this.onPressLogout.bind(this)}>
                            <View style={styles.viewRow}>
                                <Image style={styles.viewImage}
                                       source={require('../assets/images/ic_logout3x.png')}/>
                                <Text style={styles.textview}>
                                    {Strings.Logout}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        <View style={styles.line}></View>
                    </View>
                </View>
            </SafeAreaView>
        );
    }

    renderIpad() {
        if (this.state.isIpad) {
            return (
                <View>
                    <View>
                        <TouchableOpacity>
                            <View style={styles.viewRow}>
                                <Image style={styles.viewImage}
                                       source={require('../assets/images/information.png')}/>
                                <Text style={[styles.textview, {flex: 3}]}>
                                    {Strings.Kiosk_mode}
                                </Text>
                                <View style={styles.container}>
                                    <Switch
                                        value={this.state.configkiosk}
                                        onValueChange={this.toggleSwitch1.bind(this)}/>
                                </View>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.line}></View>
                    <View>
                        <TouchableOpacity onPress={this.onPressSettingApp.bind(this)}>
                            <View style={styles.viewRow}>
                                <Image style={styles.viewImage}
                                       source={require('../assets/images/information.png')}/>
                                <Text style={[styles.textview, {flex: 3}]}>
                                    {Strings.printer_mode}
                                </Text>
                            </View>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.line}></View>
                </View>
            );
        }
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.white,
    },
    line: {
        borderBottomColor: color.textFilterBlue,
        borderBottomWidth: 0.5,
        width: '100%',
    },
    headerSetting: {
        paddingHorizontal: 20,
        paddingBottom: 20,
        paddingTop: Platform.OS === 'ios' ? 30 : 20,
        color: color.textFilterBlue,
        fontSize: fontSize.large,
        backgroundColor: color.bg_topic_setting
    },
    viewRow: {
        flexDirection: 'row',
        paddingVertical: 20,
        paddingHorizontal: 30
    },
    viewImage: {
        width: 20,
        height: 20,
        marginRight: 20
    },
    textview: {
        color: color.textFilterBlue,
        fontSize: fontSize.medium,
        fontFamily: Light.font,
    },
});
const mapStateToProps = ({setting}) => {
    return {
        setting
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(Setting);
