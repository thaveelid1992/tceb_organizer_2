/**
 * Created by jaskkit on 1/19/2018 AD.
 */
const INITIAL_STATE = {
    loading: false,
    message: '',
    success:true,
    forgetSuccess:false,
};
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'EMAIL_ACTION_LOADING':
            return {
                ...state,
                loading: true,
                success:true,
            };
        case 'EMAIL_ACTION_SUCCESS':
            return {
                ...state,
                loading: false,
                success:true,
                message:action.payload.message,
                forgetSuccess:action.payload.success
            };
        case 'EMAIL_ACTION_FAILED':
            return {
                ...state,
                loading: false,
                success:false,
                message:action.payload.message,
                forgetSuccess:action.payload.success
            };
        case 'EMAIL_RESET_PROPS':
            return {
                ...state,
                loading: false,
                message: '',
                success:true,
                forgetSuccess:false,
            };
        default:
            return state;
    }
};
