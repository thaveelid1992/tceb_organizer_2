import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Image, TouchableHighlight, Keyboard, Alert, SafeAreaView } from 'react-native';
import { Form, Item, Input, Label, Icon, Left, Container, Header, Body, Right, Title } from 'native-base';
import { color, fontSize } from '../utills/color';
import { NavigationActions } from "react-navigation";
import { Strings } from '../utills/StringConfig';
import { Medium, Light } from '../utills/Fonts';
import * as actions from './ForgetAction'
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import Loading from '../components/Loading';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

const tcelogo = require('../assets/images/tceb-logo.png');
const ic_back = require('../assets/images/ic_back.png');

let isVaildEmail = false;

class forgot extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: ''
    };
  }

  backPress() {
    const backAction = NavigationActions.back({
      key: null
    })
    this.props.navigation.dispatch(backAction)
  }

  sendmail() {
    Keyboard.dismiss();
    if (this.validation()) {
      this.props.actions.forgetPassword(this.state.email);
    }
    // this.props.navigation.navigate('resetpassword')
  }

  validation() {
    let isdata = true;
    if (this.state.email.length === 0) {
      Alert.alert(
        Strings.Error,
        Strings.Email_is_required,
        [
          { text: Strings.OK, onPress: () => '' },
        ],
        { cancelable: false }
      )
      return false;
    }
    if (!this.validateEmail(this.state.email)) {
      this.isdata = false
      Alert.alert(
        Strings.Error,
        Strings.Email_is_invalid,
        [
          { text: Strings.OK, onPress: () => '' },
        ],
        { cancelable: false }
      )
      return false;
    }
    return isdata
  }

  validateEmail(email) {
    let re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
  }

  onEmailChange(text) {
    if (this.validateEmail(text)) {
      this.isVaildEmail = true
    } else {
      this.isVaildEmail = false
    }
    this.setState({ email: text });
  }

  componentWillReceiveProps(nextProps) {
    if (!nextProps.forgot.success && !nextProps.forgot.forgetSuccess) {
      Alert.alert(
        Strings.Error,
        nextProps.forget.message,
        [
          { text: Strings.OK, onPress: () => this.props.actions.resetProps() },
        ],
        { cancelable: false }
      )
      this.props.actions.resetProps();
    }
    if (nextProps.forgot.forgetSuccess && nextProps.forgot.success) {
      this.props.actions.resetProps();
      this.props.navigation.navigate('resetpass', { email: this.state.email })
    }
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={styles.container}>
          <Loading loading={this.props.forgot.loading} />
          <KeyboardAwareScrollView style={styles.keyboard} >
            <View style={styles.containerActionBar}>
              <TouchableOpacity onPress={this.backPress.bind(this)} style={{ flex: 0.1 }}>
                <Image source={ic_back}
                  style={styles.btnImage} resizeMode='contain' />
              </TouchableOpacity>
              <View style={{ flex: 0.8 }}>
                <Text style={[styles.textTitleActionBar, { textAlign: 'center' }]}>{Strings.forgot_password_header}</Text>
              </View>
            </View>
            <View style={styles.containerContent}>
              <Image
                style={{ width: 100, height: 100, marginBottom: 40, marginTop: 20 }}
                source={tcelogo} />
              <View style={[styles.itemInputText, { marginBottom: 20 }]}>
                <Label style={styles.textTitle}>{Strings.email}</Label>
                <Item>
                  <Input style={styles.textBox}
                    onChangeText={(text) => this.onEmailChange(text)}
                    value={this.state.username}
                  />
                </Item>
              </View>
              <View style={styles.layoutlogincontainer}>
                <TouchableHighlight
                  style={this.isVaildEmail ? styles.loginSuccess : styles.login}
                  onPress={this.sendmail.bind(this)}
                  underlayColor={color.white}>
                  <Text style={styles.loginText}>{Strings.sent_to_mail}</Text>
                </TouchableHighlight>
              </View>
            </View>
          </KeyboardAwareScrollView>
        </View>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: color.primary
  },
  containerActionBar: {
    flexDirection: 'row',
    marginTop: Platform.OS === 'ios' ? 20 : 10,
    marginLeft: 10,
  },
  containerContent: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 40,
    marginLeft: 40,
    marginTop: '30%'
  },
  textTitleActionBar: {
    color: color.white,
    fontSize: fontSize.large,
    fontFamily: Medium.font,
  },
  itemInputText: {
    alignItems: 'flex-start',
  },
  btnImage: {
    height: 28,
    width: 28
  },
  textTitle: {
    fontSize: fontSize.medium,
    marginLeft: 3,
    marginTop: Platform.OS === 'ios' ? 5 : 3,
    backgroundColor: 'rgba(0,0,0,0)',
    color: color.white,
    fontFamily: Light.font,
  },
  textBox: {
    fontSize: fontSize.medium,
    alignSelf: 'stretch',
    color: color.white,
    fontFamily: Light.font,
  },
  loginText: {
    color: color.white,
    textAlign: 'center',
    fontFamily: Medium.font,
  },
  keyboard: {
    alignSelf: 'stretch',
    // marginRight: 40,
    // marginLeft: 40
  },
  login: {
    flex: 1,
    marginTop: 10,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: color.buttonLoginUnSuscess,
    borderRadius: 5,
  },
  loginSuccess: {
    flex: 1,
    marginTop: 10,
    paddingTop: 15,
    paddingBottom: 15,
    backgroundColor: color.buttonLoginSuscess,
    borderRadius: 5,
  },
  layoutlogincontainer: {
    marginTop: 20,
    justifyContent: 'center',
    flexDirection: 'row'
  },
});

const mapStateToProps = ({ forgot }) => {
  return {
    forgot
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    actions: bindActionCreators(actions, dispatch)
  }
};
export default connect(mapStateToProps, mapDispatchToProps)(forgot);