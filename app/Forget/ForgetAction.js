/**
 * Created by jaskkit on 1/19/2018 AD.
 */
import axios from 'axios'
import {baseUrl} from '../navigation/AppNavigation';
import { Strings } from '../utills/StringConfig';
export const forgetPassword = (email) => {
    return (dispatch) => {
        dispatch({
            type: 'EMAIL_ACTION_LOADING',
        })
        axios.post(`${baseUrl}/forgetPassword`, {
            email: email
        }, {
            headers: {
                'Content-Type': 'application/json',
            }
        }).then(function (response) {
            if(response.data.success){
                dispatch({
                    type: 'EMAIL_ACTION_SUCCESS',
                    payload: response.data
                })

            }else{
                dispatch({
                    type: 'EMAIL_ACTION_FAILED',
                    payload: response.data
                })
            }
        }).catch(function (error) {
            dispatch({
                type: 'EMAIL_ACTION_FAILED',
                payload: error
            })
        });
    }
};
export const resetProps = () => {
    return (dispatch) => {
        dispatch({
            type: 'EMAIL_RESET_PROPS',
        });
    }
};