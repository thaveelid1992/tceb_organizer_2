import React, { Component } from 'react'
import { color, fontSize } from '../utills/color';
import { Text, TextInput, Platform, StyleSheet, View, Image, TouchableOpacity, FlatList, Alert, Modal, Dimensions, ScrollView, AsyncStorage, SafeAreaView } from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon, Title, Input, Item, Content } from 'native-base';
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as actions from './FloorPlanAction';
import { Strings } from '../utills/StringConfig';
import { Medium, Light } from '../utills/Fonts';
import { NavigationActions } from "react-navigation";
import ImageViewer from 'react-native-image-zoom-viewer';
import FastImage from 'react-native-fast-image';
import Loading from '../components/Loading';

const { width, height } = Dimensions.get('window');

let floorplan = require('../assets/images/ic_notfound.png');
const close = require('../assets/images/ic_action_close.png');
const ic_notfound = require('../assets/images/ic_notfound.png');

const formatData = (data, numColumns) => {
    const numberOfFullRows = Math.floor(data.length / numColumns);

    let numberOfElementsLastRow = data.length - (numberOfFullRows * numColumns);
    while (numberOfElementsLastRow !== numColumns && numberOfElementsLastRow !== 0) {
        data.push({ key: `blank-${numberOfElementsLastRow}`, empty: true });
        numberOfElementsLastRow++;
    }
    return data;
};

const numColumns = 3;

let images

let indexlist = 0 ;

class floorplanreducer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            isZone: true,
            isOvarall: false,
            ModalVisibleStatus: false,
            setimg: true
        }
    }

    componentDidMount() {
        indexlist = 0;
        const { params } = this.props.navigation.state;
        AsyncStorage.getItem('User', (err, result) => {
            if (result) {
                const { api_token } = JSON.parse(result);
                api_tokenMain = api_token;
                this.props.actions.getEventFloorPlan(api_tokenMain, params.data.event.id);
            }
        });
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.floorplanreducer.data.length > 0 && nextProps.floorplanreducer.data != null) {
            this.setFirstImg(nextProps.floorplanreducer.data[0].image)
            images = nextProps.floorplanreducer.listimg;
        }
    }

    backPress() {
        const backAction = NavigationActions.back({
            key: null
        })
        this.props.actions.setDefault()
        this.props.navigation.dispatch(backAction)
    }

    ShowModalFunction(visible) {
        this.setState({ ModalVisibleStatus: false });
    }

    onPressdisZoom() {
        this.setState({ ModalVisibleStatus: false });
    }

    onPressZoom() {
        this.setState({ ModalVisibleStatus: true });
    }

    onPressdSetImg(image,index) {
        indexlist = index;
        floorplan = { uri: image };
        this.setState({ setimg: !this.state.setimg });
    }

    setFirstImg(image) {
        floorplan = { uri: image };
    }

    renderHeaderModel() {
        return (
            <View style={styles.viewHeader}>
                <TouchableOpacity onPress={this.onPressdisZoom.bind(this)}>
                    <FastImage
                        style={{ width: 28, height: 28 }}
                        source={close} />
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        return (
            <Container style={styles.bgwhite}>
                <Header style={{ backgroundColor: color.primary }}>
                    <Left>
                        <TouchableOpacity onPress={this.backPress.bind(this)}>
                            <FastImage source={require('../assets/images/ic_back.png')}
                                style={styles.btnImage} resizeMode='contain' />
                        </TouchableOpacity>
                    </Left>
                    <Body>
                        <Title style={styles.textTitleActionBar}>{Strings.floor_plan_title}</Title>
                    </Body>
                    <Right />
                </Header>
                {this.isdata()}
            </Container >
        )
    }


    isdata() {
        const { data } = this.props.floorplanreducer
        if (this.props.floorplanreducer.loading) {
            return (
                <View style={styles.container}>
                    <Loading loading={this.props.floorplanreducer.loading} />
                </View>)
        } else {
            if (data && data.length > 0) {
                return (
                    <ScrollView>
                        <View style={styles.bgwhite}>
                            <Loading loading={this.props.floorplanreducer.loading} />
                            <View style={styles.MainContainer}>
                                <FlatList
                                    data={formatData(this.props.floorplanreducer.data, numColumns)}
                                    renderItem={this.renderListItem.bind(this)}
                                    numColumns={numColumns} />
                                <View >
                                    <TouchableOpacity onPress={this.onPressZoom.bind(this)}>
                                        <FastImage
                                            style={{ width: width * 0.8, height: height * 0.5, alignSelf: 'center' }}
                                            source={floorplan}
                                            resizeMode='contain'
                                        />
                                    </TouchableOpacity>
                                </View>
                                <Modal
                                    visible={this.state.ModalVisibleStatus}
                                    transparent={true}
                                    onRequestClose={() => this.ShowModalFunction()}>
                                    <SafeAreaView style={{ flex: 1, backgroundColor: color.black }}>
                                        <ImageViewer index={indexlist} imageUrls={images}
                                            renderHeader={this.renderHeaderModel.bind(this)} />
                                    </SafeAreaView>
                                </Modal>
                            </View>
                        </View>
                    </ScrollView>
                );
            } else {
                return (
                    <View style={styles.container}>
                        <View style={{ flex: 1, alignSelf: 'center', alignItems: 'center', justifyContent: 'center' }}>
                            <FastImage source={ic_notfound} style={{ width: width * 0.3, height: height * 0.3 }}
                                resizeMode='stretch' />
                            <Text style={{ marginTop: height * 0.03, fontSize: 24, color: '#8CA0B3' }}>
                                {Strings.not_found}
                            </Text>
                        </View>
                    </View>
                )
            }
        }
    }

    renderListItem({ item,index }) {
        const { id, title, image, description, empty } = item;
        if (empty === true) {
            return <View style={[styles.item, styles.itemInvisible]} />;
        }
        return (
            <View style={{ marginRight: 10, flex: 1 }}>
                <TouchableOpacity
                    style={styles.loginSuccess}
                    onPress={this.onPressdSetImg.bind(this, item.image,index)}
                    underlayColor={color.white}>
                    <Text numberOfLines={2} style={styles.loginText}>{title}</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: color.white
    },
    bgwhite: {
        backgroundColor: color.white
    },
    btnImage: {
        height: 28,
        width: 28
    },
    viewHeader: {
        marginTop: Platform.OS === 'ios' ? 20 : 10,
        marginLeft: 10
    },
    textTitleActionBar: {
        color: color.white,
        fontSize: fontSize.large,
        fontFamily: Medium.font
    },
    MainContainer: {
        flex: 1,
        alignItems: 'stretch',
        margin: 20,
    },
    loginSuccess: {
        marginTop: 10,
        paddingTop: 15,
        paddingBottom: 15,
        backgroundColor: color.buttonLoginSuscess,
        borderRadius: 5,
    },
    loginText: {
        color: color.white,
        textAlign: 'center',
        fontFamily: Medium.font,
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    itemInvisible: {
        backgroundColor: 'transparent',
    },
    item: {
        marginRight: 10,
        backgroundColor: '#4D243D',
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        height: Dimensions.get('window').width / numColumns,
    },
})

const mapStateToProps = ({ floorplanreducer }) => {
    return {
        floorplanreducer
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        actions: bindActionCreators(actions, dispatch)
    }
};
export default connect(mapStateToProps, mapDispatchToProps)(floorplanreducer)