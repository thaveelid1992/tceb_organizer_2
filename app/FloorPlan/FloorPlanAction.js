import axios from 'axios'
import {
    Alert
} from 'react-native';
import {baseUrl} from '../navigation/AppNavigation';
import { Strings } from '../utills/StringConfig';

export const getEventFloorPlan = (token, event_id) => {
    return (dispatch) => {
        dispatch({
            type: 'FLOOR_LOAD_DATA',
        })
        axios.get(`${baseUrl}/getEventFloorPlan?event_id=${event_id}`, {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': `Bearer ${token}`
            }
        })
            .then((response) => {
                dispatch({
                    type: 'FLOOR_SUCCESS',
                    payload: response.data
                })
            })
            .catch((error) => {
                dispatch({
                    type: 'FLOOR_FAILED',
                    payload: response.data
                })
                Alert.alert(
                    Strings.Error,
                    error.message,
                    [
                        { text: Strings.OK, onPress: () => '' },
                    ],
                    { cancelable: false }
                )
            });
    }
};

export const setDefault = () => {
    return (dispatch) => {
        dispatch({
            type: 'FLOOR_DEFAULT',
        })
    }
};