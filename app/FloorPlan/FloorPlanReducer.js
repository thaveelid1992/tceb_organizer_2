const INITIAL_STATE = {
    data: [],
    message: '',
    status: false,
    loading: false,
    success: true,
    listimg: []
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'FLOOR_LOAD_DATA':
            return {
                ...state,
                loading: true,
                success: true,
            };
        case 'FLOOR_SUCCESS':
            let list = [];
            if (action.payload.floor_plans.length > 0 && action.payload.floor_plans != null) {
                let arr = {};
                for (let i of action.payload.floor_plans) {
                    arr = { url: i.image }
                    list.push(arr)
                }
            }
            return {
                ...state,
                data: action.payload.floor_plans,
                listimg : list,
                message: '',
                status: false,
                loading: false,
                success: true,
            };
        case 'FLOOR_FAILED':
            return {
                ...state,
                data: [],
                message: '',
                status: false,
                loading: false,
                success: false,
            };
        case 'FLOOR_DEFAULT':
            return {
                ...state,
                data: [],
                message: '',
                status: false,
                loading: false,
                success: true,
                listimg: [],
            };
        default:
            return state;
    }
};