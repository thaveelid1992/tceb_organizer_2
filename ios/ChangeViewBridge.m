//
//  ChangeViewBridge.m
//  bizconnectKeyos
//
//  Created by thejong on 11/1/2562 BE.
//  Copyright © 2562 Facebook. All rights reserved.
//

#import "ChangeViewBridge.h"
#import "AppDelegate.h"
@implementation ChangeViewBridge
RCT_EXPORT_MODULE(ChangeViewBridge);

RCT_EXPORT_METHOD(changeToNativeView:(NSString*)str) {
  NSLog(@"RN binding - Native View - Loading MyViewController.swift");
  AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
  [appDelegate goToNativeViewWithHtmlString:str];
}
RCT_EXPORT_METHOD(changeToSetupPrinterView) {
  NSLog(@"RN binding - Native View - Loading MyViewController.swift");
  AppDelegate *appDelegate = (AppDelegate *)[UIApplication sharedApplication].delegate;
  [appDelegate goToSettingPrintView];
}
+ (id)allocWithZone:(NSZone *)zone {
  static ChangeViewBridge *sharedInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedInstance = [super allocWithZone:zone];
  });
  return sharedInstance;
}
- (NSArray<NSString *> *)supportedEvents
{
  return @[@"closeView"];
}

- (void)setEventReceived
{
  [self sendEventWithName:@"closeView" body:@{@"name": @"backReact"}];
}

@end
