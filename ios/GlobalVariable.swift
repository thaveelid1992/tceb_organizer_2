
//
//  GlobalVariable.swift
//  bizconnectKeyos
//
//  Created by thejong on 15/1/2562 BE.
//  Copyright © 2562 Facebook. All rights reserved.
//

import Foundation
public class GlobalVariable: NSObject {
  static var devicePrinter:String = ""
  static var strModelName:String = ""
  static var strSerialNumber:String = ""

  static var selectedDeviceInfo : BRPtouchDeviceInfo?

  static func setDevicePrinter(deviceInfo:BRPtouchDeviceInfo){
    DispatchQueue.main.async {
    print("deviceInfo : \(deviceInfo)")
    UserDefaults.standard.set(deviceInfo.strModelName, forKey: "strModelName")
    UserDefaults.standard.set(deviceInfo.strSerialNumber, forKey: "strSerialNumber")
    strModelName  = UserDefaults.standard.object(forKey: "strModelName") as! String
    strSerialNumber  = UserDefaults.standard.object(forKey: "strSerialNumber") as! String
    UserDefaults.standard.synchronize()
    }
  }
  static func isDeviceInfo()->Bool{
    if (UserDefaults.standard.value(forKey: "strModelName") != nil) {
      return true

    }else{
      return false

    }
  }
  static func getDevicePrinter(){
    strModelName  = UserDefaults.standard.object(forKey: "strModelName") as! String
    strSerialNumber  = UserDefaults.standard.object(forKey: "strSerialNumber") as! String
    
  }
}
