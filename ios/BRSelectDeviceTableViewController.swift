//
//  BRSelectDeviceTableViewController.swift
//  SDK_Sample_Swift
//
//  Copyright © 2017 Brother Industries, Ltd. All rights reserved.
//

import Foundation
import UIKit

@objc protocol BRSelectDeviceTableViewControllerDelegate : NSObjectProtocol {
    func setSelected(deviceInfo: BRPtouchDeviceInfo)
}

@objc class BRSelectDeviceTableViewController: UITableViewController,CBCentralManagerDelegate {
    var manager:CBCentralManager!

    @objc var delegate: BRSelectDeviceTableViewControllerDelegate?
    @objc var deviceListByMfi : [BRPtouchDeviceInfo]?
    @objc var isPresentView : Bool = false

    
    deinit {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.BRDeviceDidConnect, object: nil)
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.BRDeviceDidDisconnect, object: nil)
    }
  func centralManagerDidUpdateState(_ central: CBCentralManager) {
    switch central.state {
    case .poweredOn:
      break
    case .poweredOff:
      print("Bluetooth is Off.")
      self.presentAlertWithTitle(title: "Bizconnect", message: "Please check Printer is connected. \nHow to \n1. Go to Settings > Bluetooth. \n 2. Make sure that Printer is connected.", options:"Close" ) { (option) in
        switch(option) {
     
        case 0:
          print("option two")
          self.dismiss(animated: true, completion: nil)
        default:
          break
        }
      }
      break
    case .resetting:
      break
    case .unauthorized:
      break
    case .unsupported:
      break
    case .unknown:
      break
    default:
      break
    }
  }
  @objc func backAction() -> Void {
      self.dismiss(animated: true, completion: nil)
    }
    func setupBackBtn() {
      let backButton = UIButton(type: .custom)
      //backButton.setImage(UIImage(named: "BackButton.png"), for: .normal) // Image can be downloaded from here below link
      backButton.setTitle("Back", for: .normal)
      backButton.setTitleColor(backButton.tintColor, for: .normal) // You can change the TitleColor
      backButton.addTarget(self, action: #selector(self.backAction), for: .touchUpInside)
      
      self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
      if isPresentView {
        self.setupBackBtn()
      }
      manager = CBCentralManager()
      manager.delegate = self
        BRPtouchBluetoothManager.shared()?.registerForBRDeviceNotifications()
        NotificationCenter.default.addObserver(self, selector: #selector(accessoryDidConnect), name: NSNotification.Name.BRDeviceDidConnect , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(accessoryDidDisconnect), name: NSNotification.Name.BRDeviceDidDisconnect, object: nil)
      
      self.presentAlertWithTitle(title: "Bizconnect", message: "**If this list not appear your printer name,\nplease check Printer is paired. ", options:"Go to Blutooh Setting.","Dismiss" ) { (option) in
        switch(option) {
        case 0:
          print("option one")
          self.openBluetooth()
          
          break
        case 1:
          print("option two")
          //self.dismiss(animated: true, completion: nil)
        default:
          break
        }
      }
      
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        deviceListByMfi = BRPtouchBluetoothManager.shared()?.pairedDevices() as? [BRPtouchDeviceInfo] ?? []
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func accessoryDidConnect( notification : Notification) {
        if let connectedAccessory = notification.userInfo?[BRDeviceKey] {
            print("ConnectDevice : \(String(describing: (connectedAccessory as? BRPtouchDeviceInfo)?.description()))")
        }

        deviceListByMfi = BRPtouchBluetoothManager.shared()?.pairedDevices() as? [BRPtouchDeviceInfo] ?? []
        
        self.tableView.reloadData()
    }
    
    @objc func accessoryDidDisconnect( notification : Notification) {
        if let disconnectedAccessory = notification.userInfo?[BRDeviceKey] {
            print("DisconnectDevice : \(String(describing: (disconnectedAccessory as? BRPtouchDeviceInfo)?.description()))")
        }

        deviceListByMfi = BRPtouchBluetoothManager.shared()?.pairedDevices()  as? [BRPtouchDeviceInfo] ?? []
        
        self.tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return deviceListByMfi?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "selectDeviceTableViewCell"
        let cell = tableView.dequeueReusableCell(withIdentifier: cellIdentifier) ?? UITableViewCell(style: .subtitle, reuseIdentifier: cellIdentifier)

        cell.textLabel?.text = deviceListByMfi?[indexPath.row].strModelName ?? nil
        cell.detailTextLabel?.text = deviceListByMfi?[indexPath.row].strSerialNumber ?? nil
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let deviceInfo = deviceListByMfi?[indexPath.row] {
          if(self.delegate != nil){
            self.delegate?.setSelected(deviceInfo: deviceInfo)

          }
          GlobalVariable.strModelName  = deviceInfo.strModelName as! String
          GlobalVariable.strSerialNumber  = deviceInfo.strSerialNumber as! String
          GlobalVariable.setDevicePrinter(deviceInfo: deviceInfo)
          

          if isPresentView {
            // being presented
            self.dismiss(animated: true, completion: nil)

          }else{
            // being pushed
            let deadlineTime = DispatchTime.now()
            DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "STARTPRINT"), object: nil)
            }
            self.navigationController?.popViewController(animated: true)

          }

        }
    }
  func openBluetooth(){
//    let url = URL(string: "App-Prefs:root=Bluetooth") //for bluetooth setting
//    let app = UIApplication.shared
//    app.openURL(url!)
//    //self.dismiss(animated: true, completion: nil)
    
  }
}
