//
//  ChangeViewBridge.h
//  bizconnectKeyos
//
//  Created by thejong on 11/1/2562 BE.
//  Copyright © 2562 Facebook. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

NS_ASSUME_NONNULL_BEGIN

NS_ASSUME_NONNULL_END

@interface ChangeViewBridge : RCTEventEmitter <RCTBridgeModule>
+ (id)allocWithZone:(NSZone *)zone;
- (void) changeToNativeView:(NSString*)str;
- (void) changeToSetupPrinterView;
- (void)setEventReceived;
@end

