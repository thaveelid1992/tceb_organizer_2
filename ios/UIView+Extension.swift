//
//  UIView+Extension.swift
//  SDK_Sample_Swift
//
//  Created by thejong on 10/1/2562 BE.
//  Copyright © 2562 kusumona. All rights reserved.
//

import Foundation
extension UIView {
    
    func takeScreenshot() -> UIImage {
        
        // Begin context
        UIGraphicsBeginImageContextWithOptions(self.bounds.size, false, UIScreen.main.scale)
        
        // Draw view in that context
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        // And finally, get image
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        if (image != nil)
        {
            return image!
        }
        return UIImage()
    }
}
extension UIViewController {
  
  var isModal: Bool {
    
    let presentingIsModal = presentingViewController != nil
    let presentingIsNavigation = navigationController?.presentingViewController?.presentedViewController == navigationController
    let presentingIsTabBar = tabBarController?.presentingViewController is UITabBarController
    
    return presentingIsModal || presentingIsNavigation || presentingIsTabBar || false
  }
  func alert(message: String, title: String = "") {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
    alertController.addAction(OKAction)
    self.present(alertController, animated: true, completion: nil)
  }
  func presentAlertWithTitle(title: String, message: String, options: String..., completion: @escaping (Int) -> Void) {
    let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
    for (index, option) in options.enumerated() {
      alertController.addAction(UIAlertAction.init(title: option, style: .default, handler: { (action) in
        completion(index)
      }))
    }
    self.present(alertController, animated: true, completion: nil)
  }
}
