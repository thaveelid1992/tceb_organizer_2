/**
 * Copyright (c) Facebook, Inc. and its affiliates.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */

#import "AppDelegate.h"

#import <React/RCTBridge.h>
#import <React/RCTBundleURLProvider.h>
#import <React/RCTRootView.h>
#import <NewOrganizer2-Swift.h>

@implementation AppDelegate
UIViewController *rootViewController;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
  RCTBridge *bridge = [[RCTBridge alloc] initWithDelegate:self launchOptions:launchOptions];
  RCTRootView *rootView = [[RCTRootView alloc] initWithBridge:bridge
                                                   moduleName:@"NewOrganizer2"
                                            initialProperties:nil];

  rootView.backgroundColor = [[UIColor alloc] initWithRed:1.0f green:1.0f blue:1.0f alpha:1];

  self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
  rootViewController = [UIViewController new];
  rootViewController.view = rootView;
  self.window.rootViewController = rootViewController;
  [self.window makeKeyAndVisible];
  return YES;
}

- (NSURL *)sourceURLForBridge:(RCTBridge *)bridge
{
#if DEBUG
  return [[RCTBundleURLProvider sharedSettings] jsBundleURLForBundleRoot:@"index" fallbackResource:nil];
#else
  return [[NSBundle mainBundle] URLForResource:@"main" withExtension:@"jsbundle"];
#endif
}
- (void) goToNativeViewWithHtmlString:(NSString*)str{
   NSLog(@"RN binding - Native View - MyViewController.swift - Load From main storyboard");
  dispatch_async(dispatch_get_main_queue(), ^{
    // view instantition here.
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BRSimplePrintViewController" bundle:nil];
    BRSimplePrintViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BRSimplePrintViewController"];
    vc.printContentStr = str;
    UINavigationController *navView = [[UINavigationController alloc] initWithRootViewController:vc];
    [rootViewController presentViewController:navView animated:NO completion:nil];
  });
  
  //  BRSimplePrintViewController *vc = [UIStoryboard storyboardWithName:@"BRSimplePrintViewController" bundle:nil].instantiateInitialViewController;
  //self.window.rootViewController = vc;
  // [self.window.rootViewController presentViewController:vc animated:YES completion:nil];
  
  
#define ROOTVIEW [[[UIApplication sharedApplication] keyWindow] rootViewController]
  // [ROOTVIEW presentViewController:vc animated:YES completion:^{}];
  
  //  UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
  //
  //    while (topController.presentedViewController) {
  //    topController = topController.presentedViewController;
  //    }
  
}

- (void) goToSettingPrintView{
  dispatch_async(dispatch_get_main_queue(), ^{
    // view instantition here.
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"BRSimplePrintViewController" bundle:nil];
    BRSelectDeviceTableViewController *vc = [storyboard instantiateViewControllerWithIdentifier:@"BRSelectDeviceTableViewController"];
    vc.isPresentView = YES;
    UINavigationController *navView = [[UINavigationController alloc] initWithRootViewController:vc];
    
    [rootViewController presentViewController:navView animated:NO completion:nil];
  });
}
+ (UIViewController*) topMostController
{
  UIViewController *topController = [UIApplication sharedApplication].keyWindow.rootViewController;
  
  while (topController.presentedViewController) {
    topController = topController.presentedViewController;
  }
  
  return topController;
}
@end
