//
//  BRSimplePrintViewController.swift
//  SDK_Sample_Swift
//
//  Copyright © 2017 Brother Industries, Ltd. All rights reserved.
//

import Foundation
import UIKit
@objc class BRSimplePrintViewController: UIViewController, BRSelectDeviceTableViewControllerDelegate ,UIWebViewDelegate{

    var selectedDeviceInfo : BRPtouchDeviceInfo?
    var imageQr : UIImage?

    @objc public var printContentStr:String = ""
    @IBOutlet weak var webView: UIWebView!
  @objc func backAction() -> Void {
    self.dismiss(animated: true, completion: nil)
  }
  func setupBackBtn() {
    let backButton = UIButton(type: .custom)
    //backButton.setImage(UIImage(named: "BackButton.png"), for: .normal) // Image can be downloaded from here below link
    backButton.setTitle("Cancel Print", for: .normal)
    backButton.setTitleColor(backButton.tintColor, for: .normal) // You can change the TitleColor
    backButton.addTarget(self, action: #selector(self.backAction), for: .touchUpInside)
    
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(customView: backButton)
  }
    override func viewDidLoad() {
        super.viewDidLoad()
        //webView.scalesPageToFit = true
      NSLog("log printContentStr : %@",printContentStr)
      self.setupBackBtn()
        webView.contentMode = .scaleAspectFill

        //343*258
       // wkWebView.loadHTMLString(<#T##string: String##String#>, baseURL: printContentStr)
        webView.loadHTMLString(printContentStr, baseURL: nil)
        //webView.loadHTMLString("<!DOCTYPE html><html><style>span{display:inline-block;border-bottom:3px solid black;padding-bottom:2px;width: 100%;height: auto;},p {line-height : 1em; //relative to this element, 14pxmargin-bottom : 1em; //relative to this element, 14px}</style><body><p><font size=\"5\"><center><span><b>Event Name </b></center></span></font></p><p><font size=\"3\" ><center><b>Date</b></center></span></font></p><p><font size=\"5\"><center><b>Name</b></font></p><center><img id='barcode'src=\"https://api.qrserver.com/v1/create-qr-code/?data=796412480&amp;size=100x100\"alt=\"\"title=\"HELLO\"width=\"200\"height=\"200\" /></center></body></body></html>", baseURL: nil)
        webView.sizeToFit()
      NotificationCenter.default.addObserver(self, selector: #selector(self.forcePrint), name: Notification.Name("STARTPRINT"), object: nil)

    }
  override func viewWillAppear(_ animated: Bool) {
    //self.startPrint()
  }
    // Select Device
    @IBAction func selectButtonTouchDown(_ sender: Any) {
        self.performSegue(withIdentifier: "goSelectDeviceSegue", sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if(segue.identifier == "goSelectDeviceSegue") {
            guard let selectDeviceTableViewController = segue.destination as? BRSelectDeviceTableViewController else {
                print("Prepare for Segue Error")
                return
            }
            selectDeviceTableViewController.delegate = self
        }
    }
    
    func setSelected(deviceInfo: BRPtouchDeviceInfo) {
        selectedDeviceInfo = deviceInfo
    }
    
    // Print
  @objc func forcePrint() {
    guard let img:UIImage = self.webView.takeScreenshot() else {return}
    
    // guard let img = UIImage(named: "100×200.bmp") else {return}
    guard let modelName:String = GlobalVariable.strModelName else {return}
    let venderName = "Brother "
    let dev = venderName + modelName
    guard let num:String = GlobalVariable.strSerialNumber else {return}
    
    
    
    self.printImage(image: imageQr!, deviceName: dev, serialNumber: num)
    
  }
  @objc func startPrint() {
      if  GlobalVariable.isDeviceInfo(){
        self.printing()
      }else{
        self.performSegue(withIdentifier: "goSelectDeviceSegue", sender: nil)
      }
    
    }
    func printing()  {
      GlobalVariable.getDevicePrinter()
      guard let img:UIImage = self.webView.takeScreenshot() else {return}
    
    // guard let img = UIImage(named: "100×200.bmp") else {return}
      guard let modelName:String = GlobalVariable.strModelName else {return}
      let venderName = "Brother "
      let dev = venderName + modelName
      guard let num:String = GlobalVariable.strSerialNumber else {return}
    
    
    
      self.printImage(image: img, deviceName: dev, serialNumber: num)
    }
    @IBAction func printButtonTouchDown(_ sender: Any) {
        guard let img:UIImage = self.webView.takeScreenshot() else {return}

       // guard let img = UIImage(named: "100×200.bmp") else {return}
        guard let modelName = selectedDeviceInfo?.strModelName else {return}
        let venderName = "Brother "
        let dev = venderName + modelName
        guard let num = selectedDeviceInfo?.strSerialNumber else {return}

        
        
        self.printImage(image: img, deviceName: dev, serialNumber: num)
    }
    
    func printImage (image: UIImage, deviceName: String, serialNumber: String) {
        
        guard let ptp = BRPtouchPrinter(printerName: deviceName, interface: CONNECTION_TYPE.BLUETOOTH) else {
            print("*** Prepare Print Error ***")
          let alertController = UIAlertController(title: "title", message: "Error", preferredStyle: .alert)
          let OKAction = UIAlertAction(title: "OK", style: .default, handler: nil)
          alertController.addAction(OKAction)
          self.present(alertController, animated: true, completion: nil)
          
      
            return
        }
        ptp.setupForBluetoothDevice(withSerialNumber: serialNumber)
        ptp.setPrintInfo(self.settingPrintInfoForPJ())
        
        guard ptp.isPrinterReady() else {
            print("*** Printer is not Ready ***")
          self.presentAlertWithTitle(title: "Bizconnect", message: "Please check Printer is connected. \nHow to \n1. Go to Settings > Bluetooth. \n 2. Make sure that Printer is connected.", options:"Close" ) { (option) in
            switch(option) {
            case 0:
              print("option two")
              self.dismiss(animated: true, completion: nil)
            default:
              break
            }
          }
            return
        }
        
        if ptp.startCommunication() {
            let result = ptp.print(image.cgImage, copy: 1)
            if result != ERROR_NONE_ {
                print ("*** Printing Error *** \(image)")
            }
            ptp.endCommunication()
          let notiToReact:ChangeViewBridge = ChangeViewBridge.alloc(with: nil)
           // ChangeViewBridge.init().setEventReceived()
          notiToReact.setEventReceived()
            self.dismiss(animated: true, completion: nil)
        }
        else {
            print("Communication Error")
        }
    }
    
    func settingPrintInfoForPJ() -> BRPtouchPrintInfo {
        
        let printInfo = BRPtouchPrintInfo()
        
        printInfo.strPaperName = "62mm"
        printInfo.nPrintMode = PRINT_FIT
        printInfo.nOrientation = ORI_LANDSCAPE
        printInfo.nHorizontalAlign = ALIGN_CENTER
        printInfo.nVerticalAlign = ALIGN_MIDDLE
        printInfo.nAutoCutFlag = 1
        printInfo.nAutoCutCopies = 1
        
        return printInfo
    }
    
    func settingPrintInfoForQL() -> BRPtouchPrintInfo {
        
        let printInfo = BRPtouchPrintInfo()
        
        printInfo.strPaperName = "17mmx54"
        printInfo.nPrintMode = PRINT_FIT
        printInfo.nOrientation = ORI_LANDSCAPE
        printInfo.nHorizontalAlign = ALIGN_CENTER
        printInfo.nVerticalAlign = ALIGN_MIDDLE
        printInfo.nAutoCutFlag = 1
        printInfo.nAutoCutCopies = 1
        
        return printInfo
    }
    
    func settingPrintInfoForPT() -> BRPtouchPrintInfo {
        
        let printInfo = BRPtouchPrintInfo()
        
        printInfo.strPaperName = "17mmx54"
        printInfo.nPrintMode = PRINT_FIT
        printInfo.nOrientation = ORI_LANDSCAPE
        printInfo.nHorizontalAlign = ALIGN_CENTER
        printInfo.nVerticalAlign = ALIGN_MIDDLE
        printInfo.nAutoCutFlag = 1
        printInfo.nAutoCutCopies = 1
        
        return printInfo
    }
  func webViewDidFinishLoad(_ webView: UIWebView) {
    print("webview finish")
    let deadlineTime = DispatchTime.now() + .seconds(1)
    DispatchQueue.main.asyncAfter(deadline: deadlineTime) {
      self.imageQr = self.webView.takeScreenshot()
      self.startPrint()
    }
  }
  func openBluetooth(){
//    let url = URL(string: "App-Prefs:root=Bluetooth") //for bluetooth setting
//    let app = UIApplication.shared
//    app.openURL(url!)
//    self.dismiss(animated: true, completion: nil)

  }
}
